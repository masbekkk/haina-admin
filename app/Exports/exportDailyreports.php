<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Models\Reports;
use App\Models\Followers;
use App\Models\views;
use App\Models\Increasing;
use App\Models\Ratio;
use App\Models\Inventory;
use App\Models\Actualpost;
use App\Models\Akun;

class exportDailyreports implements FromView, ShouldAutoSize, WithStyles, WithTitle
{
    
    protected $id;
    protected $nama;
    public function __construct($id){
        $this->id = $id;
        $akun = Akun::find($this->id);
        $this->nama = $akun->nama;
    }

    public function view() : View
    {
        $Followers = Followers::where('akun_id', $this->id)->first();
        $views = views::where('akun_id', $this->id)->first();
        $Increasing = Increasing::where('akun_id', $this->id)->first();
        $Ratio = Ratio::where('akun_id', $this->id)->first();
        $Actualpost = Actualpost::where('akun_id', $this->id)->first();
        $Inventory = Inventory::where('akun_id', $this->id)->first();

        view()->share([
            'followers' => $Followers,
            'views' => $views,
            'Increasing' => $Increasing,
            'Ratio' => $Ratio,
            'actualp' => $Actualpost,
            'inventory' => $Inventory
        ]);

        return view('exports.dailyreports');
    }

    public function title(): string
    {
    	return 'Rekap Daily Reports '.$this->nama;
       
    }

    public function styles(Worksheet $sheet)
    {

        $sheet->getStyle(1)->getFont()->setBold(true);

    }
}
