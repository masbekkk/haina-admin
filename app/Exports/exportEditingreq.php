<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Models\Akun;
use App\Models\EditingRequest;
use Carbon\Carbon;
Use Auth;

class exportEditingreq implements FromView, ShouldAutoSize, WithStyles, WithTitle
{
  
    protected $nama;
    public function __construct(){
        $this->nama = Auth::user()->name;
    }

    public function view() : View
    {
        $data = EditingRequest::with('akun', 'acc', 'user')->get();
        view()->share([
            'data' => $data
        ]);

        return view('exports.editreq');
    }

    public function title(): string
    {
    	return 'Rekap Editing Request by '.$this->nama;
       
    }

    public function styles(Worksheet $sheet)
    {

        $sheet->getStyle(1)->getFont()->setBold(true);

    }
}
