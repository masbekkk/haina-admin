<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Models\Monthly;
use App\Models\Akun;

class exportMonthlyprogress implements FromView, ShouldAutoSize, WithStyles, WithTitle
{
    protected $id;
    protected $nama;
    public function __construct($id){
        $this->id = $id;
        $akun = Akun::find($this->id);
        $this->nama = $akun->nama;
    }

    public function view() : View
    {
        $data = Monthly::where('akun_id', $this->id)->first();

        view()->share([
            'data' => $data
        ]);

        return view('exports.monthlyprogress');
    }

    public function title(): string
    {
    	return 'Rekap Monthly Progress '.$this->nama;
       
    }

    public function styles(Worksheet $sheet)
    {

        $sheet->getStyle(1)->getFont()->setBold(true);

    }
}
