<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Models\Akun;
use App\Models\Invlastweek;
use App\Models\Finish;
use App\Models\Posting;
use App\Models\Inventory;
use App\Models\InventoryControl;

class exportDailyinventory implements FromView, ShouldAutoSize, WithStyles, WithTitle
{
    protected $id;
    protected $nama;
    public function __construct($id){
        $this->id = $id;
        $akun = Akun::find($this->id);
        $this->nama = $akun->nama;
    }

    public function view() : View
    {
        $Invlastweek = Invlastweek::where('akun_id', $this->id)->first();
        $Finish = Finish::where('akun_id', $this->id)->first();
        $Posting = Posting::where('akun_id', $this->id)->first();
        $Inventory = Inventory::where('akun_id', $this->id)->first();

        view()->share([
            'Invlastweek' => $Invlastweek,
            'Finish' => $Finish,
            'posting' => $Posting,
            'inventory' => $Inventory
        ]);

        return view('exports.dailyinventory');
    }

    public function title(): string
    {
    	return 'Rekap Daily Inventory '.$this->nama;
       
    }

    public function styles(Worksheet $sheet)
    {

        $sheet->getStyle(1)->getFont()->setBold(true);

    }
}
