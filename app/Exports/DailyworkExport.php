<?php

namespace App\Exports;

use App\Models\Akun;
use App\Models\Script;
use App\Models\Edit;
use App\Models\Posting;
use App\Models\Shooting;
use App\Models\Admin;
use App\Models\Days;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;


class DailyworkExport implements FromView, ShouldAutoSize, WithStyles, WithTitle
{
    
    protected $id;
    protected $nama;
    public function __construct($id){
        $this->id = $id;
        $akun = Akun::find($this->id);
        $this->nama = $akun->nama;
    }

    public function view() : View
    {
        $akun = Akun::find($this->id);
        $this->nama = $akun->nama;
        $script = Script::where('akun_id', $this->id)->first();
        $Edit = Edit::where('akun_id', $this->id)->first();
        $Posting = Posting::where('akun_id', $this->id)->first();
        $Shooting = Shooting::where('akun_id', $this->id)->first();
        $Admin = Admin::where('akun_id', $this->id)->first();
        view()->share([
            'script' => $script,
            'Edit' => $Edit,
            'posting' => $Posting,
            'Shooting' => $Shooting,
            'admin' => $Admin
        ]);
        return view('exports.dailyworks');
    }
    // public function view() : View
    // {
    //     return view('exports.dailyworks');
    // }
    public function title(): string
    {
    	return 'Rekap Daily Works'.$this->nama;
        // return 'rekap';
       
    }

    public function styles(Worksheet $sheet)
    {

        $sheet->getStyle(1)->getFont()->setBold(true);

    }
}
