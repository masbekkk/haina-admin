<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\Models\Akun;
use App\Models\Trouble;
use Carbon\Carbon;

class exportDailytroubles implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithTitle, WithStyles
{
    protected $id;
    protected $nama;

    public function __construct($id)
    {
        $this->id = $id;
        $akun = Akun::find($this->id);
        $this->nama = $akun->nama;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Trouble::with('akun')->where('akun_id', $this->id)->first();
    }

    public function map($data) : array 
    {
        return [
            $data->akun->nama,
            $data->senin,
            $data->selasa,
            $data->rabu,
            $data->kamis,
            $data->jumat,
            $data->sabtu,
            Carbon::parse($data->created_at)->toFormattedDateString(),
            Carbon::parse($data->updated_at)->toFormattedDateString()

        ];
    }

    public function headings() : array 
    {
        return[
            'Akun',
            'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Tgl Dibuat',
            'Tgl Diupdate'
        ];
    }

    public function title(): string
    {
    	return 'Rekap Daily Trouble '.$this->nama;
    }

    public function styles(Worksheet $sheet)
    {

        $sheet->getStyle(1)->getFont()->setBold(true);

    }
}
