<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GAReport extends Model
{
    use HasFactory;
    protected $table='ga_reports';
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
