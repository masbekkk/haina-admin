<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(){
        return $this->belongsTo(Role::class, 'level');
    }

    public function user(){
        return $this->hasMany(Akun::class, 'id');
    }

    public function editAcc(){
        return $this->hasMany(EditingRequest::class, 'id');
    }

    public function editreq(){
        return $this->hasMany(EditingRequest::class, 'id');
    }

    public function income(){
        return $this->hasMany(Income::class, 'id');
    }

    public function cooperation(){
        return $this->hasMany(Cooperation::class, 'id');
    }

    public function propertyloan(){
        return $this->hasMany(Propertyloan::class, 'id');
    }

    public function ga_reports(){
        return $this->hasMany(GAReport::class, 'id');
    }

    public function presensi()
    {
        return $this->hasMany(Presensi::class, 'id');
    }

    public function editorReport()
    {
        return $this->hasMany(EditorReport::class, 'id');
    }
}
