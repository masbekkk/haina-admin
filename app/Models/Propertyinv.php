<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Propertyinv extends Model
{
    use HasFactory;

    public function loan(){
        return $this->hasMany(Propertyloan::class, 'id');
    }
}
