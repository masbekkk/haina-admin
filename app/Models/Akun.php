<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Akun extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function trouble(){
        return $this->hasMany(Trouble::class, 'id');
    }

    public function Actualpost(){
        return $this->hasMany(Actualpost::class, 'id');
    }

    public function Admin(){
        return $this->hasMany(Admin::class, 'id');
    }

    public function Days(){
        return $this->hasMany(Days::class, 'id');
    }

    public function Edit(){
        return $this->hasMany(Edit::class, 'id');
    }

    public function Followers(){
        return $this->hasMany(Followers::class, 'id');
    }

    public function Increasing(){
        return $this->hasMany(Increasing::class, 'id');
    }

    public function Inventory(){
        return $this->hasMany(Inventory::class, 'id');
    }

    public function Monthly(){
        return $this->hasMany(Monthly::class, 'id');
    }

    public function Posting(){
        return $this->hasMany(Posting::class, 'id');
    }

    public function Ratio(){
        return $this->hasMany(Ratio::class, 'id');
    }

    public function Reports(){
        return $this->hasMany(Reports::class, 'id');
    }

    public function Script(){
        return $this->hasMany(Script::class, 'id');
    }

    public function Shooting(){
        return $this->hasMany(Shooting::class, 'id');
    }

    public function views(){
        return $this->hasMany(views::class, 'id');
    }

    public function reqEdit(){
        return $this->hasMany(EditingRequest::class, 'id');
    }

    public function inventorycontrol(){
        return $this->hasMany(InventoryControl::class, 'id');
    }

    public function invlastweek(){
        return $this->hasMany(Invlastweek::class, 'id');
    }

    public function finish(){
        return $this->hasMany(Finish::class, 'id');
    }
    

    // // this is a recommended way to declare event handlers
    // public static function boot() {
    //     parent::boot();

    //     static::deleting(function($akun) { // before delete() method call this
    //          $akun->Days()->delete();
    //          $akun->Reports()->delete();
    //          // do the rest of the cleanup...
    //     });
    // }
}
