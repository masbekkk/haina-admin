<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EditorReport extends Model
{
    use HasFactory;

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }

    public function setAkunAttribute($value)
    {
        $this->attributes['editing_accounts'] = str_replace("'", "\'", json_encode($value));
    }

    public function getAkunAttribute($value)
    {
        $this->attributes['editing_accounts'] = str_replace("'", "\'", json_decode($value));
    }
}
