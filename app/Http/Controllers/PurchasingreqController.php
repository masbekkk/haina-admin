<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Purchasingreq;
use App\Models\Reimbursmentprog;
use App\Models\User;
Use Auth;
Use Alert;
use File;
use Carbon\Carbon;

class PurchasingreqController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Purchasingreq::all();
            // dd($data);
            view()->share([
                'data' => $data
            ]);
            return view('admin.Purchasingreq.Purchasingreq');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function create(){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $users = User::all();
            view()->share([
                'users' => $users
            ]);
            return view('admin.Purchasingreq.add');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function store(Request $request){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = new Purchasingreq();
            $request->validate([
                'user_id' => 'required',
                'kebutuhan' => 'required',
                'harga' => 'required',
                'jumlah' => 'required'
                // 'foto' => 'image|mimes:jpeg,png,jpg,gif,svg,webp,heic,heif,hevc,pdf|max:5048'
            ]);

            $data->user_id = $request->user_id;
            $data->kebutuhan = $request->kebutuhan;
            $data->harga = $request->harga;
            $data->jumlah = $request->jumlah;
            $photo = $request->file('foto');
            $path = 'image';
            $string = rand(22,5033);
            if ($photo != null) {
                $fileName = $string .'___foto.'.$photo->getClientOriginalExtension();
                $photo->move($path, $fileName);
                $data->foto = $path . '/' . $fileName;
            }
            $data->save();
            return redirect()->route('admin.purchasing-req')
            ->with('toast_success', 'Purchasing Request Berhasl ditambahkan!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function edit($id){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Purchasingreq::find($id);
            $users = User::all();
            view()->share([
                'data' => $data,
                'users' => $users
            ]);
            return view('admin.Purchasingreq.edit');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function update($id, Request $request){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Purchasingreq::find($id);
            $request->validate([
                'user_id' => 'required',
                'kebutuhan' => 'required',
                'harga' => 'required',
                'jumlah' => 'required'
                // 'foto' => 'image|mimes:jpeg,png,jpg,gif,svg,webp,heic,heif,hevc,pdf|max:5048'
            ]);

            $data->user_id = $request->user_id;
            $data->kebutuhan = $request->kebutuhan;
            $data->harga = $request->harga;
            $data->jumlah = $request->jumlah;
            if($data->foto != NULL){
                $foto_old = $data->foto;
                if(File::exists($foto_old)) File::delete($foto_old);
            }

            $photo = $request->file('foto');
            $path = 'image';
            $string = rand(22,5033);
            if ($photo != null) {
                $fileName = $string .'___foto.'.$photo->getClientOriginalExtension();
                $photo->move($path, $fileName);
                $data->foto = $path . '/' . $fileName;
            }
            $data->save();
            return redirect()->route('admin.purchasing-req')
            ->with('toast_success', 'Purchasing Request Berhasl diupdate!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function delete($id){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Purchasingreq::find($id);
            if($data->foto != NULL){
                $foto_old = $data->foto;
                if(File::exists($foto_old)) File::delete($foto_old);
            }
            $data->delete();

            return redirect()->route('admin.purchasing-req')
            ->with('toast_success', 'Purchasing Request Berhasil dihapus!');
        }
        
        else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function management(){
        if(Auth::user()->level == 6){
            $data = Purchasingreq::all();
            view()->share([
                'data' => $data
            ]);
            return view('management.admin.Purchasingreq.Purchasingreq');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function acc($id)
    {
        $data = Purchasingreq::find($id);
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data->status = 2;
            $reimburshment = new Reimbursmentprog();
            $reimburshment->request = $data->kebutuhan;
            $reimburshment->quantity = $data->jumlah;
            $reimburshment->price = $data->harga;
            $reimburshment->submitdate = Carbon::now();
            $reimburshment->user_id = $data->user_id;
            $reimburshment->status = 1;
            $reimburshment->save();
            $data->save();
            return redirect()->route('admin.purchasing-req')
            ->with('toast_success', 'Purchasingreq Berhasil diubah status!');
        }
        else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
        
    }

    public function decline($id)
    {
        $data = Purchasingreq::find($id);
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data->status = 1;
            $data->save();
            return redirect()->route('admin.purchasing-req')
            ->with('toast_success', 'Purchasingreq Berhasil diubah status!');
        }
        else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
        
    }
    public function reset(){
        Purchasingreq::truncate();
        echo "sukses reset";
    }
}
