<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Presensi;
use App\Models\User;
Use Auth;
Use Alert;
use Carbon\Carbon;

class PresensiController extends Controller
{
    public function index()
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 6){
            $data = Presensi::orderBy('created_at', 'desc')->get();
            view()->share([
                'data' => $data
            ]);
            return view('presensi.presensi');
        }else return view('presensi.presensi');
    }

    public function presensiIn($status, Request $request)
    {
        $data = new Presensi();
        $request->validate([
            'bukti_in' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp,heic,heif,hevc,pdf|max:50048'
        ]);
        $user = User::find(Auth::user()->id);
        $data->user_id = Auth::user()->id;
        if($status == "wfh") {
            $data->status_id = 2;
            $user->status = 2;
        } 
        elseif($status == "wfo") {
            $data->status_id = 1;
            $user->status = 1;
        }
        $photo = $request->file('bukti_in');
        $path = 'bukti_presensi';
        $string = rand(22, 10003);
        if( $photo != NULL){
            $fileName = $string . '___buktipresensimasuk'.$photo->getClientOriginalExtension();
            $photo->move($path, $fileName);
            $data->bukti_in = $path .'/'. $fileName;
        }
        $data->save();
        $user->save();
        return redirect()->route('presensi')
        ->with('toast_success', 'Terima kasih telah melakukan Presensi!');

    }

    public function presensiOut($status, Request $request)
    {
        $data = Presensi::where('user_id', Auth::user()->id)->first();
        // dd($data);
        $request->validate([
            'bukti_out' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp,heic,heif,hevc,pdf|max:50048'
        ]);
        $user = User::find(Auth::user()->id);
        // $data->user_id = Auth::user()->id;
        if($status == "wfh") {
            $data->status_id = 2;
            $user->status = 0;
        } 
        elseif($status == "wfo") {
            $data->status_id = 1;
            $user->status = 0;
        }
        $photo = $request->file('bukti_out');
        $path = 'bukti_presensi';
        $string = rand(22, 10003);
        if($photo != NULL){
            $fileName = $string . '___buktipresensiout'.$photo->getClientOriginalExtension();
            $photo->move($path, $fileName);
            $data->bukti_out = $path .'/'. $fileName;
        }
        $data->presensi_out = Carbon::now();
        $data->save();
        $user->save();
        return redirect()->route('presensi')
        ->with('toast_success', 'Terima kasih telah melakukan Presensi!');

    }
}
