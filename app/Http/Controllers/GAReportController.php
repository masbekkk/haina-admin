<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GAReport;
// use App\Models\Propertyloan;
Use Auth;
Use Alert;

class GAReportController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        if(Auth::user()->level == 0){
            $belum = 0;
            $data = GAReport::with('user')->get();
            view()->share([
                'data' => $data,
                'belum' => $belum
            ]);
            return view('admin.GAReport.GAReport');
        }elseif(Auth::user()->level == 2){
            $data = GAReport::where('user_id', Auth::user()->id)->first();
            $belum = 1;
            if(!empty($data)) {
                $belum = 0;
                $data = GAReport::where('user_id', Auth::user()->id)->get();
            }
            view()->share([
                'data' => $data,
                'belum' => $belum
            ]);
            return view('admin.GAReport.GAReport');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function create(){
        if(Auth::user()->level == 2 || Auth::user()->level == 0)
            return view('admin.GAReport.add');
        else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function store(Request $request){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = new GAReport();
            $request->validate([
                'date_of_reports' => 'required',
                'today_works' => 'required',
                'number_of_works' => 'required'
            ]);

            $data->user_id = Auth::user()->id;
            $data->date_of_reports = $request->date_of_reports;
            $data->today_works = $request->today_works;
            $data->number_of_works = $request->number_of_works;
            $data->working_of_trouble = $request->working_of_trouble;
            $data->save();
            return redirect()->route('admin.ga-reports')
            ->with('toast_success', 'GA Report Berhasl ditambahkan!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function edit($id){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = GAReport::find($id);
            view()->share([
                'data' => $data
            ]);
            return view('admin.GAReport.edit');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function update($id, Request $request){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = GAReport::find($id);
            $request->validate([
                'date_of_reports' => 'required',
                'today_works' => 'required',
                'number_of_works' => 'required'
            ]);

            $data->date_of_reports = $request->date_of_reports;
            $data->today_works = $request->today_works;
            $data->number_of_works = $request->number_of_works;
            $data->working_of_trouble = $request->working_of_trouble;
            $data->save();
            return redirect()->route('admin.ga-reports')
            ->with('toast_success', 'GA Report Berhasl diupdate!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function delete($id){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = GAReport::find($id)->delete();
            return redirect()->route('admin.ga-reports')
            ->with('toast_success', 'GA Report Berhasil dihapus!');
        }
        
        else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function management(){
        if(Auth::user()->level == 6){
            $data = GAReport::all();
            view()->share([
                'data' => $data
            ]);
            return view('management.admin.GAReport.GAReport');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }
}
