<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Monthly;
use App\Models\posterInfo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Monthly::with('akun')->get();
        $poster = posterInfo::orderBy('id', 'desc')->get();
        // dd($poster);
        view()->share([
            'data' => $data,
            'poster' => $poster
        ]);
        return view('home');
    }
}
