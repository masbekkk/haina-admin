<?php

namespace App\Http\Controllers;

use App\Models\Reports;
use App\Models\Followers;
use App\Models\views;
use App\Models\Increasing;
use App\Models\Ratio;
use App\Models\Inventory;
use App\Models\Actualpost;
use App\Models\Akun;
use Illuminate\Http\Request;
Use Auth;
Use Alert;
use App\Exports\exportDailyreports;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function view(){
        if(Auth::user()->level == 0 || Auth::user()->level == 2){
            $data = Akun::with('user')->get();
            $belum = 0;
        }else {
            $data = Akun::where('user_id', Auth::user()->id)->first();
            if(!empty($data)){
                $belum =0;
                $data = Akun::where('user_id', Auth::user()->id)->get();
            } 
            else $belum =1;
        }
            view()->share([
                'data' => $data,
                'belum' => $belum
            ]);
            return view('manage.dailyreports');

    }

    public function index($id)
    {
        $Followers = Followers::where('akun_id', $id)->first();
        $views = views::where('akun_id', $id)->first();
        $Increasing = Increasing::where('akun_id', $id)->first();
        $Ratio = Ratio::where('akun_id', $id)->first();
        $Actualpost = Actualpost::where('akun_id', $id)->first();
        $Inventory = Inventory::where('akun_id', $id)->first();
        
        $reports = Reports::where('akun_id', $id)->first();
        if(!empty($reports)) $belum = 0;
        else $belum = 1;
        
        if(Auth::user()->level == 0 || Auth::user()->level == 2) $data = Akun::find($id);
        else{
            $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            if(!empty($data)) $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            else return redirect()->back()->with('errors', 'Kamu bukan Handler dari akun ini!');
        }
        view()->share([
            'data' => $data,
            'ada' => $belum,
            'followers' => $Followers,
            'views' => $views,
            'increasing' => $Increasing,
            'ratio' => $Ratio,
            'actualp' => $Actualpost,
            'inventory' => $Inventory
        ]);

        return view('fitur.reports');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id, Request $request)
    {
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $data = new Reports();
        $data->akun_id = $id;
        $data->save();
        $followers = new Followers();
        $views = new views();
        $increasing = new Increasing();
        $actualp = new Actualpost();
        $inventory = new Inventory();
        $ratio = new Ratio();

        $followers->akun_id = $id;
        $followers->senin = $request->followers1;
        $followers->selasa = $request->followers2;
        $followers->rabu = $request->followers3;
        $followers->kamis = $request->followers4;
        $followers->jumat = $request->followers5;
        $followers->sabtu = $request->followers6;
        $followers->save();

        //views
        $views->akun_id = $id;
        $views->senin = $request->views1;
        $views->selasa = $request->views2;
        $views->rabu = $request->views3;
        $views->kamis = $request->views4;
        $views->jumat = $request->views5;
        $views->sabtu = $request->views6;
        $views->save();

        //increasing
        $increasing->akun_id = $id;
        $increasing->senin = $request->increasing1;
        $increasing->selasa = $request->increasing2;
        $increasing->rabu = $request->increasing3;
        $increasing->kamis = $request->increasing4;
        $increasing->jumat = $request->increasing5;
        $increasing->sabtu = $request->increasing6;
        $increasing->save();

        //ratio
        $ratio->akun_id = $id;
        $ratio->senin = $request->ratio1;
        $ratio->selasa = $request->ratio2;
        $ratio->rabu = $request->ratio3;
        $ratio->kamis = $request->ratio4;
        $ratio->jumat = $request->ratio5;
        $ratio->sabtu = $request->ratio6;
        $ratio->save();

        //actualp
        $actualp->akun_id = $id;
        $actualp->senin = $request->actualp1;
        $actualp->selasa = $request->actualp2;
        $actualp->rabu = $request->actualp3;
        $actualp->kamis = $request->actualp4;
        $actualp->jumat = $request->actualp5;
        $actualp->sabtu = $request->actualp6;
        $actualp->save();

        //inventory
        $inventory->akun_id = $id;
        $inventory->senin = $request->inventory1;
        $inventory->selasa = $request->inventory2;
        $inventory->rabu = $request->inventory3;
        $inventory->kamis = $request->inventory4;
        $inventory->jumat = $request->inventory5;
        $inventory->sabtu = $request->inventory6;
        $inventory->save();

        return redirect()->route('manage.dailyreports')
        ->with('toast_success', 'Daily Reports berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function show(Reports $reports)
    {
        //
    }

    /**
     * Show the form for viewsing the specified resource.
     *
     * @param  \App\Models\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function views(Reports $reports)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        
        $followers = Followers::where('akun_id', $id)->first();
        $views = views::where('akun_id', $id)->first();
        $increasing = Increasing::where('akun_id', $id)->first();
        $actualp = Actualpost::where('akun_id', $id)->first();
        $inventory = Inventory::where('akun_id', $id)->first();
        $ratio = Ratio::where('akun_id', $id)->first();

        $followers->akun_id = $id;
        $followers->senin = $request->followers1;
        $followers->selasa = $request->followers2;
        $followers->rabu = $request->followers3;
        $followers->kamis = $request->followers4;
        $followers->jumat = $request->followers5;
        $followers->sabtu = $request->followers6;
        $followers->save();

        //views
        $views->akun_id = $id;
        $views->senin = $request->views1;
        $views->selasa = $request->views2;
        $views->rabu = $request->views3;
        $views->kamis = $request->views4;
        $views->jumat = $request->views5;
        $views->sabtu = $request->views6;
        $views->save();

        //increasing
        $increasing->akun_id = $id;
        $increasing->senin = $request->increasing1;
        $increasing->selasa = $request->increasing2;
        $increasing->rabu = $request->increasing3;
        $increasing->kamis = $request->increasing4;
        $increasing->jumat = $request->increasing5;
        $increasing->sabtu = $request->increasing6;
        $increasing->save();

        //ratio
        $ratio->akun_id = $id;
        $ratio->senin = $request->ratio1;
        $ratio->selasa = $request->ratio2;
        $ratio->rabu = $request->ratio3;
        $ratio->kamis = $request->ratio4;
        $ratio->jumat = $request->ratio5;
        $ratio->sabtu = $request->ratio6;
        $ratio->save();

        //actualp
        $actualp->akun_id = $id;
        $actualp->senin = $request->actualp1;
        $actualp->selasa = $request->actualp2;
        $actualp->rabu = $request->actualp3;
        $actualp->kamis = $request->actualp4;
        $actualp->jumat = $request->actualp5;
        $actualp->sabtu = $request->actualp6;
        $actualp->save();

        //inventory
        $inventory->akun_id = $id;
        $inventory->senin = $request->inventory1;
        $inventory->selasa = $request->inventory2;
        $inventory->rabu = $request->inventory3;
        $inventory->kamis = $request->inventory4;
        $inventory->jumat = $request->inventory5;
        $inventory->sabtu = $request->inventory6;
        $inventory->save();

        return redirect()->route('manage.dailyreports')
        ->with('toast_success', 'Daily Reports berhasil diupdate!');
    }

    public function viewmanagement(){
        if(Auth::user()->level == 6){
            $data = Akun::with('user')->get();
            $belum = 0;
        }else {
            $data = Akun::where('user_id', Auth::user()->id)->first();
            if(!empty($data)){
                $belum =0;
                $data = Akun::where('user_id', Auth::user()->id)->get();
            } 
            else $belum =1;
        }
            view()->share([
                'data' => $data,
                'belum' => $belum
            ]);

        return view('management.manage.dailyreports');
    }

    public function management($id)
    {
        $Followers = Followers::where('akun_id', $id)->first();
        $views = views::where('akun_id', $id)->first();
        $Increasing = Increasing::where('akun_id', $id)->first();
        $Ratio = Ratio::where('akun_id', $id)->first();
        $Actualpost = Actualpost::where('akun_id', $id)->first();
        $Inventory = Inventory::where('akun_id', $id)->first();
        
        $reports = Reports::where('akun_id', $id)->first();
        if(!empty($reports)) $belum = 0;
        else $belum = 1;
        
        if(Auth::user()->level == 6) $data = Akun::find($id);
        else{
            $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            if(!empty($data)) $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            else return redirect()->back()->with('errors', 'Kamu bukan Handler dari akun ini!');
        }
        view()->share([
            'data' => $data,
            'ada' => $belum,
            'followers' => $Followers,
            'views' => $views,
            'increasing' => $Increasing,
            'ratio' => $Ratio,
            'actualp' => $Actualpost,
            'inventory' => $Inventory
        ]);

        return view('management.fitur.reports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reports $reports)
    {
        //
    }

    public function export($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = Akun::find($id);
            return Excel::download(new exportDailyreports($id), 'Rekap Daily Reports '.$data->nama.'.xlsx');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');
    }

    public function reset($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {   
            $Followers = Followers::where('akun_id', $id)->delete();
            $views = views::where('akun_id', $id)->delete();
            $Increasing = Increasing::where('akun_id', $id)->delete();
            $Ratio = Ratio::where('akun_id', $id)->delete();
            $Actualpost = Actualpost::where('akun_id', $id)->delete();
            $Inventory = Inventory::where('akun_id', $id)->delete();
            $reports = Reports::where('akun_id', $id)->delete();
            return redirect()->route('manage.dailyreports')
            ->with('toast_success', 'Daily Reports berhasil direset!');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');

    }
}
