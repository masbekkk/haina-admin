<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cooperation;
use App\Models\User;
Use Auth;
Use Alert;

class CooperationController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $ada = 2;
        if(Auth::user()->level == 2 || Auth::user()->level == 0) $data = Cooperation::with('user')->get();
        else {
            $data = Cooperation::where('user_id', Auth::user()->id)->first();
            $ada = 1;
            if(!empty($data)) $data = Cooperation::where('user_id', Auth::user()->id)->get();
            else $ada = 0;
        }
            view()->share([
                'data' => $data,
                'ada' => $ada
            ]);
        return view('admin.Cooperation.Cooperation');
        
    }

    public function create(){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $users = User::all();
            view()->share([
                'users' => $users
            ]);
            return view('admin.Cooperation.add');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function store(Request $request){
        $data = new Cooperation();
        $request->validate([
            'company' => 'required',
            'type_promot' => 'required',
            'duration' => 'required',
            'pricingdeal' => 'required',
            'user' => 'required'
        ]);

        $data->company = $request->company;
        $data->type_promot = $request->type_promot;
        $data->terms = $request->terms;
        $data->start = $request->start;
        $data->end = $request->end;
        $data->duration = $request->duration;
        $data->pricingdeal = $request->pricingdeal;
        $data->user_id = $request->user;
        $data->status = $request->status;
        $data->save();
        return redirect()->route('admin.Cooperation')
        ->with('toast_success', 'Cooperation Berhasl ditambahkan!');
    }

    public function edit($id){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Cooperation::find($id);
            $users = User::all();
            view()->share([
                'data' => $data,
                'users' => $users
            ]);
            return view('admin.Cooperation.edit');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function update($id, Request $request)
    {
        $data = Cooperation::find($id);
        $request->validate([
            'company' => 'required',
            'type_promot' => 'required',
            'duration' => 'required',
            'pricingdeal' => 'required',
            'user' => 'required'
        ]);

        $data->company = $request->company;
        $data->duration = $request->duration;
        $data->type_promot = $request->type_promot;
        $data->terms = $request->terms;
        $data->start = $request->start;
        $data->end = $request->end;
        $data->pricingdeal = $request->pricingdeal;
        $data->user_id = $request->user;
        $data->save();
        return redirect()->route('admin.Cooperation')
        ->with('toast_success', 'Cooperation Berhasl diupdate!');
    }

    public function delete($id)
    {
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Cooperation::find($id)->delete();
            return redirect()->route('admin.Cooperation')
            ->with('toast_success', 'Cooperation Berhasil dihapus!');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function done($id)
    {
        $data = Cooperation::find($id);
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data->status = 2;
            $data->save();
            return redirect()->route('admin.Cooperation')
            ->with('toast_success', 'Cooperation Berhasil diubah status!');
        }
        else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
        
    }

    public function on_progress($id)
    {
        $data = Cooperation::find($id);
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data->status = 1;
            $data->save();
            return redirect()->route('admin.Cooperation')
            ->with('toast_success', 'Cooperation Berhasil diubah status!');
        }
        else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
        
    }
    public function management(){
        $data = Cooperation::with('user')->get();
        view()->share([
            'data' => $data
        ]);
        return view('management.admin.Cooperation.Cooperation');
        
    }
}
