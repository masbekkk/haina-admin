<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Income;
use App\Models\User;
Use Auth;
Use Alert;

class IncomeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $ada = 2;
        if(Auth::user()->level == 2 || Auth::user()->level == 0) $data = Income::with('user')->get();
        else {
            $data = Income::where('user_id', Auth::user()->id)->first();
            $ada = 1;
            if(!empty($data)) $data = Income::where('user_id', Auth::user()->id)->get();
            else $ada = 0;
        }
            view()->share([
                'data' => $data,
                'ada' => $ada
            ]);
        return view('admin.income.income');
        
    }

    public function create(){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $users = User::all();
            view()->share([
                'users' => $users
            ]);
            return view('admin.income.add');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function store(Request $request){
        $data = new Income();
        $request->validate([
            'company' => 'required',
            'duration' => 'required',
            'pricingdeal' => 'required',
            'user' => 'required'
        ]);

        $data->company = $request->company;
        $data->duration = $request->duration;
        $data->pricingdeal = $request->pricingdeal;
        $data->user_id = $request->user;
        $data->status = $request->status;
        $data->save();
        return redirect()->route('admin.income')
        ->with('toast_success', 'Income Berhasl ditambahkan!');
    }

    public function edit($id){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Income::find($id);
            $users = User::all();
            view()->share([
                'data' => $data,
                'users' => $users
            ]);
            return view('admin.income.edit');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function update($id, Request $request)
    {
        $data = Income::find($id);
        $request->validate([
            'company' => 'required',
            'duration' => 'required',
            'pricingdeal' => 'required',
            'user' => 'required'
        ]);

        $data->company = $request->company;
        $data->duration = $request->duration;
        $data->pricingdeal = $request->pricingdeal;
        $data->user_id = $request->user;
        $data->save();
        return redirect()->route('admin.income')
        ->with('toast_success', 'Income Berhasl diupdate!');
    }

    public function delete($id)
    {
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Income::find($id)->delete();
            return redirect()->route('admin.income')
            ->with('toast_success', 'Income Berhasil dihapus!');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function done($id)
    {
        $data = Income::find($id);
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data->status = 2;
            $data->save();
            return redirect()->route('admin.income')
            ->with('toast_success', 'Income Berhasil diubah status!');
        }
        else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
        
    }

    public function on_progress($id)
    {
        $data = Income::find($id);
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data->status = 1;
            $data->save();
            return redirect()->route('admin.income')
            ->with('toast_success', 'Income Berhasil diubah status!');
        }
        else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
        
    }

    public function management(){
        $data = Income::with('user')->get();
        view()->share([
            'data' => $data
        ]);
        return view('management.admin.income.income');
        
    }
}
