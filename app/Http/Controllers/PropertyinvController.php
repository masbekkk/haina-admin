<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Propertyinv;
use App\Models\Propertyloan;
Use Auth;
Use Alert;

class PropertyinvController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Propertyinv::all();
            view()->share([
                'data' => $data
            ]);
            return view('admin.propertyinv.propertyinv');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function create(){
        if(Auth::user()->level == 2 || Auth::user()->level == 0)
            return view('admin.propertyinv.add');
        else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function store(Request $request){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = new Propertyinv();
            $request->validate([
                'nama_barang' => 'required',
                'status' => 'required',
                'harga' => 'required',
                'jumlah' => 'required'
            ]);

            $data->nama_barang = $request->nama_barang;
            $data->status = $request->status;
            $data->harga = $request->harga;
            $data->jumlah = $request->jumlah;
            $data->save();
            return redirect()->route('admin.property-inv')
            ->with('toast_success', 'Property Investment Berhasl ditambahkan!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function edit($id){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Propertyinv::find($id);
            view()->share([
                'data' => $data
            ]);
            return view('admin.propertyinv.edit');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function update($id, Request $request){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Propertyinv::find($id);
            $request->validate([
                'nama_barang' => 'required',
                'status' => 'required',
                'harga' => 'required',
                'jumlah' => 'required'
            ]);

            $data->nama_barang = $request->nama_barang;
            $data->status = $request->status;
            $data->harga = $request->harga;
            $data->jumlah = $request->jumlah;
            $data->save();
            return redirect()->route('admin.property-inv')
            ->with('toast_success', 'Property Investment Berhasl diupdate!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function delete($id){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Propertyinv::find($id)->delete();
            Propertyloan::where('property_id', $id)->delete();
            return redirect()->route('admin.property-inv')
            ->with('toast_success', 'Property Investment Berhasil dihapus!');
        }
        
        else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function management(){
        if(Auth::user()->level == 6){
            $data = Propertyinv::all();
            view()->share([
                'data' => $data
            ]);
            return view('management.admin.propertyinv.propertyinv');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }
}
