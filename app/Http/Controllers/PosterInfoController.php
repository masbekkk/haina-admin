<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\posterInfo;
Use Auth;
Use Alert;
Use File;

class PosterInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = posterInfo::all();
            view()->share([
                'data' => $data
            ]);
            return view('admin.poster.index');
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');
    }

    public function store(Request $request)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = new posterInfo();
            $request->validate([
                'gambar' => 'required'
            ]);
            $photo = $request->file('gambar');
            $path = 'poster';
            $string = rand(22,5033);
            if ($photo != null) {
                $fileName = $string .'___poster.'.$photo->getClientOriginalExtension();
                $photo->move($path, $fileName);
                $data->gambar = $path . '/' . $fileName;
            }
            $data->save();
            return redirect()->route('admin.poster')
            ->with('toast_success', 'Data berhasil ditambahkan!');
            
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');
    }

    public function edit($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = posterInfo::find($id);
            view()->share([
                'data' => $data
            ]);
            return view('admin.poster.edit');
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');
    }

    public function update($id, Request $request)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = posterInfo::find($id);
            $request->validate([
                'gambar' => 'required'
            ]);
            $photo = $request->file('gambar');
            $path = 'poster';
            $string = rand(22,5033);
            if ($photo != null) {
                $fileName = $string .'___poster.'.$photo->getClientOriginalExtension();
                $photo->move($path, $fileName);
                $gambar_old = $data->gambar;
                if(File::exists($gambar_old)){
                    File::delete($gambar_old);
                }
                $data->gambar = $path . '/' . $fileName;
                
            }
            $data->save();
            return redirect()->route('admin.poster')
            ->with('toast_success', 'Data berhasil diupdate!');
            
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');
    }

    public function destroy($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = posterInfo::find($id);
            $gambar_old = $data->gambar;
            if(File::exists($gambar_old)){
                File::delete($gambar_old);
            }
            $data->delete();
           
            return redirect()->route('admin.poster')
            ->with('toast_success', 'Data Berhasil Dihapus!');
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');
    }
}
