<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Akun;
use App\Models\Script;
use App\Models\Edit;
use App\Models\Posting;
use App\Models\Shooting;
use App\Models\Admin;
use App\Models\Days;
Use Auth;
Use Alert;
use App\Exports\DailyworkExport;
use Maatwebsite\Excel\Facades\Excel;
Use PDF;

class DaysController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function view(){
        if(Auth::user()->level == 0 || Auth::user()->level == 2){
            $data = Akun::with('user')->get();
            $belum = 0;
        }else {
            $data = Akun::where('user_id', Auth::user()->id)->first();
            if(!empty($data)){
                $belum =0;
                $data = Akun::where('user_id', Auth::user()->id)->get();
            } 
            else $belum =1;
        }
            view()->share([
                'data' => $data,
                'belum' => $belum
            ]);
            return view('manage.dailyworks');

    }

    public function manage($id){
        $script = Script::where('akun_id', $id)->first();
        $Edit = Edit::where('akun_id', $id)->first();
        $Posting = Posting::where('akun_id', $id)->first();
        $Shooting = Shooting::where('akun_id', $id)->first();
        $Admin = Admin::where('akun_id', $id)->first();
        $day = Days::where('akun_id', $id)->first();
        if(!empty($day)) $belum = 0;
        else $belum = 1;
        
        if(Auth::user()->level == 0 || Auth::user()->level == 2) $data = Akun::find($id);
        else{
            $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            if(!empty($data)) $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            else return redirect()->back()->with('errors', 'Kamu bukan Handler dari akun ini!');
        }
        view()->share([
            'data' => $data,
            'ada' => $belum,
            'script' => $script,
            'Edit' => $Edit,
            'posting' => $Posting,
            'Shooting' => $Shooting,
            'admin' => $Admin
        ]);

        return view('fitur.schedule');
    }

    public function addDaily($id, Request $request)
    {
        $data = new Days();
        $data->akun_id = $id;
        $data->fitur_id = 0;
        $data->save();
        $script = new Script();
        $edit = new Edit();
        $posting = new Posting();
        $shooting = new Shooting();
        $admin = new Admin();

        $script->akun_id = $id;
        $script->senin = $request->script1;
        $script->selasa = $request->script2;
        $script->rabu = $request->script3;
        $script->kamis = $request->script4;
        $script->jumat = $request->script5;
        $script->sabtu = $request->script6;
        $script->save();

        //edit
        $edit->akun_id = $id;
        $edit->senin = $request->edit1;
        $edit->selasa = $request->edit2;
        $edit->rabu = $request->edit3;
        $edit->kamis = $request->edit4;
        $edit->jumat = $request->edit5;
        $edit->sabtu = $request->edit6;
        $edit->save();

        //posting
        $posting->akun_id = $id;
        $posting->senin = $request->posting1;
        $posting->selasa = $request->posting2;
        $posting->rabu = $request->posting3;
        $posting->kamis = $request->posting4;
        $posting->jumat = $request->posting5;
        $posting->sabtu = $request->posting6;
        $posting->save();

        //shooting
        $shooting->akun_id = $id;
        $shooting->senin = $request->shooting1;
        $shooting->selasa = $request->shooting2;
        $shooting->rabu = $request->shooting3;
        $shooting->kamis = $request->shooting4;
        $shooting->jumat = $request->shooting5;
        $shooting->sabtu = $request->shooting6;
        $shooting->save();

        //admin
        $admin->akun_id = $id;
        $admin->senin = $request->admin1;
        $admin->selasa = $request->admin2;
        $admin->rabu = $request->admin3;
        $admin->kamis = $request->admin4;
        $admin->jumat = $request->admin5;
        $admin->sabtu = $request->admin6;
        $admin->save();
        return redirect()->route('manage.dailyworks')
        ->with('toast_success', 'Daily works berhasil ditambahkan!');
    }

    public function updateDaily($id, Request $request)
    {
        $script = Script::where('akun_id', $id)->first();
        $edit = Edit::where('akun_id', $id)->first();
        $posting = Posting::where('akun_id', $id)->first();
        $shooting = Shooting::where('akun_id', $id)->first();
        $admin = Admin::where('akun_id', $id)->first();
        
        $script->akun_id = $id;
        $script->senin = $request->script1;
        $script->selasa = $request->script2;
        $script->rabu = $request->script3;
        $script->kamis = $request->script4;
        $script->jumat = $request->script5;
        $script->sabtu = $request->script6;
        $script->total = $request->total1;
        $script->save();

        //edit
        $edit->akun_id = $id;
        $edit->senin = $request->edit1;
        $edit->selasa = $request->edit2;
        $edit->rabu = $request->edit3;
        $edit->kamis = $request->edit4;
        $edit->jumat = $request->edit5;
        $edit->sabtu = $request->edit6;
        $edit->save();

        //posting
        $posting->akun_id = $id;
        $posting->senin = $request->posting1;
        $posting->selasa = $request->posting2;
        $posting->rabu = $request->posting3;
        $posting->kamis = $request->posting4;
        $posting->jumat = $request->posting5;
        $posting->sabtu = $request->posting6;
        $posting->save();

        //shooting
        $shooting->akun_id = $id;
        $shooting->senin = $request->shooting1;
        $shooting->selasa = $request->shooting2;
        $shooting->rabu = $request->shooting3;
        $shooting->kamis = $request->shooting4;
        $shooting->jumat = $request->shooting5;
        $shooting->sabtu = $request->shooting6;
        $shooting->save();

        //admin
        $admin->akun_id = $id;
        $admin->senin = $request->admin1;
        $admin->selasa = $request->admin2;
        $admin->rabu = $request->admin3;
        $admin->kamis = $request->admin4;
        $admin->jumat = $request->admin5;
        $admin->sabtu = $request->admin6;
        $admin->save();
        
        return redirect()->route('manage.dailyworks')
        ->with('toast_success', 'Daily works berhasil diupdate!');
    }

    public function viewmanagement(){
        $data = Akun::with('user')->get();
        view()->share([
            'data' => $data
        ]);
        return view('management.manage.dailyworks');
    }

    public function management($id){
        $script = Script::where('akun_id', $id)->first();
        $Edit = Edit::where('akun_id', $id)->first();
        $Posting = Posting::where('akun_id', $id)->first();
        $Shooting = Shooting::where('akun_id', $id)->first();
        $Admin = Admin::where('akun_id', $id)->first();
        $day = Days::where('akun_id', $id)->first();
        if(!empty($day)) $belum = 0;
        else $belum = 1;
        
        if(Auth::user()->level == 6 ) $data = Akun::find($id);
        else{
            $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            if(!empty($data)) $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            else return redirect()->back()->with('errors', 'Kamu bukan Handler dari akun ini!');
        }
        view()->share([
            'data' => $data,
            'ada' => $belum,
            'script' => $script,
            'Edit' => $Edit,
            'posting' => $Posting,
            'Shooting' => $Shooting,
            'admin' => $Admin
        ]);

        return view('management.fitur.schedule');
    }

    public function export($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = Akun::find($id);
            // $script = Script::where('akun_id', $id)->first();
            // $edit = Edit::where('akun_id', $id)->first();
            // $posting = Posting::where('akun_id', $id)->first();
            // $shooting = Shooting::where('akun_id', $id)->first();
            // $admin = Admin::where('akun_id', $id)->first();
            // $script->total = $request->total1;
            // $script->save();
            return Excel::download(new DailyworkExport($id), 'Rekap Daily Works '.$data->nama.'.xlsx');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');
    }

    public function reset($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {   
            $day = Days::where('akun_id', $id)->delete();
            $script = Script::where('akun_id', $id)->delete();
            $edit = Edit::where('akun_id', $id)->delete();
            $posting = Posting::where('akun_id', $id)->delete();
            $shooting = Shooting::where('akun_id', $id)->delete();
            $admin = Admin::where('akun_id', $id)->delete();
            return redirect()->route('manage.dailyworks')
            ->with('toast_success', 'Daily works berhasil direset!');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');

    }

    public function exportPDF($id)
    {
        $data = Akun::find($id);
    	$judul = ['title' => 'Rekap '.$data->nama];
        $pdf = PDF::loadView('exports.dailyworks', $judul);
  
        return $pdf->stream('Nicesnippets.pdf');
    }
}
