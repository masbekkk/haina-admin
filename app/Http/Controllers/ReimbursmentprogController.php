<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reimbursmentprog;
use App\Models\User;
Use Auth;
Use Alert;

class ReimbursmentprogController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $ada = 2;
        if(Auth::user()->level == 2 || Auth::user()->level == 0) $data = Reimbursmentprog::with('user')->get();
        else {
            $data = Reimbursmentprog::where('user_id', Auth::user()->id)->first();
            $ada = 1;
            if(!empty($data)) $data = Reimbursmentprog::where('user_id', Auth::user()->id)->get();
            else $ada = 0;
        }
            view()->share([
                'data' => $data,
                'ada' => $ada
            ]);
        return view('admin.Reimbursmentprog.Reimbursmentprog');
        
    }

    public function create(){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $users = User::all();
            view()->share([
                'users' => $users
            ]);
            return view('admin.Reimbursmentprog.add');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function store(Request $request){
        $data = new Reimbursmentprog();
        $request->validate([
            'rekues' => 'required',
            'quantity' => 'required',
            'price' => 'required',
            'submitdate' => 'required',
            'user' => 'required'
        ]);

        $data->request = $request->rekues;
        $data->quantity = $request->quantity;
        $data->price = $request->price;
        $data->submitdate = $request->submitdate;
        $data->user_id = $request->user;
        $data->status = $request->status;
        $data->save();
        return redirect()->route('admin.Reimbursmentprog')
        ->with('toast_success', 'Reimbursmentprog Berhasl ditambahkan!');
    }

    public function edit($id){
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Reimbursmentprog::find($id);
            $users = User::all();
            view()->share([
                'data' => $data,
                'users' => $users
            ]);
            return view('admin.Reimbursmentprog.edit');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function update($id, Request $request)
    {
        $data = Reimbursmentprog::find($id);
        $request->validate([
            'rekues' => 'required',
            'quantity' => 'required',
            'price' => 'required',
            'submitdate' => 'required',
            'user' => 'required'
        ]);

        $data->request = $request->rekues;
        $data->quantity = $request->quantity;
        $data->price = $request->price;
        $data->submitdate = $request->submitdate;
        $data->user_id = $request->user;
        $data->save();
        return redirect()->route('admin.Reimbursmentprog')
        ->with('toast_success', 'Reimbursmentprog Berhasl diupdate!');
    }

    public function delete($id)
    {
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Reimbursmentprog::find($id)->delete();
            return redirect()->route('admin.Reimbursmentprog')
            ->with('toast_success', 'Reimbursmentprog Berhasil dihapus!');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function done($id)
    {
        $data = Reimbursmentprog::find($id);
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data->status = 2;
            $data->save();
            return redirect()->route('admin.Reimbursmentprog')
            ->with('toast_success', 'Reimbursmentprog Berhasil diubah status!');
        }
        else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
        
    }

    public function on_progress($id)
    {
        $data = Reimbursmentprog::find($id);
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data->status = 1;
            $data->save();
            return redirect()->route('admin.Reimbursmentprog')
            ->with('toast_success', 'Reimbursmentprog Berhasil diubah status!');
        }
        else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
        
    }

    public function management(){
        $data = Reimbursmentprog::with('user')->get();
        view()->share([
            'data' => $data
        ]);
        return view('management.admin.Reimbursmentprog.Reimbursmentprog');
        
    }
}
