<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DigitalDocuments;
Use Auth;
Use Alert;
Use File;

class DigitalDocumentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($kategori)
    {
       
        if($kategori == 1) $kategori_text = "Rules and Regulation";
        else $kategori_text = "Ratecard Estimation";
        $data = DigitalDocuments::where('kategori', $kategori)->first();
        if(!empty($data)){
            $ada = 1;
            $data = DigitalDocuments::where('kategori', $kategori)->get();
        }else $ada = 0;
        view()->share([
            'data' => $data,
            'ada' => $ada,
            'kategori' => $kategori_text
        ]);

        return view('admin.digital_documents.index');
    }

    public function create()
    {
        return view('admin.digital_documents.create');
    }

    public function store(Request $request)
    {
        $data = new DigitalDocuments();
        $request->validate([
            'nama_file' => 'required',
            'kategori' => 'required',
            'file' => 'required'
        ]);

        $data->kategori = $request->kategori;
        if($request->kategori == 1) $kategori = "Rules and Regulation";
        else $kategori = "Ratecard Estimation";
        $data->nama_file = $request->nama_file;
        $file = $request->file('file');
        if($file != NULL){
            $string = rand(22, 6033);
            $path = 'digital_documents';
            $fileName = $string. '______digitaldocument.'.$file->getClientOriginalExtension();
            $file->move($path, $fileName);
            $data->file = $path.'/'.$fileName;
        }
        $data->save();
        return redirect()->route('admin.digital_documents', ['kategori' => $request->kategori])
        ->with('toast_success', 'Data berhasil ditambahkan');
    }

    public function update($id, Request $request)
    {
        $data = DigitalDocuments::find($id);
        $request->validate([
            'nama_file' => 'required',
            'kategori' => 'required',
            'file' => 'required'
        ]);
        $data->kategori = $request->kategori;
        $data->nama_file = $request->nama_file;
        if($request->kategori == 1) $kategori = "Rules and Regulation";
        else $kategori = "Ratecard Estimation";
        $file = $request->file('file');
        if($file != NULL){
            $file_old = $data->file;
            if(File::exists($file_old)) File::delete($file_old);
            $string = rand(22, 6033);
            $path = 'digital_documents';
            $fileName = $string. '______digitaldocument.'.$file->getClientOriginalExtension();
            $file->move($path, $fileName);
            $data->file = $path.'/'.$fileName;
        }
        $data->save();
        return redirect()->route('admin.digital_documents', ['kategori' => $request->kategori])
        ->with('toast_success', 'Data berhasil diupdate');
    }

    public function destroy($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2){
            $data = DigitalDocuments::find($id);
            if($data->kategori == 1) $kategori = "Rules and Regulation";
            else $kategori = "Ratecard Estimation";
            $file_old = $data->file;
            if(File::exists($file_old)) File::delete($file_old);
            $data->delete();
            return redirect()->route('admin.digital_documents', ['kategori' => $data->kategori])
            ->with('toast_success', 'Data berhasil dihapus');
        }else return redirect()->back()
        ->with('errors', 'Kamu tidak ada akses!');
    }

    public function download($id)
    {
        $data =  DigitalDocuments::find($id);
        $file_old = $data->file;
        if(File::exists($file_old)) {
            $files = public_path().'/'. $data->file;
            $headers = array('Content-Type: application/pdf');
            return response()->download($files, $data->nama_file.'.'.pathinfo($data->file, PATHINFO_EXTENSION), $headers);
        }else return redirect()->back()->with('errors', 'File tidak ditemukan!');
    }
}
