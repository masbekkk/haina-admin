<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Akun;
use App\Models\Days;
use App\Models\Actualpost;
use App\Models\Admin;
use App\Models\Edit;
use App\Models\Followers;
use App\Models\Increasing;
use App\Models\Inventory;
use App\Models\Monthly;
use App\Models\Posting;
use App\Models\Ratio;
use App\Models\Reports;
use App\Models\Script;
use App\Models\Shooting;
use App\Models\Trouble;
use App\Models\views;
use App\Models\EditingRequest;
use App\Models\Finish;
use App\Models\InventoryControl;
use App\Models\Invlastweek;
Use Auth;
Use Alert;

class AkunController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    // public function hzz(){
    //     for($i=1; $i<=15; $i++){
    //         if($i % 2 == 0)
    //         echo "INSERT INTO `peminjaman`(`id_peminjaman`, `petugas_id`, `anggota_id`, `buku_id`, `tgl_pinjam`, `tgl_kembali`) VALUES ($i,$i,$i,$i,'2022-02-02','2002-02-08');<br>";
    //         else 
    //         echo "INSERT INTO `peminjaman`(`id_peminjaman`, `petugas_id`, `anggota_id`, `buku_id`, `tgl_pinjam`, `tgl_kembali`) VALUES ($i,$i,$i,$i,'2022-02-02','2002-02-07');<br>";
    //     }
    // }

    public function count(){
        $data = Akun::all();
        view()->share([
            'data' => $data
        ]);
 
        return view('fitur.count');
    }

    public function index(){
        $data =  Akun::where('user_id', Auth::user()->id)->first();
        if(!empty($data)){
         $data =  Akun::where('user_id', Auth::user()->id)->get();
         $belum = 0;
        }else{
            $belum = 1;
        }
 
        view()->share([
            'data' => $data,
            'belum' => $belum
        ]);
 
        return view('fitur.manageAkun');
 
     }

    public function add(Request $request){
        $data = new Akun();
        $request->validate([
            'nama' => 'required',
            'kategori' => 'required',
            'type' => 'required',
            'subtype' => 'required',
            'link' => 'required'
        ]);
        $data->user_id = Auth::user()->id;
        $data->nama = $request->nama;
        $data->kategori = $request->kategori;
        $data->type = $request->type;
        $data->subtype = $request->subtype;
        $data->link = $request->link;
        $data->save();
        return redirect()->back()->with('toast_success', 'Akun Berhasil ditambahkan!');
    }

    public function edit($id, Request $request){
        $akun =  Akun::where('user_id', Auth::user()->id)->first();
        if(!empty($akun) || Auth::user()->level == 0){
            $data = Akun::find($id);
            $request->validate([
                'nama' => 'required'
            ]);
        
            $data->nama = $request->nama;
            $data->kategori = $request->kategori;
            $data->type = $request->type;
            $data->subtype = $request->subtype;
            $data->link = $request->link;
            $data->save();
            return redirect()->back()->with('toast_success', 'Akun Berhasil ditupdate!');
        }else return redirect()->back()->with('errors', 'Kamu bukan Handler dari akun ini!');
    }

    public function delete($id){
        // $user = Akun::where('user_id', Auth::user()->id)->get();
        $akun = Akun::where('user_id', Auth::user()->id)->first();
        if(!empty($akun) || Auth::user()->level == 0){
            $data = Akun::find($id)->delete();
            \App\Models\Days::where('akun_id', $id)->delete();
            \App\Models\Actualpost::where('akun_id', $id)->delete();
            \App\Models\Admin::where('akun_id', $id)->delete();
            \App\Models\Edit::where('akun_id', $id)->delete();
            \App\Models\Followers::where('akun_id', $id)->delete();
            \App\Models\Increasing::where('akun_id', $id)->delete();
            \App\Models\Inventory::where('akun_id', $id)->delete();
            \App\Models\Monthly::where('akun_id', $id)->delete();
            \App\Models\Posting::where('akun_id', $id)->delete();
            \App\Models\Ratio::where('akun_id', $id)->delete();
            \App\Models\Reports::where('akun_id', $id)->delete();
            \App\Models\Script::where('akun_id', $id)->delete();
            \App\Models\Shooting::where('akun_id', $id)->delete();
            \App\Models\Trouble::where('akun_id', $id)->delete();
            \App\Models\views::where('akun_id', $id)->delete();
            \App\Models\EditingRequest::where('akun_id', $id)->delete();
            \App\Models\Finish::where('akun_id', $id)->delete();
            InventoryControl::where('akun_id', $id)->delete();
            Invlastweek::where('akun_id', $id)->delete();
            return redirect()->back()->with('toast_success', 'Akun Berhasil dihapus!');
        }else return redirect()->back()->with('errors', 'Kamu bukan Handler dari akun ini!');
    }

    public function reset(){
        if(Auth::user()->level == 0){
            $data = Akun::truncate();
            \App\Models\Days::truncate();
            \App\Models\Actualpost::truncate();
            \App\Models\Admin::truncate();
            \App\Models\Edit::truncate();
            \App\Models\Followers::truncate();
            \App\Models\Increasing::truncate();
            \App\Models\Inventory::truncate();
            \App\Models\Monthly::truncate();
            \App\Models\Posting::truncate();
            \App\Models\Ratio::truncate();
            \App\Models\Reports::truncate();
            \App\Models\Script::truncate();
            \App\Models\Shooting::truncate();
            \App\Models\Trouble::truncate();
            \App\Models\views::truncate();

            return redirect()->back()->with('toast_success', 'Berhasil direset!');
        }else return redirect()->back()->with('errors', 'Ngapain?!');
    }
}
