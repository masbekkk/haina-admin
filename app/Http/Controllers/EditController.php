<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EditingRequest;
use App\Models\Akun;
Use Auth;
Use Alert;
use App\Exports\exportEditingreq;
use Maatwebsite\Excel\Facades\Excel;

class EditController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        if(Auth::user()->level == 4 || Auth::user()->level == 0){
            if(Auth::user()->level == 0) $akuns = Akun::all();
            $data = EditingRequest::with('akun', 'acc', 'user')->get();
            $editor = 2; //gausa looping data akun = editor
            $belum_submit = 2; //editor gausa submit
            $data_hasil_submit = 1; //editor gausa submit
        }else{
            $data =  Akun::where('user_id', Auth::user()->id)->first(); //cek list akun yang dipegang
            if(!empty($data)){
             $data =  Akun::where('user_id', Auth::user()->id)->get(); //show list akun yang dipegang
             $hasil_submit = EditingRequest::where('user_id', Auth::user()->id)->with('akun', 'acc')->first(); //cek hasil submit request
             if(!empty($hasil_submit)){
                $data_hasil_submit = EditingRequest::where('user_id', Auth::user()->id)->with('akun', 'acc')->orderBy('created_at', 'desc')->get(); //show hasil submit
                $belum_submit = 0; //sudah submit
             }else {
                 $belum_submit = 1; //belum submit
                 $data_hasil_submit = 0;
             }
            $editor = 0; //bukan editor
            }else{
                $editor = 1; //tidak punya akun
                $belum_submit = 1; //belum submit
                $data_hasil_submit = 0;
            }
        }
        if(Auth::user()->level == 0){
            view()->share([
                'akuns' => $akuns,
                'data' => $data,
                'editor' => $editor,
                'belum_submit' => $belum_submit,
                'data_hasil_submit' => $data_hasil_submit
            ]);
        }else {
            view()->share([
                'data' => $data,
                'editor' => $editor,
                'belum_submit' => $belum_submit,
                'data_hasil_submit' => $data_hasil_submit
            ]);
        }

        return view('fitur.editreq');
    }

    public function create($id)
    {
        $data = Akun::find($id);
        view()->share([
            'data' => $data
        ]);

        return view('fitur.editreq.add');
    }

    public function edit($id)
    {
        $data = Akun::find($id);
        $req = EditingRequest::with('akun')->find($id);
       
        view()->share([
            'data' => $data,
            'req' => $req
        ]);

        return view('fitur.editreq.edit');
    }

    public function store($id, Request $request){
        $request->validate([
            'concept' => 'required',
            'estimeted_posting' => 'required'
        ]);
        $data = new EditingRequest();
        $data->user_id = Auth::user()->id;
        $data->akun_id = $id;
        $data->concept = $request->concept;
        $data->estimeted_posting = $request->estimeted_posting;
        
        $data->save();
        return redirect()->route('editReq')
        ->with('toast_success', 'Editing Request berhasil ditambahkan!');
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'concept' => 'required',
            'estimeted_posting' => 'required'
        ]);
        $data = EditingRequest::find($id);
        $data->concept = $request->concept;
        $data->estimeted_posting = $request->estimeted_posting;
        
        $data->save();
        return redirect()->route('editReq')
        ->with('toast_success', 'Editing Request berhasil diupdate!');
    }
    //acc =2, decline = 1, hold = 3, waiting = 4, trouble=5, finish = 6 
    public function finish($id)
    {
        if(Auth::user()->level == 4 || Auth::user()->level == 0){
            $data = EditingRequest::find($id);
            $data->acc_by = Auth::user()->id;
            $data->status = 6;
            $data->save();
            return redirect()->route('editReq')
            ->with('toast_success', 'Editing Request berhasil Finish!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }
    public function trouble($id)
    {
        if(Auth::user()->level == 4 || Auth::user()->level == 0){
            $data = EditingRequest::find($id);
            $data->acc_by = Auth::user()->id;
            $data->status = 5;
            $data->save();
            return redirect()->route('editReq')
            ->with('toast_success', 'Editing Request berhasil diubah statusnya!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }
    public function waiting($id)
    {
        if(Auth::user()->level == 4 || Auth::user()->level == 0){
            $data = EditingRequest::find($id);
            $data->acc_by = Auth::user()->id;
            $data->status = 4;
            $data->save();
            return redirect()->route('editReq')
            ->with('toast_success', 'Editing Request berhasil diubah statusnya!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }
    public function hold($id)
    {
        if(Auth::user()->level == 4 || Auth::user()->level == 0){
            $data = EditingRequest::find($id);
            $data->acc_by = Auth::user()->id;
            $data->status = 3;
            $data->save();
            return redirect()->route('editReq')
            ->with('toast_success', 'Editing Request berhasil diubah statusnya!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }
    public function accept($id)
    {
        if(Auth::user()->level == 4 || Auth::user()->level == 0){
            $data = EditingRequest::find($id);
            $data->acc_by = Auth::user()->id;
            $data->status = 2;
            $data->save();
            return redirect()->route('editReq')
            ->with('toast_success', 'Editing Request berhasil diaccept!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function decline($id)
    {
        if(Auth::user()->level == 4 || Auth::user()->level == 0){
            $data = EditingRequest::find($id);
            $data->acc_by = Auth::user()->id;
            $data->status = 1;
            $data->save();
            return redirect()->route('editReq')
            ->with('toast_success', 'Editing Request telah ditolak!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function delete($id)
    {
        $data = Akun::where('user_id', Auth::user()->id)->first();
        if(!empty($data)){
            $data = EditingRequest::find($id)->delete();
            return redirect()->route('editReq')
            ->with('toast_success', 'Editing Request berhasil dihapus!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function management(){
        $data = EditingRequest::with('akun', 'acc', 'user')->get();
        view()->share([
            'data' => $data
        ]);
        return view('management.fitur.editreq');
    }

    public function export()
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = EditingRequest::with('akun', 'acc', 'user')->get();
            // dd($data);
            return Excel::download(new exportEditingreq(), 'Rekap Editing Request by '.Auth::user()->name.'.xlsx');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');
    }

    public function reset()
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            EditingRequest::truncate();
            return redirect()->route('editReq')
            ->with('toast_success', 'Editing Request berhasil direset!');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');
    }

}
