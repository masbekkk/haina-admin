<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Propertyloan;
use App\Models\Propertyinv;
use App\Models\User;
Use Auth;
Use Alert;

class PropertyloanController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $ada = 2;
        if(Auth::user()->level == 2 || Auth::user()->level == 0) $data = Propertyloan::with('user', 'property')->get();
        else {
            $data = Propertyloan::where('user_id', Auth::user()->id)->first();
            $ada = 1;
            if(!empty($data)) $data = Propertyloan::where('user_id', Auth::user()->id)->get();
            else $ada = 0;
        }
            view()->share([
                'data' => $data,
                'ada' => $ada
            ]);
        return view('admin.propertyloan.propertyloan');
        
    }

    public function create(){
        // if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $property = Propertyinv::all();
            view()->share([
                'property' => $property
            ]);
            return view('admin.propertyloan.add');
        // }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function store(Request $request){
        $data = new Propertyloan();
        $request->validate([
            'kebutuhan' => 'required',
            'tanggal_pinjam' => 'required',
            'tanggal_kembali' => 'required',
            'property' => 'required'
        ]);

        $data->user_id = Auth::user()->id;
        $data->kebutuhan = $request->kebutuhan;
        $data->tanggal_pinjam = $request->tanggal_pinjam;
        $data->tanggal_kembali = $request->tanggal_kembali;
        $data->property_id = $request->property;
        // $data->status = $request->status;
        $data->save();
        return redirect()->route('admin.Propertyloan')
        ->with('toast_success', 'Propertyloan Berhasl ditambahkan!');
    }

    public function edit($id){
        // if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Propertyloan::find($id);
            $property = Propertyinv::all();
            view()->share([
                'data' => $data,
                'property' => $property
            ]);
            return view('admin.propertyloan.edit');
        // }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function update($id, Request $request)
    {
        $data = Propertyloan::find($id);
        $request->validate([
            'kebutuhan' => 'required',
            'tanggal_pinjam' => 'required',
            'tanggal_kembali' => 'required',
            'property' => 'required'
        ]);

        $data->user_id = Auth::user()->id;
        $data->kebutuhan = $request->kebutuhan;
        $data->tanggal_pinjam = $request->tanggal_pinjam;
        $data->tanggal_kembali = $request->tanggal_kembali;
        $data->property_id = $request->property;
        $data->save();
        return redirect()->route('admin.Propertyloan')
        ->with('toast_success', 'Propertyloan Berhasl diupdate!');
    }

    public function delete($id)
    {
        if(Auth::user()->level == 2 || Auth::user()->level == 0){
            $data = Propertyloan::find($id)->delete();
            return redirect()->route('admin.Propertyloan')
            ->with('toast_success', 'Propertyloan Berhasil dihapus!');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    public function done($id)
    {
        $data = Propertyloan::find($id);
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data->status = 2;
            $data->save();
            return redirect()->route('admin.Propertyloan')
            ->with('toast_success', 'Propertyloan Berhasil diubah status!');
        }
        else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
        
    }

    public function on_progress($id)
    {
        $data = Propertyloan::find($id);
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data->status = 1;
            $data->save();
            return redirect()->route('admin.Propertyloan')
            ->with('toast_success', 'Propertyloan Berhasil diubah status!');
        }
        else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
        
    }

    public function management(){
        $data = Propertyloan::with('user', 'property')->get();
        view()->share([
            'data' => $data
        ]);
        return view('management.admin.propertyloan.propertyloan');
        
    }
}
