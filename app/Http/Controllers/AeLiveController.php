<?php

namespace App\Http\Controllers;

use App\Models\AeLive;
use Illuminate\Http\Request;
Use Auth;
Use Alert;

class AeLiveController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('aelive.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new AeLive();
        $request->validate([
            'platforms' => 'required',
            'live_start' => 'required',
            'live_end' => 'required'
        ]);

        $data->platforms = $request->platforms;
        $data->live_start = $request->live_start;
        $data->live_end = $request->live_end;
        $data->link_live = $request->link_live;
        $data->result_audience = $request->result_audience;
        $data->result_selling = $request->result_selling;
        $data->save();
        return redirect()->route('ae-live')
            ->with('toast_success', 'AE Live Berhasl ditambahkan!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AeLive  $aeLive
     * @return \Illuminate\Http\Response
     */
    public function show(AeLive $aeLive)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 5){
            // $aeLive->get();
            $aeLive = AeLive::all();
            view()->share([
                'data' => $aeLive
            ]);

            return view('aelive.aelive');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AeLive  $aeLive
     * @return \Illuminate\Http\Response
     */
    public function edit($id, AeLive $aeLive)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 5){
            $data = AeLive::findOrFail($id);
            view()->share([
                'data' => $data
            ]);

            return view('aelive.edit');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AeLive  $aeLive
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, AeLive $aeLive)
    {
        $request->validate([
            'platforms' => 'required'
        ]);
        $data = AeLive::findOrFail($id);
        $data->platforms = $request->platforms;
        $data->live_start = $request->live_start;
        $data->live_end = $request->live_end;
        $data->link_live = $request->link_live;
        $data->result_audience = $request->result_audience;
        $data->result_selling = $request->result_selling;
        $data->save();
        return redirect()->route('ae-live')
            ->with('toast_success', 'AE Live Berhasl diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AeLive  $aeLive
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, AeLive $aeLive)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 5){
            $data = AeLive::findOrFail($id);
            $data->delete();
            return redirect()->route('ae-live')
            ->with('toast_success', 'AE Live Berhasl dihapus!');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }

    public function management(){
        if(Auth::user()->level == 6){
            // $aeLive->get();
            $aeLive = AeLive::all();
            view()->share([
                'data' => $aeLive
            ]);

            return view('aelive.management');
        }else return redirect()->back()->with('errors', 'Kamu tidak punya akses!');
    }
}
