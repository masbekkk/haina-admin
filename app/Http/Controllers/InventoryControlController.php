<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Akun;
use App\Models\Invlastweek;
use App\Models\Finish;
use App\Models\Posting;
use App\Models\Inventory;
use App\Models\InventoryControl;
Use Auth;
Use Alert;
use App\Exports\exportDailyinventory;
use Maatwebsite\Excel\Facades\Excel;

class InventoryControlController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function view(){
        if(Auth::user()->level == 0 || Auth::user()->level == 2){
            $data = Akun::with('user')->get();
            $belum = 0;
        }else {
            $data = Akun::where('user_id', Auth::user()->id)->first();
            if(!empty($data)){
                $belum =0;
                $data = Akun::where('user_id', Auth::user()->id)->get();
            } 
            else $belum =1;
        }
            view()->share([
                'data' => $data,
                'belum' => $belum
            ]);
            return view('manage.inventorycontrol');

    }

    public function manage($id){
        $Invlastweek = Invlastweek::where('akun_id', $id)->first();
        $Finish = Finish::where('akun_id', $id)->first();
        $Posting = Posting::where('akun_id', $id)->first();
        $Inventory = Inventory::where('akun_id', $id)->first();
        if(!empty($Inventory) && !empty($Posting)) $bisa = 1;
        else $bisa = 0;
        $day = InventoryControl::where('akun_id', $id)->first();
        if(!empty($day)) $belum = 0;
        else $belum = 1;
        
        if(Auth::user()->level == 0 || Auth::user()->level == 2) $data = Akun::find($id);
        else{
            $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            if(!empty($data)) $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            else return redirect()->back()->with('errors', 'Kamu bukan Handler dari akun ini!');
        }
        view()->share([
            'data' => $data,
            'ada' => $belum,
            'bisa' => $bisa,
            'Invlastweek' => $Invlastweek,
            'Finish' => $Finish,
            'posting' => $Posting,
            'Inventory' => $Inventory
        ]);

        return view('fitur.inventorycontrol');
    }

    public function add($id, Request $request)
    {
        $data = new InventoryControl();
        $data->akun_id = $id;
        // $data->fitur_id = 0;
        $data->save();
        $Invlastweek = new Invlastweek();
        $Finish = new Finish();
        $posting = Posting::where('akun_id', $id)->first();
        $Inventory = Inventory::where('akun_id', $id)->first();

        $Invlastweek->akun_id = $id;
        $Invlastweek->senin = $request->Invlastweek1;
        $Invlastweek->selasa = $request->Invlastweek2;
        $Invlastweek->rabu = $request->Invlastweek3;
        $Invlastweek->kamis = $request->Invlastweek4;
        $Invlastweek->jumat = $request->Invlastweek5;
        $Invlastweek->sabtu = $request->Invlastweek6;
        $Invlastweek->save();

        //Finish
        $Finish->akun_id = $id;
        $Finish->senin = $request->Finish1;
        $Finish->selasa = $request->Finish2;
        $Finish->rabu = $request->Finish3;
        $Finish->kamis = $request->Finish4;
        $Finish->jumat = $request->Finish5;
        $Finish->sabtu = $request->Finish6;
        $Finish->save();

        //posting
        $posting->akun_id = $id;
        $posting->senin = $request->posting1;
        $posting->selasa = $request->posting2;
        $posting->rabu = $request->posting3;
        $posting->kamis = $request->posting4;
        $posting->jumat = $request->posting5;
        $posting->sabtu = $request->posting6;
        $posting->save();

        //Inventory
        $Inventory->akun_id = $id;
        $Inventory->senin = $request->Inventory1;
        $Inventory->selasa = $request->Inventory2;
        $Inventory->rabu = $request->Inventory3;
        $Inventory->kamis = $request->Inventory4;
        $Inventory->jumat = $request->Inventory5;
        $Inventory->sabtu = $request->Inventory6;
        $Inventory->save();
        return redirect()->route('manage.inventorycontrol')
        ->with('toast_success', 'Inventory Report berhasil ditambahkan!');
    }

    public function update($id, Request $request)
    {
        $Invlastweek = Invlastweek::where('akun_id', $id)->first();
        $Finish = Finish::where('akun_id', $id)->first();
        $posting = Posting::where('akun_id', $id)->first();
        $Inventory = Inventory::where('akun_id', $id)->first();
        
        $Invlastweek->akun_id = $id;
        $Invlastweek->senin = $request->Invlastweek1;
        $Invlastweek->selasa = $request->Invlastweek2;
        $Invlastweek->rabu = $request->Invlastweek3;
        $Invlastweek->kamis = $request->Invlastweek4;
        $Invlastweek->jumat = $request->Invlastweek5;
        $Invlastweek->sabtu = $request->Invlastweek6;
        $Invlastweek->save();

        //Finish
        $Finish->akun_id = $id;
        $Finish->senin = $request->Finish1;
        $Finish->selasa = $request->Finish2;
        $Finish->rabu = $request->Finish3;
        $Finish->kamis = $request->Finish4;
        $Finish->jumat = $request->Finish5;
        $Finish->sabtu = $request->Finish6;
        $Finish->save();

        //posting
        $posting->akun_id = $id;
        $posting->senin = $request->posting1;
        $posting->selasa = $request->posting2;
        $posting->rabu = $request->posting3;
        $posting->kamis = $request->posting4;
        $posting->jumat = $request->posting5;
        $posting->sabtu = $request->posting6;
        $posting->save();

        //Inventory
        $Inventory->akun_id = $id;
        $Inventory->senin = $request->Inventory1;
        $Inventory->selasa = $request->Inventory2;
        $Inventory->rabu = $request->Inventory3;
        $Inventory->kamis = $request->Inventory4;
        $Inventory->jumat = $request->Inventory5;
        $Inventory->sabtu = $request->Inventory6;
        $Inventory->save();
        
        return redirect()->route('manage.inventorycontrol')
        ->with('toast_success', 'Inventory Report berhasil diupdate!');
    }

    public function viewmanagement(){
        if(Auth::user()->level == 6){
            $data = Akun::with('user')->get();
            $belum = 0;
        }else {
            $data = Akun::where('user_id', Auth::user()->id)->first();
            if(!empty($data)){
                $belum =0;
                $data = Akun::where('user_id', Auth::user()->id)->get();
            } 
            else $belum =1;
        }
            view()->share([
                'data' => $data,
                'belum' => $belum
            ]);
            return view('management.manage.inventorycontrol');

    }

    public function management($id){
        $Invlastweek = Invlastweek::where('akun_id', $id)->first();
        $Finish = Finish::where('akun_id', $id)->first();
        $Posting = Posting::where('akun_id', $id)->first();
        $Inventory = Inventory::where('akun_id', $id)->first();
        if(!empty($Inventory) && !empty($Posting)) $bisa = 1;
        else $bisa = 0;
        $day = InventoryControl::where('akun_id', $id)->first();
        if(!empty($day)) $belum = 0;
        else $belum = 1;
        
        if(Auth::user()->level == 6) $data = Akun::find($id);
        else{
            $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            if(!empty($data)) $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
            else return redirect()->back()->with('errors', 'Kamu bukan Handler dari akun ini!');
        }
        view()->share([
            'data' => $data,
            'ada' => $belum,
            'bisa' => $bisa,
            'Invlastweek' => $Invlastweek,
            'Finish' => $Finish,
            'posting' => $Posting,
            'Inventory' => $Inventory
        ]);

        return view('management.fitur.inventorycontrol');
    }

    public function export($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = Akun::find($id);
            return Excel::download(new exportDailyinventory($id), 'Rekap Daily Inventory '.$data->nama.'.xlsx');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');
    }

    public function reset($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {   
            $Invlastweek = Invlastweek::where('akun_id', $id)->delete();
            $Finish = Finish::where('akun_id', $id)->delete();
            $Posting = Posting::where('akun_id', $id)->delete();
            $Inventory = Inventory::where('akun_id', $id)->delete();
            return redirect()->route('manage.inventorycontrol')
            ->with('toast_success', 'Daily Inventory berhasil direset!');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');

    }

}
