<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Akun;
use App\Models\Trouble;
Use Auth;
Use Alert;
use App\Exports\exportDailytroubles;
use Maatwebsite\Excel\Facades\Excel;

class TroubleController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function view(){
        if(Auth::user()->level == 0 || Auth::user()->level == 2){
            $data = Akun::with('user')->get();
            $belum = 0;
        }else {
            $data = Akun::where('user_id', Auth::user()->id)->first();
            if(!empty($data)){
                $belum =0;
                $data = Akun::where('user_id', Auth::user()->id)->get();
            } 
            else $belum =1;
        }
            view()->share([
                'data' => $data,
                'belum' => $belum
            ]);
            return view('manage.dailytroubles');

    }


    public function index($id)
    {
        $data = Trouble::where('akun_id', $id)->with('akun')->first();
        $akun = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
        if(!empty($data) && !empty($akun)) $belum = 1;
        else {
            if(Auth::user()->level == 0 || Auth::user()->level == 2) $data = Akun::find($id);
            else{
                $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
                if(!empty($data)) $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
                else return redirect()->back()->with('errors', 'Kamu bukan Handler dari akun ini!');
            }
            $belum = 0;
        }
        
        view()->share([
            'data' => $data,
            'belum' => $belum
        ]);

        return view('fitur.trouble');
    }

    public function add($id, Request $request)
    {
        $data = new Trouble();
        $data->akun_id = $id;
        $data->senin = $request->trouble1;
        $data->selasa = $request->trouble2;
        $data->rabu = $request->trouble3;
        $data->kamis = $request->trouble4;
        $data->jumat = $request->trouble5;
        $data->sabtu = $request->trouble6;
        $data->save();
        return redirect()->route('manage.dailytroubles')->with('toast_success', 'Data Berhasil ditambahkan!');

    }

    public function update($id, Request $request)
    {
        $data = Trouble::where('akun_id', $id)->first();
        // $data->akun_id = $id;
        $data->senin = $request->trouble1;
        $data->selasa = $request->trouble2;
        $data->rabu = $request->trouble3;
        $data->kamis = $request->trouble4;
        $data->jumat = $request->trouble5;
        $data->sabtu = $request->trouble6;
        $data->update();
        return redirect()->route('manage.dailytroubles')->with('toast_success', 'Data Berhasil diperbarui!');

    }

    public function viewmanagement(){
        if(Auth::user()->level == 6){
            $data = Akun::with('user')->get();
            $belum = 0;
        }else {
            $data = Akun::where('user_id', Auth::user()->id)->first();
            if(!empty($data)){
                $belum =0;
                $data = Akun::where('user_id', Auth::user()->id)->get();
            } 
            else $belum =1;
        }
            view()->share([
                'data' => $data,
                'belum' => $belum
            ]);
            return view('management.manage.dailytroubles');

    }

    public function management($id)
    {
        $data = Trouble::where('akun_id', $id)->with('akun')->first();
        $akun = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
        if(!empty($data) && !empty($akun)) $belum = 1;
        else {
            if(Auth::user()->level == 6) $data = Akun::find($id);
            else{
                $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
                if(!empty($data)) $data = Akun::where('user_id', Auth::user()->id)->where('id', $id)->first();
                else return redirect()->back()->with('errors', 'Kamu bukan Handler dari akun ini!');
            }
            $belum = 0;
        }
        
        view()->share([
            'data' => $data,
            'belum' => $belum
        ]);

        return view('management.fitur.trouble');
    }

    public function export($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = Akun::find($id);
            return Excel::download(new exportDailytroubles($id), 'Rekap Daily Trouble '.$data->nama.'.xlsx');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');
    }

    public function reset($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {   
            $trouble = Trouble::where('akun_id', $id)->delete();
            return redirect()->route('manage.dailytroubles')
            ->with('toast_success', 'Daily Trouble berhasil direset!');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');
    }
}
