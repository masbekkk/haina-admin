<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EditorReport;
use App\Models\Akun;
Use Auth;
Use Alert;

class EditorReportsController extends Controller
{
    public function __construct()
    {
        if($this->middleware('auth')){}
        
    }

    public function index()
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 4 ){
            if(Auth::user()->level == 0 || Auth::user()->level == 2){
                $ada = 1;
                $data = EditorReport::all();
            }else{
                $data = EditorReport::where('editor_id', Auth::user()->id)->first();
                if(!empty($data)){
                    $ada = 1;
                    $data = EditorReport::where('editor_id', Auth::user()->id)->get();
                }else $ada = 0;
            }
            view()->share([
                'data' => $data,
                'ada' => $ada
            ]);
            return view('editor_reports.index');
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');   
        
    }
    public function create()
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 4 ){    
            $akun = Akun::all();
            view()->share([
                'akun' => $akun
            ]);

            return view('editor_reports.create');
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');   
    }

    public function store(Request $request)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 4 ){    
            $request->validate([
                'date_of_report' => 'required',
                'editing_accounts' => 'required',
                'numbers_of_editing' => 'required',
                'troubles' => 'required'
            ]);
            $data = new EditorReport();
            $data->editor_id = Auth::user()->id;
            $data->date_of_report = $request->date_of_report;
            // $editing_akun = $request->input()
            $data->editing_accounts = implode(',', $request->editing_accounts);
            $data->numbers_of_editing = $request->numbers_of_editing;
            $data->troubles = $request->troubles;
            $data->save();
            return redirect()->route('editor-reports')
            ->with('toast_success', 'Editor Report berhasil ditambahkan!');
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');   
    }

    public function edit($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 4 ){    
            $akun = Akun::all();
            $data = EditorReport::find($id);
            view()->share([
                'akun' => $akun,
                'data' => $data
            ]);

            return view('editor_reports.edit');
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');   
    }

    public function update($id, Request $request)
    {
         
        if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 4 ){       
            $request->validate([
                'date_of_report' => 'required',
                // 'editing_accounts' => 'required',
                'numbers_of_editing' => 'required',
                'troubles' => 'required'
            ]);
            $data = EditorReport::find($id);
            $data->editor_id = Auth::user()->id;
            $data->date_of_report = $request->date_of_report;
            if($request->editing_accounts != NULL)
            $data->editing_accounts = implode(',', $request->editing_accounts);
            $data->numbers_of_editing = $request->numbers_of_editing;
            $data->troubles = $request->troubles;
            $data->save();
            return redirect()->route('editor-reports')
            ->with('toast_success', 'Editor Report berhasil diupdate!');
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');   
    }

    public function destroy($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 4 ){       
            $data = EditorReport::find($id)->delete();
            return redirect()->route('editor-reports')
            ->with('toast_success', 'Editor Report berhasil dihapus!');
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');   
    }

    public function management()
    {
        if(Auth::user()->level == 6){
            $data = EditorReport::all();
            view()->share([
                'data' => $data
            ]);
            return view('management.editor_reports.index');
        }else return redirect()->back()->with('errors', 'Tidak ada akses!');   
    }
}
