<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
Use Alert;
use DataTables;
use Illuminate\Support\Facades\Cache;
use App\Models\Akun;
// Use Location;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function addImage(Request $request)
    {
        $data = User::find(Auth::user()->id);
        $request->validate([
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp,heic,heif,hevc,pdf|max:5048'
        ]);
        $photo = $request->file('foto');
        $path = 'foto_profil';
        $string = rand(22,5033);

        if ($photo != null) {
            $fileName = $string .'___foto.'.$photo->getClientOriginalExtension();
            $photo->move($path, $fileName);
            $data->foto = $path . '/' . $fileName;
        }
        $data->email = $request->email;
        $data->name = $request->name;
        $data->update();
        Alert::success('Foto Profil Kamu berhasil diperbarui', 'Success Message');
        // toast('Foto Profil Kamu berhasil diperbarui','success');
        return redirect()->route('home');
        // ->with('toast_success', 'Foto Profil Kamu berhasil diperbarui');
    }

    public function chgPwd(Request $request){
        $data = User::find(Auth::user()->id);

        $request->validate([
            'pwd_lama' => 'required|password|min:8',
            'pwd_baru' => 'required|min:8',
            'pwd_conf' => 'required|same:pwd_baru'
        ]);
       
        if(Hash::check($request->pwd_lama, Auth::user()->password)){
            $data->password = Hash::make($request->pwd_conf);
            $data->update();
            Alert::success('Password Berhasil Diubah', 'yy');
        }else{
            Alert::error('Ada error di passwordmu', 'yy');
        }  

        return redirect()->route('home');
        
    }

    public function showUser(Request $request){
        if(Auth::user()->level == 0){
            $data = User::with('role')->get();
            // if($request->ajax()) {
            //     $data = User::all();
            //     return Datatables::of($data)
            //     ->editColumn("created_at", function ($data) {
            //         return date("m/d/Y", strtotime($data->created_at));
            //     })
            //     ->addColumn('Aksi', function ($data) {
            //         $update = '<a href="/admin/change-role-user/Auth::user()->id/1" class="btn btn-primary">Make as Creator</a>&nbsp;<a href="/admin/change-role-user/Auth::user()->id/1" class="btn btn-info">Make as GA</a>';
            //         return $update;
            //     })
            //     ->rawColumns(['Aksi'])
            //     ->make(true);
            view()->share([
                'data' => $data
            ]);
            
        }else Alert::toast('Kamu bukan admin!', 'error');

        return view('admin.users');
    }

    public function indexUser(){
        return view('admin.users');
    }

    public function chgRoleUser($id, $level){
        if(Auth::user()->level == 0){
            $data = User::find($id);
            $data->level = $level;
            $data->update();
            Alert::toast('Role User '.$data->name. ' Berhasil diubah', 'success');
        }else{
            Alert::toast('Kamu bukan admin!', 'error');
        }
        return redirect()->route('admin.users');
    }

    public function delete($id)
    {
        if(Auth::user()->level == 0){
            $akun = Akun::where('user_id', $id)->first();
            if(!empty($akun->id)){
                \App\Models\Days::where('akun_id', $akun->id)->delete();
                \App\Models\Actualpost::where('akun_id', $akun->id)->delete();
                \App\Models\Admin::where('akun_id', $akun->id)->delete();
                \App\Models\Edit::where('akun_id', $akun->id)->delete();
                \App\Models\Followers::where('akun_id', $akun->id)->delete();
                \App\Models\Increasing::where('akun_id', $akun->id)->delete();
                \App\Models\Inventory::where('akun_id', $akun->id)->delete();
                \App\Models\Monthly::where('akun_id', $akun->id)->delete();
                \App\Models\Posting::where('akun_id', $akun->id)->delete();
                \App\Models\Ratio::where('akun_id', $akun->id)->delete();
                \App\Models\Reports::where('akun_id', $akun->id)->delete();
                \App\Models\Script::where('akun_id', $akun->id)->delete();
                \App\Models\Shooting::where('akun_id', $akun->id)->delete();
                \App\Models\Trouble::where('akun_id', $akun->id)->delete();
                \App\Models\views::where('akun_id', $akun->id)->delete();
                \App\Models\EditingRequest::where('akun_id', $akun->id)->delete();
                \App\Models\Finish::where('akun_id', $akun->id)->delete();
                \App\Models\InventoryControl::where('akun_id', $akun->id)->delete();  
                \App\Models\Invlastweek::where('akun_id', $akun->id)->delete();
            }
            \App\Models\EditingRequest::where('user_id', $id)->orWhere('acc_by', $id)->delete();
            \App\Models\Income::where('user_id', $id)->delete();
            \App\Models\Cooperation::where('user_id', $id)->delete();
            \App\Models\Propertyloan::where('user_id', $id)->delete();
            Akun::where('user_id', $id)->delete();
            User::find($id)->delete();
            return redirect()->route('admin.users')
            ->with('toast_success', 'Data Pengguna Berhasil dihapus!');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin!');
    }

    // public function lokasi(){
    //     $data = Location::get('192.168.1.1');
    //     view()->share([
    //         'data' => $data
    //     ]);

    //     return view('lokasi');
    // }
}

