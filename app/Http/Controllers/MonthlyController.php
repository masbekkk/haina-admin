<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Monthly;
use App\Models\Akun;
Use Alert;
Use Auth;
use App\Exports\exportMonthlyprogress;
use Maatwebsite\Excel\Facades\Excel;

class MonthlyController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function view(){
        if(Auth::user()->level == 0 || Auth::user()->level == 2){
            $data = Akun::with('user')->get();
            view()->share([
                'data' => $data
            ]);
            return view('manage.monthly');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');

    }
    public function ae(){
        if(Auth::user()->level == 0 || Auth::user()->level == 2){
            $data = Akun::with('user')->get();
            view()->share([
                'data' => $data
            ]);
            return view('manage.ae');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');

    }
    public function index($id){
        if(Auth::user()->level == 0 || Auth::user()->level == 2){
        $data = Monthly::where('akun_id', $id)->first();
        $akun = Akun::find($id);
        if(!empty($data)) $belum = 0;
        else $belum = 1;
        view()->share([
            'akun' => $akun,
            'data' => $data,
            'ada' => $belum
        ]);
        Alert::info('Isi Target dulu baru Progress ', 'Silahkan mengisi Target kemudian baru bisa mengisi Progressnya ya!');
        return view('fitur.monthly');
        }else return redirect()->back()->with('errors', 'Kamu bukan Admin atau GA!');
    }

    // public function store($id, Request $request)
    // {
        // $data = new Monthly();
        // $data->target1 = $request->target1;
        // $data->target2 = $request->target2;
        // $data->target3 = $request->target3;
        // $data->target4 = $request->target4;
        // $data->progress1 = $request->progress1;
        // $data->progress2 = $request->progress2;
        // $data->progress3 = $request->progress3;
        // $data->progress4 = $request->progress4;
        // $data->save();
        // return redirect()->route('manage.monthly')
        // ->with('toast_success', 'Target berhasil berhasil ditambahkan!');

    // }

    public function update($id, Request $request)
    {
        $data = Monthly::where('akun_id', $id)->first();
        if(!empty($data)){
            $data = Monthly::where('akun_id', $id)->first();
            $data->target1 = $request->target1;
            $data->target2 = $request->target2;
            $data->target3 = $request->target3;
            $data->target4 = $request->target4;
            $data->progress1 = $request->progress1;
            $data->progress2 = $request->progress2;
            $data->progress3 = $request->progress3;
            $data->progress4 = $request->progress4;
            $data->save();
            return redirect()->route('manage.monthly')
            ->with('toast_success', 'Progress berhasil berhasil diupdate!');
        }else{
            $data = new Monthly();
            $data->akun_id = $id;
            $data->target1 = $request->target1;
            $data->target2 = $request->target2;
            $data->target3 = $request->target3;
            $data->target4 = $request->target4;
            $data->progress1 = $request->progress1;
            $data->progress2 = $request->progress2;
            $data->progress3 = $request->progress3;
            $data->progress4 = $request->progress4;
            $data->save();
            return redirect()->route('manage.monthly')
            ->with('toast_success', 'Target berhasil berhasil ditambahkan!');
        }

    }

    public function viewmanagement(){
        $data = Akun::with('user')->get();
        view()->share([
            'data' => $data
        ]);
        return view('management.manage.monthly');
    }
    public function viewmanagementae(){
        $data = Akun::with('user')->get();
        view()->share([
            'data' => $data
        ]);
        return view('management.manage.ae');
    }

    public function management($id){
        $data = Monthly::where('akun_id', $id)->first();
        $akun = Akun::find($id);
        if(!empty($data)) $belum = 0;
        else $belum = 1;
        view()->share([
            'akun' => $akun,
            'data' => $data,
            'ada' => $belum
        ]);
        return view('management.fitur.monthly');
    }

    public function export($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {
            $data = Akun::find($id);
            return Excel::download(new exportMonthlyprogress($id), 'Rekap Monthly Progress '.$data->nama.'.xlsx');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');
    }

    public function reset($id)
    {
        if(Auth::user()->level == 0 || Auth::user()->level == 2) {   
            $trouble = Monthly::where('akun_id', $id)->delete();
            $akun = Akun::find($id);
            if($akun->kategori == "Account Executive")
            return redirect()->route('manage.account-executive')
            ->with('toast_success', 'Monthly Progress berhasil direset!');
            else return redirect()->route('manage.monthly')
            ->with('toast_success', 'Monthly Progress berhasil direset!');
        }else return redirect()->back()->with('errors', 'Kamu tidak ada akses!');
    }

}