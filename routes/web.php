<?php

use Illuminate\Support\Facades\Route;
// Use Auth;
// Use Alert;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/export', function () {
    return view('exports.dailyworks');
})->name('export');

Route::get('/tes', function () {
    // echo 5 % 0.25;
    // echo "INSERT INTO `Peminjaman_1514`(`id_peminjaman_1514`, `petugas_id_1514`, `buku_id_1514`, `tgl_pinjam_1514`, `tgl_kembali_1514`) VALUES <br>";
    // for($i=1; $i<=15; $i++){
    //     if($i % 2 == 0)
    //     echo "($i,$i,$i,'2022-02-17','2022-02-24'),<br>";
    //     else 
    //     echo "($i,$i,$i,'2022-02-10','2022-02-17'),<br>";
    // }
    $coba = "oke.sql";
    echo pathinfo($coba, PATHINFO_EXTENSION);
});

Route::get('/scan', function () {
    return view('scan');
});

Route::get('/scan2', function () {
    return view('scan2');
});

//presensi 
Route::get('/presensi', [App\Http\Controllers\PresensiController::class, 'index'])->name('presensi');
Route::get('/presensi/wfh', function(){
    if(Auth::user()->status != 0) return redirect()->back()->with('errors', 'Kamu sudah Presensi!');
    else
    return view('presensi.wfh');
})->name('presensi.wfh');
Route::get('/presensi/wfo', function(){
    if(Auth::user()->status != 0) return redirect()->back()->with('errors', 'Kamu sudah Presensi!');
    else
    return view('presensi.wfo');
})->name('presensi.wfo');

Route::get('/presensi/keluar', function(){
    if(Auth::user()->status == 2) return view('presensi.keluar.wfh');
    elseif(Auth::user()->status == 1) return view('presensi.keluar.wfo');
    else
    return redirect()->back()->with('errors', 'Kamu belum Presensi Masuk!');
})->name('presensi.out');

Route::post('/store/presensi/masuk/{status}', [App\Http\Controllers\PresensiController::class, 'presensiIn'])->name('store.presensi_in');
Route::post('/store/presensi/keluar/{status}', [App\Http\Controllers\PresensiController::class, 'presensiOut'])->name('store.presensi_out');

//download file
Route::get('/download-digital-documents/{id}', [App\Http\Controllers\DigitalDocumentsController::class, 'download'])->name('download.digital_documents');

Route::get('/digital-documents/{kategori}', [App\Http\Controllers\DigitalDocumentsController::class, 'index'])->name('digital_documents');

  //Editor Reports
  Route::get('/EditorReports', [App\Http\Controllers\EditorReportsController::class, 'index'])->name('editor-reports');
  Route::get('/add/EditorReports', [App\Http\Controllers\EditorReportsController::class, 'create'])->name('add.editor-reports');
  Route::post('/store/EditorReports', [App\Http\Controllers\EditorReportsController::class, 'store'])->name('store.editor-reports');
  Route::get('/edit/EditorReports/{id}', [App\Http\Controllers\EditorReportsController::class, 'edit'])->name('edit.editor-reports');
  Route::put('/update/EditorReports/{id}', [App\Http\Controllers\EditorReportsController::class, 'update'])->name('update.editor-reports');
  Route::get('/delete/EditorReports/{id}', [App\Http\Controllers\EditorReportsController::class, 'destroy'])->name('delete.editor-reports');
  Route::get('/management/EditorReports', [App\Http\Controllers\EditorReportsController::class, 'management'])->name('management.editor-reports');

// Route::get('/lokasi', [App\Http\Controllers\ProfilController::class, 'lokasi'])->name('lokasi');
Route::get('/monthly/{id}', [App\Http\Controllers\MonthlyController::class, 'index'])->name('monthly');
Route::post('/update/monthly/progress/{id}', [App\Http\Controllers\MonthlyController::class, 'update'])->name('monthlyProgress');
Route::get('/export-akun/Monthlyprogress/{id}', [App\Http\Controllers\MonthlyController::class, 'export'])->name('exportMonthlyprogress');
Route::get('/reset-akun/Monthlyprogress/{id}', [App\Http\Controllers\MonthlyController::class, 'reset'])->name('resetMonthlyprogress');

Route::get('/random', [App\Http\Controllers\AkunController::class, 'hzz']);
Route::get('/view/akun/count-progress', [App\Http\Controllers\AkunController::class, 'count'])->name('countProgress');
Route::get('/creator/manageAkun', [App\Http\Controllers\AkunController::class, 'index'])->name('manageAkun');
Route::put('/update/akun/{id}', [App\Http\Controllers\AkunController::class, 'edit'])->name('editAkun');
Route::get('/delete/akun/{id}', [App\Http\Controllers\AkunController::class, 'delete'])->name('deleteAkun');
// Route::get('/delete/akun/{id}', [App\Http\Controllers\AkunController::class, 'index'])->name('manageAkun');

Route::get('/creator/daily/works/{id}', [App\Http\Controllers\DaysController::class, 'manage'])->name('daily');
Route::post('/add/daily/works/{id}', [App\Http\Controllers\DaysController::class, 'addDaily'])->name('adddaily');
Route::put('/update/daily/works/{id}', [App\Http\Controllers\DaysController::class, 'updateDaily'])->name('updatedaily');
Route::get('/export-akun/dailyworks/{id}', [App\Http\Controllers\DaysController::class, 'export'])->name('exportDailyworks');
Route::get('/reset-akun/dailyworks/{id}', [App\Http\Controllers\DaysController::class, 'reset'])->name('resetDailyworks');
Route::get('/export-akunpdf/{id}', [App\Http\Controllers\DaysController::class, 'exportPDF'])->name('exportDailyworksPDF');

Route::get('/creator/daily/reports/{id}', [App\Http\Controllers\ReportsController::class, 'index'])->name('reports');
Route::post('/add/daily/reports/{id}', [App\Http\Controllers\ReportsController::class, 'store'])->name('addreports');
Route::put('/update/daily/reports/{id}', [App\Http\Controllers\ReportsController::class, 'update'])->name('updatereports');
Route::get('/export-akun/dailyreports/{id}', [App\Http\Controllers\ReportsController::class, 'export'])->name('exportdailyreports');
Route::get('/reset-akun/dailyreports/{id}', [App\Http\Controllers\ReportsController::class, 'reset'])->name('resetdailyreports');

Route::get('/creator/daily/trouble/{id}', [App\Http\Controllers\TroubleController::class, 'index'])->name('trouble');
Route::post('/creator/add/trouble/{id}', [App\Http\Controllers\TroubleController::class, 'add'])->name('addtrouble');
Route::put('/creator/update/trouble/{id}', [App\Http\Controllers\TroubleController::class, 'update'])->name('updatetrouble');
Route::get('/export-akun/dailyTrouble/{id}', [App\Http\Controllers\TroubleController::class, 'export'])->name('exportdailyTrouble');
Route::get('/reset-akun/dailyTrouble/{id}', [App\Http\Controllers\TroubleController::class, 'reset'])->name('resetdailyTrouble');

Route::post('/creator/addAkun', [App\Http\Controllers\AkunController::class, 'add'])->name('addAkun');
Route::get('/creator/editAkun/{id}', [App\Http\Controllers\AkunController::class, 'edit'])->name('editAkun');
Route::get('/creator/deleteAkun/{id}', [App\Http\Controllers\AkunController::class, 'delete'])->name('deleteAkun');

Route::get('/inventory-control/{id}', [App\Http\Controllers\InventoryControlController::class, 'manage'])->name('inventorycontrol');
Route::post('/add/inventory-control/{id}', [App\Http\Controllers\InventoryControlController::class, 'add'])->name('add.inventory-control');
Route::put('/update/inventory-control/{id}', [App\Http\Controllers\InventoryControlController::class, 'update'])->name('update.inventory-control');
Route::get('/export-akun/dailyInventoryControl/{id}', [App\Http\Controllers\InventoryControlController::class, 'export'])->name('exportdailyInventoryControl');
Route::get('/reset-akun/dailyInventoryControl/{id}', [App\Http\Controllers\InventoryControlController::class, 'reset'])->name('resetdailyInventoryControl');

Route::get('/editing-request', [App\Http\Controllers\EditController::class, 'index'])->name('editReq');
Route::get('/create/editing-request/{id}', [App\Http\Controllers\EditController::class, 'create'])->name('createeditReq');
Route::post('/store/editing-request/{id}', [App\Http\Controllers\EditController::class, 'store'])->name('storeeditReq');
Route::pUT('/update/editing-request/{id}', [App\Http\Controllers\EditController::class, 'update'])->name('updateeditReq');
Route::get('/accept/editing-request/{id}', [App\Http\Controllers\EditController::class, 'accept'])->name('accept');
Route::get('/decline/editing-request/{id}', [App\Http\Controllers\EditController::class, 'decline'])->name('decline');
Route::get('/hold/editing-request/{id}', [App\Http\Controllers\EditController::class, 'hold'])->name('hold');
Route::get('/waiting/editing-request/{id}', [App\Http\Controllers\EditController::class, 'waiting'])->name('waiting');
Route::get('/trouble/editing-request/{id}', [App\Http\Controllers\EditController::class, 'trouble'])->name('trouble');
Route::get('/finish/editing-request/{id}', [App\Http\Controllers\EditController::class, 'finish'])->name('finish');
Route::get('/delete/request-editing/{id}', [App\Http\Controllers\EditController::class, 'delete'])->name('hapus');
Route::get('/edit/request-editing/{id}', [App\Http\Controllers\EditController::class, 'edit'])->name('edit');
Route::get('/export/Editingrequest', [App\Http\Controllers\EditController::class, 'export'])->name('exportEditingrequest');
Route::get('/reset/Editingrequest', [App\Http\Controllers\EditController::class, 'reset'])->name('resetEditingrequest');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Profil Controller
Route::post('/edit/foto-profil', [App\Http\Controllers\ProfilController::class, 'addImage'])->name('editFP');
Route::post('/change-password', [App\Http\Controllers\ProfilController::class, 'chgPwd'])->name('ubahPwd');
Route::get('/delete/user/{id}', [App\Http\Controllers\ProfilController::class, 'delete'])->name('hapus');

//ae live
Route::get('/AELive/Management', [App\Http\Controllers\AeLiveController::class, 'management'])->name('management.ae-live');
 Route::get('/AELive', [App\Http\Controllers\AeLiveController::class, 'show'])->name('ae-live');
 Route::get('/add/AELive', [App\Http\Controllers\AeLiveController::class, 'create'])->name('add.ae-live');
 Route::post('/store/AELive', [App\Http\Controllers\AeLiveController::class, 'store'])->name('store.ae-live');
 Route::get('/edit/AELive/{id}', [App\Http\Controllers\AeLiveController::class, 'edit'])->name('edit.ae-live');
 Route::put('/update/AELive/{id}', [App\Http\Controllers\AeLiveController::class, 'update'])->name('update.ae-live');
 Route::get('/delete/AELive/{id}', [App\Http\Controllers\AeLiveController::class, 'destroy'])->name('delete.ae-live');

//admin
Route::namespace('App\Http\Controllers')->name('admin.')->middleware([])->prefix('admin')->group(function () {
    Route::get('/show/allUsers', [App\Http\Controllers\ProfilController::class, 'showUser'])->name('users');
    Route::get('/allUsers', [App\Http\Controllers\ProfilController::class, 'indexUser'])->name('oke');
    Route::get('/change-role-user/{id}/{level}', [App\Http\Controllers\ProfilController::class, 'chgRoleUser'])->name('chgRole');
    
    //poster 
    Route::get('/poster', [App\Http\Controllers\PosterInfoController::class, 'index'])->name('poster');
    Route::post('/store/poster', [App\Http\Controllers\PosterInfoController::class, 'store'])->name('store.poster');
    Route::put('/update/poster/{id}', [App\Http\Controllers\PosterInfoController::class, 'update'])->name('update.poster');
    Route::get('/delete/poster/{id}', [App\Http\Controllers\PosterInfoController::class, 'destroy'])->name('delete.poster');

    //digital documents 
    Route::get('/digital-documents/{kategori}', [App\Http\Controllers\DigitalDocumentsController::class, 'index'])->name('digital_documents');
    Route::post('/store/digital_documents', [App\Http\Controllers\DigitalDocumentsController::class, 'store'])->name('store.digital_documents');
    Route::put('/update/digital_documents/{id}', [App\Http\Controllers\DigitalDocumentsController::class, 'update'])->name('update.digital_documents');
    Route::get('/delete/digital_documents/{id}', [App\Http\Controllers\DigitalDocumentsController::class, 'destroy'])->name('delete.digital_documents');

    //property inv
    Route::get('/propertyinv', [App\Http\Controllers\PropertyinvController::class, 'index'])->name('property-inv');
    Route::get('/add/propertyinv', [App\Http\Controllers\PropertyinvController::class, 'create'])->name('add.property-inv');
    Route::post('/store/propertyinv', [App\Http\Controllers\PropertyinvController::class, 'store'])->name('store.property-inv');
    Route::get('/edit/propertyinv/{id}', [App\Http\Controllers\PropertyinvController::class, 'edit'])->name('edit.property-inv');
    Route::put('/update/propertyinv/{id}', [App\Http\Controllers\PropertyinvController::class, 'update'])->name('update.property-inv');
    Route::get('/delete/propertyinv/{id}', [App\Http\Controllers\PropertyinvController::class, 'delete'])->name('delete.property-inv');

    //purchasing req
    Route::get('/reset', [App\Http\Controllers\PurchasingreqController::class, 'reset']);
    Route::get('/purchasing-req', [App\Http\Controllers\PurchasingreqController::class, 'index'])->name('purchasing-req');
    Route::get('/add/purchasing-req', [App\Http\Controllers\PurchasingreqController::class, 'create'])->name('add.purchasing-req');
    Route::post('/store/purchasing-req', [App\Http\Controllers\PurchasingreqController::class, 'store'])->name('store.purchasing-req');
    Route::get('/edit/purchasing-req/{id}', [App\Http\Controllers\PurchasingreqController::class, 'edit'])->name('edit.purchasing-req');
    Route::put('/update/purchasing-req/{id}', [App\Http\Controllers\PurchasingreqController::class, 'update'])->name('update.purchasing-req');
    Route::get('/delete/purchasing-req/{id}', [App\Http\Controllers\PurchasingreqController::class, 'delete'])->name('delete.purchasing-req');
    Route::get('/acc/purchasing-req/{id}', [App\Http\Controllers\PurchasingreqController::class, 'acc'])->name('acc.purchasing-req');
    Route::get('/decline/purchasing-req/{id}', [App\Http\Controllers\PurchasingreqController::class, 'decline'])->name('decline.purchasing-req');

    //incomes
     Route::get('/income', [App\Http\Controllers\IncomeController::class, 'index'])->name('income');
     Route::get('/add/income', [App\Http\Controllers\IncomeController::class, 'create'])->name('add.income');
     Route::post('/store/income', [App\Http\Controllers\IncomeController::class, 'store'])->name('store.income');
     Route::get('/edit/income/{id}', [App\Http\Controllers\IncomeController::class, 'edit'])->name('edit.income');
     Route::put('/update/income/{id}', [App\Http\Controllers\IncomeController::class, 'update'])->name('update.income');
     Route::get('/delete/income/{id}', [App\Http\Controllers\IncomeController::class, 'delete'])->name('delete.income');
     Route::get('/done/income/{id}', [App\Http\Controllers\IncomeController::class, 'done'])->name('done.income');
     Route::get('/on-progress/income/{id}', [App\Http\Controllers\IncomeController::class, 'on_progress'])->name('on-progress.income');

     //reimbursment progress
     Route::get('/Reimbursmentprog', [App\Http\Controllers\ReimbursmentprogController::class, 'index'])->name('Reimbursmentprog');
     Route::get('/add/Reimbursmentprog', [App\Http\Controllers\ReimbursmentprogController::class, 'create'])->name('add.Reimbursmentprog');
     Route::post('/store/Reimbursmentprog', [App\Http\Controllers\ReimbursmentprogController::class, 'store'])->name('store.Reimbursmentprog');
     Route::get('/edit/Reimbursmentprog/{id}', [App\Http\Controllers\ReimbursmentprogController::class, 'edit'])->name('edit.Reimbursmentprog');
     Route::put('/update/Reimbursmentprog/{id}', [App\Http\Controllers\ReimbursmentprogController::class, 'update'])->name('update.Reimbursmentprog');
     Route::get('/delete/Reimbursmentprog/{id}', [App\Http\Controllers\ReimbursmentprogController::class, 'delete'])->name('delete.Reimbursmentprog');
     Route::get('/done/Reimbursmentprog/{id}', [App\Http\Controllers\ReimbursmentprogController::class, 'done'])->name('done.Reimbursmentprog');
     Route::get('/on-progress/Reimbursmentprog/{id}', [App\Http\Controllers\ReimbursmentprogController::class, 'on_progress'])->name('on-progress.Reimbursmentprog');

    //cooperation
    Route::get('/Cooperation', [App\Http\Controllers\CooperationController::class, 'index'])->name('Cooperation');
    Route::get('/add/Cooperation', [App\Http\Controllers\CooperationController::class, 'create'])->name('add.Cooperation');
    Route::post('/store/Cooperation', [App\Http\Controllers\CooperationController::class, 'store'])->name('store.Cooperation');
    Route::get('/edit/Cooperation/{id}', [App\Http\Controllers\CooperationController::class, 'edit'])->name('edit.Cooperation');
    Route::put('/update/Cooperation/{id}', [App\Http\Controllers\CooperationController::class, 'update'])->name('update.Cooperation');
    Route::get('/delete/Cooperation/{id}', [App\Http\Controllers\CooperationController::class, 'delete'])->name('delete.Cooperation');
    Route::get('/done/Cooperation/{id}', [App\Http\Controllers\CooperationController::class, 'done'])->name('done.Cooperation');
    Route::get('/on-progress/Cooperation/{id}', [App\Http\Controllers\CooperationController::class, 'on_progress'])->name('on-progress.Cooperation');

     //Propertyloan
     Route::get('/Propertyloan', [App\Http\Controllers\PropertyloanController::class, 'index'])->name('Propertyloan');
     Route::get('/add/Propertyloan', [App\Http\Controllers\PropertyloanController::class, 'create'])->name('add.Propertyloan');
     Route::post('/store/Propertyloan', [App\Http\Controllers\PropertyloanController::class, 'store'])->name('store.Propertyloan');
     Route::get('/edit/Propertyloan/{id}', [App\Http\Controllers\PropertyloanController::class, 'edit'])->name('edit.Propertyloan');
     Route::put('/update/Propertyloan/{id}', [App\Http\Controllers\PropertyloanController::class, 'update'])->name('update.Propertyloan');
     Route::get('/delete/Propertyloan/{id}', [App\Http\Controllers\PropertyloanController::class, 'delete'])->name('delete.Propertyloan');
     Route::get('/done/Propertyloan/{id}', [App\Http\Controllers\PropertyloanController::class, 'done'])->name('done.Propertyloan');
     Route::get('/on-progress/Propertyloan/{id}', [App\Http\Controllers\PropertyloanController::class, 'on_progress'])->name('on-progress.Propertyloan');

     //GA Reports
    Route::get('/GAReport', [App\Http\Controllers\GAReportController::class, 'index'])->name('ga-reports');
    Route::get('/add/GAReport', [App\Http\Controllers\GAReportController::class, 'create'])->name('add.ga-reports');
    Route::post('/store/GAReport', [App\Http\Controllers\GAReportController::class, 'store'])->name('store.ga-reports');
    Route::get('/edit/GAReport/{id}', [App\Http\Controllers\GAReportController::class, 'edit'])->name('edit.ga-reports');
    Route::put('/update/GAReport/{id}', [App\Http\Controllers\GAReportController::class, 'update'])->name('update.ga-reports');
    Route::get('/delete/GAReport/{id}', [App\Http\Controllers\GAReportController::class, 'delete'])->name('delete.ga-reports');

    
});

Route::namespace('App\Http\Controllers')->name('manage.')->middleware('auth')->prefix('manage')->group(function () {
    Route::get('/monthly', [App\Http\Controllers\MonthlyController::class, 'view'])->name('monthly');
    Route::get('/account-executive', [App\Http\Controllers\MonthlyController::class, 'ae'])->name('account-executive');
    Route::get('/daily-works', [App\Http\Controllers\DaysController::class, 'view'])->name('dailyworks');
    Route::get('/daily-reports', [App\Http\Controllers\ReportsController::class, 'view'])->name('dailyreports');
    Route::get('/daily-troubles', [App\Http\Controllers\TroubleController::class, 'view'])->name('dailytroubles');
    Route::get('/inventory-control', [App\Http\Controllers\InventoryControlController::class, 'view'])->name('inventorycontrol');

});

Route::namespace('App\Http\Controllers')->name('management.')->middleware([])->prefix('management')->group(function () {
    Route::get('/monthly', 
    [App\Http\Controllers\MonthlyController::class, 'viewmanagement'])
    ->name('monthly');
    Route::get('/account-executive', 
    [App\Http\Controllers\MonthlyController::class, 'viewmanagementae'])
    ->name('account-executive');
    Route::get('/daily-works', 
    [App\Http\Controllers\DaysController::class, 'viewmanagement'])
    ->name('dailyworks');
    Route::get('/daily-reports', 
    [App\Http\Controllers\ReportsController::class, 'viewmanagement'])
    ->name('dailyreports');
    Route::get('/daily-troubles', 
    [App\Http\Controllers\TroubleController::class, 'viewmanagement'])
    ->name('dailytroubles');
    Route::get('/inventory-control', 
    [App\Http\Controllers\InventoryControlController::class, 'viewmanagement'])
    ->name('inventorycontrol');
    Route::get('/monthly/{id}', 
    [App\Http\Controllers\MonthlyController::class, 'management'])
    ->name('getmonthly');
    Route::get('/creator/manageAkun', 
    [App\Http\Controllers\AkunController::class, 'management'])
    ->name('manageAkun');
    Route::get('/creator/daily/works/{id}', 
    [App\Http\Controllers\DaysController::class, 'management'])
    ->name('daily');
    Route::get('/creator/daily/reports/{id}', 
    [App\Http\Controllers\ReportsController::class, 'management'])
    ->name('reports');
    Route::get('/creator/daily/trouble/{id}', 
    [App\Http\Controllers\TroubleController::class, 'management'])
    ->name('trouble');
    Route::get('/inventory-control/{id}', 
    [App\Http\Controllers\InventoryControlController::class, 'management'])
    ->name('getinventorycontrol');
    Route::get('/editing-request', 
    [App\Http\Controllers\EditController::class, 'management'])
    ->name('editReq');
    Route::get('/show/allUsers', 
    [App\Http\Controllers\ProfilController::class, 'showUser'])
    ->name('users');
    Route::get('/propertyinv', 
    [App\Http\Controllers\PropertyinvController::class, 'management'])
    ->name('property-inv');
    Route::get('/purchasing-req', 
    [App\Http\Controllers\PurchasingreqController::class, 'management'])
    ->name('purchasing-req');
    Route::get('/income', 
    [App\Http\Controllers\IncomeController::class, 'management'])
    ->name('income');
    Route::get('/Reimbursmentprog', 
    [App\Http\Controllers\ReimbursmentprogController::class, 'management'])
    ->name('Reimbursmentprog');
    Route::get('/Cooperation', 
    [App\Http\Controllers\CooperationController::class, 'management'])
    ->name('Cooperation');
    Route::get('/Propertyloan', 
    [App\Http\Controllers\PropertyloanController::class, 'management'])
    ->name('Propertyloan');

    Route::get('/GAReports', 
    [App\Http\Controllers\PropertyloanController::class, 'management'])
    ->name('GAReports');
});

Route::get('/clear-cache', function() {
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
    return redirect()->route('home')->with('toast_success', 'Cache Berhasil dihapus!');
})->name('clear-cache');