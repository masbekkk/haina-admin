<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::insert(array(
            array(
                'nama' => "Admin"
            ),
            array(
                'nama' => "GA"
            ),
            array(
                'nama' => "Creator"
            ),
            array(
                'nama' => "Editor"
            ),
            array(
                'nama' => "AE"
            ),
            array(
                'nama' => "Management"
            )
        )
             
            );
    }
}
