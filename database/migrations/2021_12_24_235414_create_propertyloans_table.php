<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyloansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propertyloans', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('kebutuhan');
            $table->date('tanggal_pinjam');
            $table->date('tanggal_kembali');
            $table->integer('property_id');
            $table->integer('status')->default(1); //1=on progress, 2 = done
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propertyloans');
    }
}
