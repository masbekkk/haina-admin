<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEditorReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('editor_reports', function (Blueprint $table) {
            $table->id();
            $table->integer('editor_id');
            $table->date('date_of_report');
            $table->text('editing_accounts')->nullable();
            $table->text('numbers_of_editing');
            $table->text('troubles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('editor_reports');
    }
}
