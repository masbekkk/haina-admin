<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAeLivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ae_lives', function (Blueprint $table) {
            $table->id();
            $table->string('platforms');
            $table->dateTime('live_start');
            $table->dateTime('live_end');
            $table->string('link_live')->nullable();
            $table->integer('result_audience')->nullable();
            $table->integer('result_selling')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ae_lives');
    }
}
