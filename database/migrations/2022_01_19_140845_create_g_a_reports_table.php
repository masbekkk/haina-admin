<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGAReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ga_reports', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->date('date_of_reports');
            $table->text('today_works');
            $table->integer('number_of_works');
            $table->text('working_of_trouble');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_a_reports');
    }
}
