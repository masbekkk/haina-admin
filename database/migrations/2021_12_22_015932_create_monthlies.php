<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonthlies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthlies', function (Blueprint $table) {
            $table->id();
            $table->integer('akun_id');
            $table->string('target1')->nullable();
            $table->string('progress1')->nullable();
            $table->string('target2')->nullable();
            $table->string('progress2')->nullable();
            $table->string('target3')->nullable();
            $table->string('progress3')->nullable();
            $table->string('target4')->nullable();
            $table->string('progress4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthlies');
    }
}
