<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReimbursmentprogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reimbursmentprogs', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('request');
            $table->integer('quantity');
            $table->string('price');
            $table->date('submitdate');
            $table->integer('status')->default(1); //1=on progress, 2 = done
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reimbursmentprogs');
    }
}
