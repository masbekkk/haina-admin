@extends('layouts.app')

@section('title')
Manage Monthly Progress
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Monthly Progress</h3>
      </div>
     
      <div class="section-body">
            <div class="card">
                <div class="card-body">
                <div id="reader"></div>
                {{-- <input type="file" id="qr-input-file" accept="image/*" capture> --}}
                </div>
            </div>
      </div>
    </section>
@endsection

@section('scripts')

<script
    src="https://raw.githubusercontent.com/mebjas/html5-qrcode/master/minified/html5-qrcode.min.js">
</script>
<script src="https://unpkg.com/html5-qrcode" type="text/javascript">
// To use Html5QrcodeScanner (more info below)
import {Html5QrcodeScanner} from "html5-qrcode"

// To use Html5Qrcode (more info below)
import {Html5Qrcode} from "html5-qrcode"
</script>
<script>
// This method will trigger user permissions
Html5Qrcode.getCameras().then(devices => {
  /**
   * devices would be an array of objects of type:
   * { id: "id", label: "label" }
   */
  if (devices && devices.length) {
    var cameraId = devices[0].id;
    // .. use this to start scanning.
  }
}).catch(err => {
  // handle err
});
const html5QrCode = new Html5Qrcode("reader");

html5QrCode.start(
  cameraId,     // retreived in the previous step.
  {
    fps: 10,    // sets the framerate to 10 frame per second
    qrbox: 250  // sets only 250 X 250 region of viewfinder to
                // scannable, rest shaded.
  },
  qrCodeMessage => {
    // do something when code is read. For example:
    console.log(`QR Code detected: ${qrCodeMessage}`);
  },
  errorMessage => {
    // parse error, ideally ignore it. For example:
    console.log(`QR Code no longer in front of camera.`);
  })
.catch(err => {
  // Start failed, handle it. For example,
  console.log(`Unable to start scanning, error: ${err}`);
});
// const html5QrCode = new Html5Qrcode(/* element id */ "reader");

// // File based scanning
// const fileinput = document.getElementById('qr-input-file');
// fileinput.addEventListener('change', e => {
//   if (e.target.files.length == 0) {
//     // No file selected, ignore 
//     return;
//   }

//   // Use the first item in the list
//   const imageFile = e.target.files[0];
//   html5QrCode.scanFile(imageFile, /* showImage= */true)
//   .then(qrCodeMessage => {
//     // success, use qrCodeMessage
//     console.log(qrCodeMessage);
//   })
//   .catch(err => {
//     // failure, handle it.
//     console.log(`Error scanning file. Reason: ${err}`)
//   });
// });
</script>

@endsection
    