<html>
    <body>
<table>
    <thead>
    <tr>
        <th>Daily Reports</th>
        <th>Senin (%)</th>
        <th>Selasa (%)</th>
        <th>Rabu (%)</th>
        <th>Kamis (%)</th>
        <th>Jum'at (%)</th>
        <th>Sabtu (%)</th>
        <th>Tgl Dibuat</th>
        <th>Tgl Diupdate</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>Followers</td>
            <td>{{$followers->senin}}</td> 
            <td>{{$followers->selasa}}</td> 
            <td>{{$followers->rabu}}</td> 
            <td>{{$followers->kamis}}</td> 
            <td>{{$followers->jumat}}</td> 
            <td>{{$followers->sabtu}}</td>
            <td>{{date('d M Y H:m', strtotime($followers->created_at))}}</td>
            <td>{{date('d M Y H:m', strtotime($followers->updated_at))}}</td>
            
        </tr>
        <tr>
            <td>Biggest Views</td>
            <td>{{$views->senin}}</td> 
            <td>{{$views->selasa}}</td> 
            <td>{{$views->rabu}}</td> 
            <td>{{$views->kamis}}</td> 
            <td>{{$views->jumat}}</td> 
            <td>{{$views->sabtu}}</td> 
            <td>{{date('d M Y H:m', strtotime($views->created_at))}}</td>
            <td>{{date('d M Y H:m', strtotime($views->updated_at))}}</td>
        </tr>
        <tr>
            <td>Increasing</td>
            <td>{{$Increasing->senin}}</td> 
            <td>{{$Increasing->selasa}}</td> 
            <td>{{$Increasing->rabu}}</td> 
            <td>{{$Increasing->kamis}}</td> 
            <td>{{$Increasing->jumat}}</td> 
            <td>{{$Increasing->sabtu}}</td> 
            <td>{{date('d M Y H:m', strtotime($Increasing->created_at))}}</td>
            <td>{{date('d M Y H:m', strtotime($Increasing->updated_at))}}</td>
        </tr>
        <tr>
            <td>Ratio</td>
            <td>{{$Ratio->senin}}</td> 
            <td>{{$Ratio->selasa}}</td> 
            <td>{{$Ratio->rabu}}</td> 
            <td>{{$Ratio->kamis}}</td> 
            <td>{{$Ratio->jumat}}</td> 
            <td>{{$Ratio->sabtu}}</td> 
            <td>{{date('d M Y H:m', strtotime($Ratio->created_at))}}</td>
            <td>{{date('d M Y H:m', strtotime($Ratio->updated_at))}}</td>
        </tr>
        <tr>
            <td>Actual Posting</td>
            <td>{{$actualp->senin}}</td> 
            <td>{{$actualp->selasa}}</td> 
            <td>{{$actualp->rabu}}</td> 
            <td>{{$actualp->kamis}}</td> 
            <td>{{$actualp->jumat}}</td> 
            <td>{{$actualp->sabtu}}</td> 
            <td>{{date('d M Y H:m', strtotime($actualp->created_at))}}</td>
            <td>{{date('d M Y H:m', strtotime($actualp->updated_at))}}</td>
        </tr>
        <tr>
            <td>Inventory</td>
            <td>{{$inventory->senin}}</td> 
            <td>{{$inventory->selasa}}</td> 
            <td>{{$inventory->rabu}}</td> 
            <td>{{$inventory->kamis}}</td> 
            <td>{{$inventory->jumat}}</td> 
            <td>{{$inventory->sabtu}}</td> 
            <td>{{date('d M Y H:m', strtotime($inventory->created_at))}}</td>
            <td>{{date('d M Y H:m', strtotime($inventory->updated_at))}}</td>
        </tr>
    </tbody>
    
</table>



    </body>
    
    
    
</html>