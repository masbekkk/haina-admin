<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>@yield('title') | {{ config('app.name') }}</title>
    <link rel="icon" href="{{ asset('img/logo.png') }}" />
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 4.1.1 -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Ionicons -->
    <link href="//fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link href="{{ asset('assets/css/@fortawesome/fontawesome-free/css/all.css') }}" rel="stylesheet" type="text/css">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('web/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('web/css/components.css')}}">
</head>
<body>
<div class="table-responsive table">
    <table class="table table-striped" id="table-1">
      <thead>
        <tr>
          <th>Daily Works</th>
          <th>Senin</th>
          <th>Selasa</th>
          <th>Rabu</th>
          <th>Kamis</th>
          <th>Jum'at</th>
          <th>Sabtu</th>
          <th>Total</th>
         
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">Script</li>
              <li class="list-group-item">Editing</li>
              <li class="list-group-item">Shooting</li>
              <li class="list-group-item">Posting</li>
              <li class="list-group-item">Administration</li>
              {{-- <li class="list-group-item"></li> --}}
            </ul>
          </td>  
          @for($i = 1; $i<=6; $i++) 
          <td>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">
                @if($i == 1)
                {{$script->senin}}                
                @elseif($i == 2)
                {{$script->selasa}}
                @elseif($i == 3)
                {{$script->rabu}}               
                @elseif($i == 4)
                {{$script->kamis}}                
                @elseif($i == 5)
                {{$script->jumat}}                
                @elseif($i == 6)
                {{$script->sabtu}}                
                @endif
              </li>
              <li class="list-group-item">
                    @if($i == 1)
                    {{$Edit->senin}}                
                    @elseif($i == 2)
                    {{$Edit->selasa}}
                    @elseif($i == 3)
                    {{$Edit->rabu}}               
                    @elseif($i == 4)
                    {{$Edit->kamis}}                
                    @elseif($i == 5)
                    {{$Edit->jumat}}                
                    @elseif($i == 6)
                    {{$Edit->sabtu}}                
                    @endif
              </li>
              <li class="list-group-item">
                @if($i == 1)
                    {{$Shooting->senin}}
                @elseif($i == 2)
                    {{$Shooting->selasa}}
                @elseif($i == 3)
                    {{$Shooting->rabu}}
                @elseif($i == 4)
                    {{$Shooting->kamis}}
                @elseif($i == 5)
                    {{$Shooting->jumat}}
                @elseif($i == 6)
                    {{$Shooting->sabtu}}
                @endif
              </li>
              <li class="list-group-item">
                @if($i == 1)
                    {{$posting->senin}}
                @elseif($i == 2)
                    {{$posting->selasa}}
                @elseif($i == 3)
                    {{$posting->rabu}}
                @elseif($i == 4)
                    {{$posting->kamis}}
                @elseif($i == 5)
                    {{$posting->jumat}}
                @elseif($i == 6)
                    {{$posting->sabtu}}
                @endif
              </li>
              <li class="list-group-item">
                @if($i == 1)
                    {{$admin->senin}}
                @elseif($i == 2)
                    {{$admin->selasa}}
                @elseif($i == 3)
                    {{$admin->rabu}}
                @elseif($i == 4)
                    {{$admin->kamis}}
                @elseif($i == 5)
                    {{$admin->jumat}}
                @elseif($i == 6)
                    {{$admin->sabtu}}
                @endif
              </li>
            </ul>
            </td> 
            @endfor
            <td>
              <ul class="list-group list-group-flush">
                @for($i = 1; $i<=5; $i++) 
                <li class="list-group-item">
                  total{{$i}}
                </li>
                @endfor
              </ul>
            </td>
            {{-- @if(Auth::user()->level == 0 || Auth::user()->level == 2) --}}
            <td>
              <ul class="list-group list-group-flush">
                @for($i = 1; $i<=5; $i++)
                <li class="list-group-item"> 
                  @if($i == 1)
                  {{date('d M Y H:m', strtotime($script->created_at))}}
                  @elseif($i == 2)
                  {{date('d M Y H:m', strtotime($Edit->created_at))}}
                  @elseif($i == 3)
                  {{date('d M Y H:m', strtotime($Shooting->created_at))}}
                  @elseif($i == 4)
                  {{date('d M Y H:m', strtotime($posting->created_at))}}
                  @elseif($i == 5)
                  {{date('d M Y H:m', strtotime($admin->created_at))}}
                  @endif
                </li>
                @endfor
              </ul>
            </td>
            <td>
              <ul class="list-group list-group-flush">
                @for($i = 1; $i<=5; $i++)
                <li class="list-group-item"> 
                  @if($i == 1)
                  {{date('d M Y H:m', strtotime($script->updated_at))}}
                  @elseif($i == 2)
                  {{date('d M Y H:m', strtotime($Edit->updated_at))}}
                  @elseif($i == 3)
                  {{date('d M Y H:m', strtotime($Shooting->updated_at))}}
                  @elseif($i == 4)
                  {{date('d M Y H:m', strtotime($posting->updated_at))}}
                  @elseif($i == 5)
                  {{date('d M Y H:m', strtotime($admin->updated_at))}}
                  @endif
                </li>
                @endfor
              </ul>
            </td>
            
          
        </tr>
      </tbody>
    </table>
</div>

</body>
{{-- <script src="{{ asset('assets/js/jquery.min.js') }}"></script> --}}
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
{{-- <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script> --}}
{{-- <script src="{{ asset('assets/js/select2.min.js') }}"></script> --}}
<script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>

<!-- Template JS File -->
<script src="{{ asset('web/js/stisla.js') }}"></script>
<script src="{{ asset('web/js/scripts.js') }}"></script>
<script src="{{ mix('assets/js/profile.js') }}"></script>
<script src="{{ mix('assets/js/custom/custom.js') }}"></script>
<script src="{{asset('js/easy-number-separator.js')}}"></script>
</html>