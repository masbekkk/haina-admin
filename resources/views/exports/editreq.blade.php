<html>
    <body>
<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Accounts</th>
        <th>Concept</th>
        <th>Estimeted Posting</th>
        <th>Remark</th>
        <th>Tgl Dibuat</th>
        <th>Tgl Diupdate</th>
    </tr>
    </thead>
    <tbody>
        @foreach($data as $a)
        <tr>
            <td> {{$loop->iteration}}</td>
            <td>{{$a->user->name}}</td>
            <td>{{$a->akun->nama}}</td>
            <td>{{$a->concept}}</td>
            <td>{{date('d M Y', strtotime($a->estimeted_posting))}}</td>
            <td>{{$a->acc->name}}</td>
            <td>{{date('d M Y H:m', strtotime($a->created_at))}}</td>
            <td>{{date('d M Y H:m', strtotime($a->updated_at))}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
    </body>
</html>
