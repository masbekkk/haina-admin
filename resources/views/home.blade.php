@extends('layouts.app') 
@section('title')
Dashboard
@endsection
@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Dashboard</h3>
  </div>
  <div class="section-body">
    <div class="card">
      <div class="card-header">
        <h4>POSTER &amp; INFORMATION UPDATE</h4>
      </div>
      <div class="card-body">
        {{-- @if($poster == '')
        <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators3" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators3" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators3" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="https://demo.getstisla.com/assets/img/news/img08.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://demo.getstisla.com/assets/img/news/img07.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://demo.getstisla.com/assets/img/news/img02.jpg" alt="Third slide">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        @else --}}
        <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            @foreach($poster as $a)
            @if($a->first())
            <li data-target="#carouselExampleIndicators3" data-slide-to="0" class="active"></li>
            @else
            <li data-target="#carouselExampleIndicators3" data-slide-to="{{$loop->iteration}}"></li>
            @endif
            @endforeach
          </ol>
          <div class="carousel-inner">
            @foreach($poster as $a)
            @if($a->first())
            <div class="carousel-item active">
              <img class="d-block w-100" src="{{asset($a->gambar)}}" alt="First slide">
            </div>
            @else
            <div class="carousel-item">
              <img class="d-block w-100" src="{{asset($a->gambar)}}" alt="Second slide">
            </div>
            @endif
            @endforeach
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        {{-- @endif --}}
      </div>
    </div>
    {{-- <div class="card">
      <div class="card-header">
        <h4>Crossfade</h4>
      </div>
      <div class="card-body">
        <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleFadecarouselExampleIndicators2" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleFadecarouselExampleIndicators2" data-slide-to="1"></li>
            <li data-target="#carouselExampleFadecarouselExampleIndicators2" data-slide-to="2"></li>
          </ol>
        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="https://demo.getstisla.com/assets/img/news/img08.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://demo.getstisla.com/assets/img/news/img07.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://demo.getstisla.com/assets/img/news/img01.jpg" alt="Third slide">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleFadecarouselExampleIndicators2" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleFadecarouselExampleIndicators2" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        </div>
      </div>
    </div> --}}
  
    <div class="row">
      <div class="col-lg-6">
        <div class="card gradient-bottom">
          <div class="card-header">
            <h4>Story Teller</h4>
            <div class="card-header-action dropdown"></div>
          </div>
          <div class="card-body" id="top-5-scroll" style="height: 315px; overflow: hidden; outline: none;" tabindex="2">
            <ul class="list-unstyled list-unstyled-border"> @foreach ($data as $a) @if($a->akun->type == "Story Teller") <li class="media">
                <img class="mr-3 rounded" width="55" src="https://getstisla.com/dist/img/avatar/avatar-{{mt_rand(1, 5)}}.png" alt="product">
                <div class="media-body">
                  <div class="float-right">
                    <div class="font-weight-600 text-muted text-small">Monthly Progress</div>
                  </div>
                  <div class="media-title">{{$a->akun->nama}}</div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-square bg-success" data-width="{{(int) $a->progress1 / 10}}%" style="width: 64%;">
                      </div>
                      <div class="budget-price-label">{{$a->progress1}}</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-danger" data-width="{{(int) $a->progress2 / 10}}%" style="width: 43%;">
                      </div>
                      <div class="budget-price-label">{{$a->progress2}}</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-info" data-width="{{(int) $a->progress3 / 10}}%" style="width: 43%;">
                      </div>
                      <div class="budget-price-label">{{$a->progress3}}</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-dark" data-width="{{(int) $a->progress4 / 10}}%" style="width: 43%;">
                      </div>
                      <div class="budget-price-label">{{$a->progress4}}</div>
                    </div>
                  </div>
                </div>
              </li> @endif @endforeach </ul>
          </div>
          <div class="card-footer pt-3 d-flex justify-content-center">
            <div class="budget-price justify-content-center">
              <div class="budget-price-square bg-success" data-width="20" style="width: 20px;"></div>
              <div class="budget-price-label">Progress Week 1</div>
            </div>
            <div class="budget-price justify-content-center">
              <div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
              <div class="budget-price-label">Progress Week 2</div>
            </div>
            <div class="budget-price justify-content-center">
              <div class="budget-price-square bg-info" data-width="20" style="width: 20px;"></div>
              <div class="budget-price-label">Progress Week 3</div>
            </div>
            <div class="budget-price justify-content-center">
              <div class="budget-price-square bg-dark" data-width="20" style="width: 20px;"></div>
              <div class="budget-price-label">Progress Week 4</div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card gradient-bottom">
          <div class="card-header">
            <h4>In Frame</h4>
            <div class="card-header-action dropdown"></div>
          </div>
          <div class="card-body" id="top-1-scroll" style="height: 315px; overflow: hidden; outline: none;" tabindex="2">
            <ul class="list-unstyled list-unstyled-border"> @foreach ($data as $a) @if($a->akun->type == "In Frame") <li class="media">
                <img class="mr-3 rounded" width="55" src="https://getstisla.com/dist/img/avatar/avatar-{{mt_rand(1, 5)}}.png" alt="product">
                <div class="media-body">
                  <div class="float-right">
                    <div class="font-weight-600 text-muted text-small">Monthly Progress</div>
                  </div>
                  <div class="media-title">{{$a->akun->nama}}</div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-square bg-success" data-width="{{(int) $a->progress1 / 10}}%" style="width: 64%;">
                      </div>
                      <div class="budget-price-label">{{$a->progress1}}</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-danger" data-width="{{(int) $a->progress2 / 10}}%" style="width: 43%;">
                      </div>
                      <div class="budget-price-label">{{$a->progress2}}</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-info" data-width="{{(int) $a->progress3 / 10}}%" style="width: 43%;">
                      </div>
                      <div class="budget-price-label">{{$a->progress3}}</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-dark" data-width="{{(int) $a->progress4 / 10}}%" style="width: 43%;">
                      </div>
                      <div class="budget-price-label">{{$a->progress4}}</div>
                    </div>
                  </div>
                </div>
              </li> @endif @endforeach </ul>
          </div>
          <div class="card-footer pt-3 d-flex justify-content-center">
            <div class="budget-price justify-content-center">
              <div class="budget-price-square bg-success" data-width="20" style="width: 20px;"></div>
              <div class="budget-price-label">Progress Week 1</div>
            </div>
            <div class="budget-price justify-content-center">
              <div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
              <div class="budget-price-label">Progress Week 2</div>
            </div>
            <div class="budget-price justify-content-center">
              <div class="budget-price-square bg-info" data-width="20" style="width: 20px;"></div>
              <div class="budget-price-label">Progress Week 3</div>
            </div>
            <div class="budget-price justify-content-center">
              <div class="budget-price-square bg-dark" data-width="20" style="width: 20px;"></div>
              <div class="budget-price-label">Progress Week 4</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-6">
      <div class="card gradient-bottom">
        <div class="card-header">
          <h4>Out Frame</h4>
          <div class="card-header-action dropdown"></div>
        </div>
        <div class="card-body" id="top-2-scroll" style="height: 315px; overflow: hidden; outline: none;" tabindex="2">
          <ul class="list-unstyled list-unstyled-border"> @foreach ($data as $a) @if($a->akun->type == "Out Frame") <li class="media">
              <img class="mr-3 rounded" width="55" src="https://getstisla.com/dist/img/avatar/avatar-{{mt_rand(1, 5)}}.png" alt="product">
              <div class="media-body">
                <div class="float-right">
                  <div class="font-weight-600 text-muted text-small">Monthly Progress</div>
                </div>
                <div class="media-title">{{$a->akun->nama}}</div>
                <div class="mt-1">
                  <div class="budget-price">
                    <div class="budget-price-square bg-success" data-width="{{(int) $a->progress1 / 10}}%" style="width: 64%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress1}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-danger" data-width="{{(int) $a->progress2 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress2}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-info" data-width="{{(int) $a->progress3 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress3}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-dark" data-width="{{(int) $a->progress4 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress4}}</div>
                  </div>
                </div>
              </div>
            </li> @endif @endforeach </ul>
        </div>
        <div class="card-footer pt-3 d-flex justify-content-center">
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-success" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 1</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 2</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-info" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 3</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-dark" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 4</div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="card gradient-bottom">
        <div class="card-header">
          <h4>Film</h4>
          <div class="card-header-action dropdown"></div>
        </div>
        <div class="card-body" id="top-4-scroll" style="height: 315px; overflow: hidden; outline: none;" tabindex="2">
          <ul class="list-unstyled list-unstyled-border"> @foreach ($data as $a) @if($a->akun->type == "Film") <li class="media">
              <img class="mr-3 rounded" width="55" src="https://getstisla.com/dist/img/avatar/avatar-{{mt_rand(1, 5)}}.png" alt="product">
              <div class="media-body">
                <div class="float-right">
                  <div class="font-weight-600 text-muted text-small">Monthly Progress</div>
                </div>
                <div class="media-title">{{$a->akun->nama}}</div>
                <div class="mt-1">
                  <div class="budget-price">
                    <div class="budget-price-square bg-success" data-width="{{(int) $a->progress1 / 10}}%" style="width: 64%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress1}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-danger" data-width="{{(int) $a->progress2 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress2}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-info" data-width="{{(int) $a->progress3 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress3}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-dark" data-width="{{(int) $a->progress4 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress4}}</div>
                  </div>
                </div>
              </div>
            </li> @endif @endforeach </ul>
        </div>
        <div class="card-footer pt-3 d-flex justify-content-center">
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-success" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 1</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 2</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-info" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 3</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-dark" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 4</div>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="row">
  <div class="col-lg-6">
    <div class="card gradient-bottom">
      <div class="card-header">
        <h4>Fanbase</h4>
        <div class="card-header-action dropdown"></div>
      </div>
      <div class="card-body" id="top-4-scroll" style="height: 315px; overflow: hidden; outline: none;" tabindex="2">
        <ul class="list-unstyled list-unstyled-border"> @foreach ($data as $a) @if($a->akun->type == "Fanbase") <li class="media">
            <img class="mr-3 rounded" width="55" src="https://getstisla.com/dist/img/avatar/avatar-{{mt_rand(1, 5)}}.png" alt="product">
            <div class="media-body">
              <div class="float-right">
                <div class="font-weight-600 text-muted text-small">Monthly Progress</div>
              </div>
              <div class="media-title">{{$a->akun->nama}}</div>
              <div class="mt-1">
                <div class="budget-price">
                  <div class="budget-price-square bg-success" data-width="{{(int) $a->progress1 / 10}}%" style="width: 64%;">
                  </div>
                  <div class="budget-price-label">{{$a->progress1}}</div>
                </div>
                <div class="budget-price">
                  <div class="budget-price-square bg-danger" data-width="{{(int) $a->progress2 / 10}}%" style="width: 43%;">
                  </div>
                  <div class="budget-price-label">{{$a->progress2}}</div>
                </div>
                <div class="budget-price">
                  <div class="budget-price-square bg-info" data-width="{{(int) $a->progress3 / 10}}%" style="width: 43%;">
                  </div>
                  <div class="budget-price-label">{{$a->progress3}}</div>
                </div>
                <div class="budget-price">
                  <div class="budget-price-square bg-dark" data-width="{{(int) $a->progress4 / 10}}%" style="width: 43%;">
                  </div>
                  <div class="budget-price-label">{{$a->progress4}}</div>
                </div>
              </div>
            </div>
          </li> @endif @endforeach </ul>
      </div>
      <div class="card-footer pt-3 d-flex justify-content-center">
        <div class="budget-price justify-content-center">
          <div class="budget-price-square bg-success" data-width="20" style="width: 20px;"></div>
          <div class="budget-price-label">Progress Week 1</div>
        </div>
        <div class="budget-price justify-content-center">
          <div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
          <div class="budget-price-label">Progress Week 2</div>
        </div>
        <div class="budget-price justify-content-center">
          <div class="budget-price-square bg-info" data-width="20" style="width: 20px;"></div>
          <div class="budget-price-label">Progress Week 3</div>
        </div>
        <div class="budget-price justify-content-center">
          <div class="budget-price-square bg-dark" data-width="20" style="width: 20px;"></div>
          <div class="budget-price-label">Progress Week 4</div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="card gradient-bottom">
      <div class="card-header">
        <h4>Quotes</h4>
        <div class="card-header-action dropdown"></div>
      </div>
      <div class="card-body" id="top-6-scroll" style="height: 315px; overflow: hidden; outline: none;" tabindex="2">
        <ul class="list-unstyled list-unstyled-border"> @foreach ($data as $a) @if($a->akun->type == "Quotes") <li class="media">
            <img class="mr-3 rounded" width="55" src="https://getstisla.com/dist/img/avatar/avatar-{{mt_rand(1, 5)}}.png" alt="product">
            <div class="media-body">
              <div class="float-right">
                <div class="font-weight-600 text-muted text-small">Monthly Progress</div>
              </div>
              <div class="media-title">{{$a->akun->nama}}</div>
              <div class="mt-1">
                <div class="budget-price">
                  <div class="budget-price-square bg-success" data-width="{{(int) $a->progress1 / 10}}%" style="width: 64%;">
                  </div>
                  <div class="budget-price-label">{{$a->progress1}}</div>
                </div>
                <div class="budget-price">
                  <div class="budget-price-square bg-danger" data-width="{{(int) $a->progress2 / 10}}%" style="width: 43%;">
                  </div>
                  <div class="budget-price-label">{{$a->progress2}}</div>
                </div>
                <div class="budget-price">
                  <div class="budget-price-square bg-info" data-width="{{(int) $a->progress3 / 10}}%" style="width: 43%;">
                  </div>
                  <div class="budget-price-label">{{$a->progress3}}</div>
                </div>
                <div class="budget-price">
                  <div class="budget-price-square bg-dark" data-width="{{(int) $a->progress4 / 10}}%" style="width: 43%;">
                  </div>
                  <div class="budget-price-label">{{$a->progress4}}</div>
                </div>
              </div>
            </div>
          </li> @endif @endforeach </ul>
      </div>
      <div class="card-footer pt-3 d-flex justify-content-center">
        <div class="budget-price justify-content-center">
          <div class="budget-price-square bg-success" data-width="20" style="width: 20px;"></div>
          <div class="budget-price-label">Progress Week 1</div>
        </div>
        <div class="budget-price justify-content-center">
          <div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
          <div class="budget-price-label">Progress Week 2</div>
        </div>
        <div class="budget-price justify-content-center">
          <div class="budget-price-square bg-info" data-width="20" style="width: 20px;"></div>
          <div class="budget-price-label">Progress Week 3</div>
        </div>
        <div class="budget-price justify-content-center">
          <div class="budget-price-square bg-dark" data-width="20" style="width: 20px;"></div>
          <div class="budget-price-label">Progress Week 4</div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-lg-6">
      <div class="card gradient-bottom">
        <div class="card-header">
          <h4>Instagram AE</h4>
          <div class="card-header-action dropdown"></div>
        </div>
        <div class="card-body" id="top-7-scroll" style="height: 315px; overflow: hidden; outline: none;" tabindex="2">
          <ul class="list-unstyled list-unstyled-border"> @foreach ($data as $a) @if($a->akun->kategori == "Account Executive" && $a->akun->subtype == "Instagram") <li class="media">
              <img class="mr-3 rounded" width="55" src="https://getstisla.com/dist/img/avatar/avatar-{{mt_rand(1, 5)}}.png" alt="product">
              <div class="media-body">
                <div class="float-right">
                  <div class="font-weight-600 text-muted text-small">Monthly Progress</div>
                </div>
                <div class="media-title">{{$a->akun->nama}}</div>
                <div class="mt-1">
                  <div class="budget-price">
                    <div class="budget-price-square bg-success" data-width="{{(int) $a->progress1 / 10}}%" style="width: 64%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress1}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-danger" data-width="{{(int) $a->progress2 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress2}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-info" data-width="{{(int) $a->progress3 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress3}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-dark" data-width="{{(int) $a->progress4 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress4}}</div>
                  </div>
                </div>
              </div>
            </li> @endif @endforeach </ul>
        </div>
        <div class="card-footer pt-3 d-flex justify-content-center">
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-success" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 1</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 2</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-info" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 3</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-dark" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 4</div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="card gradient-bottom">
        <div class="card-header">
          <h4>Tiktok AE</h4>
          <div class="card-header-action dropdown"></div>
        </div>
        <div class="card-body" id="top-8-scroll" style="height: 315px; overflow: hidden; outline: none;" tabindex="2">
          <ul class="list-unstyled list-unstyled-border"> @foreach ($data as $a) @if($a->akun->kategori == "Account Executive" && $a->akun->subtype == "Tiktok") <li class="media">
              <img class="mr-3 rounded" width="55" src="https://getstisla.com/dist/img/avatar/avatar-{{mt_rand(1, 5)}}.png" alt="product">
              <div class="media-body">
                <div class="float-right">
                  <div class="font-weight-600 text-muted text-small">Monthly Progress</div>
                </div>
                <div class="media-title">{{$a->akun->nama}}</div>
                <div class="mt-1">
                  <div class="budget-price">
                    <div class="budget-price-square bg-success" data-width="{{(int) $a->progress1 / 10}}%" style="width: 64%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress1}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-danger" data-width="{{(int) $a->progress2 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress2}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-info" data-width="{{(int) $a->progress3 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress3}}</div>
                  </div>
                  <div class="budget-price">
                    <div class="budget-price-square bg-dark" data-width="{{(int) $a->progress4 / 10}}%" style="width: 43%;">
                    </div>
                    <div class="budget-price-label">{{$a->progress4}}</div>
                  </div>
                </div>
              </div>
            </li> @endif @endforeach </ul>
        </div>
        <div class="card-footer pt-3 d-flex justify-content-center">
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-success" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 1</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 2</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-info" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 3</div>
          </div>
          <div class="budget-price justify-content-center">
            <div class="budget-price-square bg-dark" data-width="20" style="width: 20px;"></div>
            <div class="budget-price-label">Progress Week 4</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- <div class="col-md-6">
								<div class="card">
									<div class="card-header">
										<h4>Top Countries</h4>
									</div>
									<div class="card-body"></div>
								</div>
							</div> --}}
  </div>
</section> @endsection @section('scripts') {{-- <script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/chart.js"></script> --}}
{{-- <script>
  const labels = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
  ];
  const data = {
    labels: labels,
    datasets: [{
      label: 'My First dataset',
      backgroundColor: 'rgb(255, 99, 132)',
      borderColor: 'rgb(255, 99, 132)',
      data: [
      ],
    }]
  };

  const config = {
    type: 'line',
    data: data,
    options: {}
  };
</script> --}}
{{-- <script>
    const myChart = new Chart(
      document.getElementById('myChart2'),
      config
    );
</script> --}}
{{-- </script> --}} @endsection