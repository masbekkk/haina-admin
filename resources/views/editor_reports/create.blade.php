@extends('layouts.app') 
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('title')
Editor Reports
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Add Editor Reports</h3>
  </div>
  <div class="section-body">
            <form action="{{route('store.editor-reports')}}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label>Date Of Report</label>
                        <input type="date" name="date_of_report" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Editing Accounts</label>
                        <select class="form-control select2 js-example-basic-multiple" name="editing_accounts[]" multiple tabindex="-1" area-hidden="true">
                            @foreach($akun as $a)
                            <option value="{{$a->nama}}">{{$a->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row mb-4">
                        <label>Number Of Editing</label>
                        <textarea name="numbers_of_editing" class="form-control"></textarea>
                    </div>
                    <div class="form-group row mb-4">
                        <label>Working Of Trouble</label>
                        <textarea name="troubles" class="form-control"></textarea>
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Simpan</button>
                        <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section> 
@endsection 
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- // In your Javascript (external .js resource or <script> tag) --}}
    <script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    </script>
@endsection