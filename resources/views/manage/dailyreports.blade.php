@extends('layouts.app')

@section('title')
Manage Daily Reports
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Daily Reports</h3>
      </div>
     
      <div class="section-body">
              <div class="card">
                <div class="card-header">
                  <a data-toggle="modal" data-target="#addAkun" href="#" data-id="{{ \Auth::id() }}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-user-plus"></i> Tambah Akun</a>
                </div>
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                                <th>Nama</th>
                                @endif
                                <th>Nama Akun</th>
                                <th>Aksi</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                    <tbody>
                    @if($belum == 0)
                      @foreach ($data as $a)
                      
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                        <td>
                            {{$a->user->name}}
                        </td>
                        @endif
                        <td>
                          @if($a->kategori == "Account Executive")
                          <span class="badge badge-warning">{{$a->nama}}</span>
                          @else
                          <span class="badge badge-dark">{{$a->nama}}</span>
                          @endif
                        </td>
                        <td>
                            <a class="btn btn-icon icon-left btn-info" href="/creator/daily/reports/{{$a->id}}"><i class="fas fa-file"></i>Manage Daily Reports</a>
                        </td>
                        <td>
                          <a class="btn btn-icon icon-left btn-danger" onclick="return confirm('Yakin ingin menghapus akun {{$a->nama}}?')" href="/delete/akun/{{$a->id}}"><i class="fas fa-trash"></i> Hapus</a>
                          <a class="btn btn-icon icon-left btn-warning" data-toggle="modal" data-target="#akun{{ $a->id }}" href="#editAkun"><i class="fas fa-pen"></i> Edit</a>
                        </td>
                      </tr>
                      {{-- modal edit akun --}}
                      <div id="akun{{ $a->id }}" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="false">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Tambahkan Akun yang Anda Manage</h5>
                                    <button type="button" aria-label="Close" class="close outline-none" data-dismiss="modal">×</button>
                                </div>
                                <form method="POST" action="/update/akun/{{$a->id}}" enctype="multipart/form-data">
                                <div class="modal-body">
                                    {{-- @if ($errors->any())
                                    <div class="alert alert-danger d-none" id="">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif --}}
                                        @csrf
                                        {{method_field('PUT')}}
                                        <div class="form-group col-sm-12">
                                            <label>Nama Akun :</label><span
                                                    class="required"></span><span class="required">*</span>
                                            <div class="input-group">
                                                <input class="form-control input-group__addon" id="pfCurrentPassword" value="{{$a->nama}}" type="text"
                                                       name="nama" required>
                                                <div class="input-group-append input-group__icon">
                                                    <span class="input-group-text changeType">
                                                        <i class="fas fa-user-plus"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-12">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Kategori</label>
                                                <select class="form-control" name="kategori" id="exampleFormControlSelect1">
                                                  <option value="{{$a->kategori}}" selected>{{$a->kategori}}</option>
                                                  <option value="Original">Original</option>
                                                  <option value="Non Original">Non Original</option>
                                                  <option value="Account Executive">Account Executive</option>
                                                </select>
                                              </div>
                                              <div class="form-group">
                                                <label for="exampleFormControlSelect1">Type Akun</label>
                                                <select class="form-control" name="type" id="exampleFormControlSelect1">
                                                  <option value="{{$a->type}}" selected>{{$a->type}}</option>
                                                  <option value="Story Teller">Story Teller</option>
                                                  <option value="In Frame">In Frame</option>
                                                  <option value="Out Frame">Out Frame</option>
                                                  <option value="Film">Film</option>
                                                  <option value="Fanbase">Fanbase</option>
                                                  <option value="Quotes">Quotes</option>
                                                  <option value="General">General</option>
                                                  <option value="Product">Product</option>
                                                </select>
                                              </div>
                                              <div class="form-group">
                                                <label for="exampleFormControlSelect1">Sub Type</label>
                                                <select class="form-control" name="subtype" id="exampleFormControlSelect1">
                                                  <option value="{{$a->subtype}}" selected>{{$a->subtype}}</option>
                                                  <option value="Instagram">Instagram</option>
                                                  <option value="Tiktok">Tiktok</option>
                                                </select>
                                              </div>
                                        </div>
                                        <div class="form-group col-sm-12">
                                          <label>Link Akun :</label><span
                                                  class="required"></span><span class="required"></span>
                                          <div class="input-group">
                                              <input class="form-control input-group__addon" id="pfCurrentPassword" type="link"
                                                     name="link" value="{{$a->link}}">
                                              <div class="input-group-append input-group__icon">
                                                  <span class="input-group-text changeType">
                                                      <i class="fas fa-link"></i>
                                                  </span>
                                              </div>
                                          </div>
                                      </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary" id="btnPrPasswordEditSave" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing...">Update</button>
                                        <button type="button" class="btn btn-light ml-1" data-dismiss="modal">Cancel
                                        </button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- end modal edit akun --}}
                    
                      @endforeach
                      @else
                      @endif
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection
    