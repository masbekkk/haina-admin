<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
    <br>
    @if(Auth::user()->level == 0)
    <a class="nav-link" href="{{route('admin.users')}}">
        <i class=" fas fa-users-cog"></i><span>Manage Users</span>
    </a>
    @endif
    @if(Auth::user()->level != 1)
    <br>
    <a class="nav-link" href="{{route('manage.dailyworks')}}">
        <i class=" fas fa-suitcase"></i><span>Daily Works</span>
    </a>

    <br>
    <a class="nav-link" href="{{route('manage.dailyreports')}}">
        <i class=" fas fa-fire-alt"></i><span>Daily Reports</span>
    </a>

    <br>
    <a class="nav-link" href="{{route('manage.dailytroubles')}}">
        <i class=" fas fa-wrench"></i><span>Daily Trouble</span>
    </a>

    <br>
    <a class="nav-link" href="{{route('manageAkun')}}">
        <i class=" fas fa-boxes"></i><span>Inventory Report</span>
    </a>
    <br>
    <a class="nav-link" href="{{route('manage.monthly')}}">
        <i class=" fas fa-spinner"></i><span>Monthly Progress</span>
    </a>
    <br>
    <a class="nav-link" href="{{route('manage.monthly')}}">
        <i class=" fas fa-user-circle"></i><span>Account Executive Progress</span>
    </a>

    <br>
    <a class="nav-link" href="{{route('countProgress')}}">
        <i class=" fas fa-thumbs-up"></i><span>Akun Count Progress</span>
    </a>
    <br>
    <a class="nav-link" href="{{route('editReq')}}">
        <i class=" fas fa-receipt"></i><span>Editing Request</span>
    </a>
    <br>
    
    <li class="nav-item dropdown ">
        <a href="#" class="nav-link has-dropdown"><i class="fas fa-money-bill-wave"></i><span>Finance Report</span></a>
        <ul class="dropdown-menu">
            {{-- <li class="active"><a href="#"><i class=" fas fa-money-bill-wave"></i><span></span></a>
            </li> --}}
            <li><a href="{{ route('admin.property-inv') }}">Property Invenstment</a></li>
        </ul>
    </li>
    <br>
    <a class="nav-link" href="{{route('manageAkun')}}">
        <i class=" fas fa-percentage"></i><span> Property</span>
    </a>
    <br>
    <a class="nav-link" href="{{route('manageAkun')}}">
        <i class=" fas fa-keyboard"></i><span> Reimbursment Request</span>
    </a>
    <br>
    <a class="nav-link" href="{{route('manageAkun')}}">
        <i class=" fas fa-briefcase"></i><span> Cooperation</span>
    </a>
    @endif
</li>
