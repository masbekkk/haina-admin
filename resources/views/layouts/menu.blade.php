<li class="menu-header">Dashboard</li>
<li><a class="nav-link" href="{{route('home')}}"><i class="fas fa-columns" ></i> <span> Dashboard</span></a></li>
@if(Auth::user()->level == 0)
<li><a class="nav-link" href="{{route('admin.users')}}"><i class="fas fa-users-cog" ></i> <span> Manage Users</span></a></li>
@endif
@if(Auth::user()->level != 1 && Auth::user()->level != 6 )
<li class="nav-item dropdown">
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-address-card"></i><span>Presensi</span></a>
    <ul class="dropdown-menu">
        <li><a href="{{ route('presensi') }}">Presensi Masuk</a></li>
        <li><a href="{{ route('presensi.out') }}">Presensi Keluar</a></li>
    </ul>
</li> 
<li class="nav-item dropdown">
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-folder"></i><span>Digital Documents</span></a>
    <ul class="dropdown-menu">
        <li><a href="{{ route('admin.digital_documents', ['kategori' => 1]) }}">Rules and Regulation</a></li>
        <li><a href="{{ route('admin.digital_documents', ['kategori' => 2]) }}">Ratecard Estimation</a></li>
    </ul>
</li>   
<li><a class="nav-linknav-link" href="{{route('manage.dailyworks')}}">
    <i class=" fas fa-suitcase"></i><span>Daily Works</span>
</a>
</li>
<li><a class="nav-link"  href="{{route('manage.dailyreports')}}">
    <i class=" fas fa-fire-alt"></i><span>Daily Reports</span>
</a>
</li>
<li><a class="nav-link"   href="{{route('manage.dailytroubles')}}">
    <i class=" fas fa-wrench"></i><span>Daily Trouble</span>
</a>
</li>
<li><a class="nav-link"  href="{{route('manage.inventorycontrol')}}">
    <i class=" fas fa-boxes"></i><span>Inventory Report</span>
</a>
</li>
<li><a class="nav-link"  href="{{route('manage.monthly')}}">
    <i class=" fas fa-spinner"></i><span>Monthly Progress</span>
</a>
</li>
<li><a class="nav-link"  href="{{route('manage.account-executive')}}">
    <i class=" fas fa-user-circle"></i><span>Account Executive Progress</span>
</a>
</li>
<li><a class="nav-link" href="{{route('ae-live')}}">
    <i class=" fas fa-link"></i><span> AE Link Live</span>
</a>
</li>
<li><a class="nav-link" href="{{route('countProgress')}}">
    <i class=" fas fa-thumbs-up"></i><span>Akun Count Progress</span>
</a>
</li>
<li><a class="nav-link" href="{{route('editReq')}}">
    <i class=" fas fa-receipt"></i><span>Editing Request</span>
</a>
</li>
@if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 4)
<li><a class="nav-link" href="{{route('editor-reports')}}">
    <i class=" fas fa-file"></i><span>Editor Reports</span>
</a>
</li>
@endif
<li class="nav-item dropdown">
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-money-bill-wave"></i><span>Finance Report</span></a>
    <ul class="dropdown-menu">
        <li><a href="{{ route('admin.property-inv') }}">Property Invenstment</a></li>
        <li><a href="{{ route('admin.purchasing-req') }}">Purchasing Request</a></li>
        <li><a href="{{ route('admin.income') }}">Income</a></li>
    </ul>
</li>
<li><a class="nav-link" href="{{route('admin.Propertyloan')}}">
    <i class=" fas fa-percentage"></i><span> Property Loan Requires</span>
</a>
</li>
<li><a class="nav-link"href="{{route('admin.Reimbursmentprog')}}">
    <i class=" fas fa-keyboard"></i><span> Reimbursment Request</span>
</a>
</li>
<li><a class="nav-link" href="{{route('admin.Cooperation')}}">
    <i class=" fas fa-briefcase"></i><span> Cooperation</span>
</a>
</li>

<li><a class="nav-link" href="{{route('clear-cache')}}">
    <i class=" fas fa-trash-alt"></i><span> Hapus Cache Web</span>
</a>
</li>
@if(Auth::user()->level == 0 || Auth::user()->level == 2)
<li><a class="nav-link" href="{{route('admin.ga-reports')}}">
    <i class=" fas fa-file"></i><span> GA Reports</span>
</a>
</li>

<li><a class="nav-link" href="{{route('admin.poster')}}">
    <i class=" fas fa-image"></i><span> Poster and Information Update</span>
</a>
</li>
@endif
@elseif(Auth::user()->level == 6)

<li><a class="nav-linknav-link" href="{{route('management.dailyworks')}}">
    <i class=" fas fa-suitcase"></i><span>Daily Works</span>
</a>
</li>
<li><a class="nav-link"  href="{{route('management.dailyreports')}}">
    <i class=" fas fa-fire-alt"></i><span>Daily Reports</span>
</a>
</li>
<li><a class="nav-link"   href="{{route('management.dailytroubles')}}">
    <i class=" fas fa-wrench"></i><span>Daily Trouble</span>
</a>
</li>
<li><a class="nav-link"  href="{{route('management.inventorycontrol')}}">
    <i class=" fas fa-boxes"></i><span>Inventory Report</span>
</a>
</li>
<li><a class="nav-link"  href="{{route('management.monthly')}}">
    <i class=" fas fa-spinner"></i><span>Monthly Progress</span>
</a>
</li>
<li><a class="nav-link"  href="{{route('management.account-executive')}}">
    <i class=" fas fa-user-circle"></i><span>Account Executive Progress</span>
</a>
</li>
<li><a class="nav-link" href="{{route('management.ae-live')}}">
    <i class=" fas fa-link"></i><span> AE Link Live</span>
</a>
</li>
<li><a class="nav-link" href="{{route('countProgress')}}">
    <i class=" fas fa-thumbs-up"></i><span>Akun Count Progress</span>
</a>
</li>
<li><a class="nav-link" href="{{route('management.editReq')}}">
    <i class=" fas fa-receipt"></i><span>Editing Request</span>
</a>
</li>
<li><a class="nav-link" href="{{route('management.editor-reports')}}">
    <i class=" fas fa-file"></i><span>Editor Reports</span>
</a>
</li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-money-bill-wave"></i><span>Finance Report</span></a>
    <ul class="dropdown-menu">
        <li><a href="{{ route('management.property-inv') }}">Property Invenstment</a></li>
        <li><a href="{{ route('management.purchasing-req') }}">Purchasing Request</a></li>
        <li><a href="{{ route('management.income') }}">Income</a></li>
    </ul>
</li>
<li><a class="nav-link" href="{{route('management.Propertyloan')}}">
    <i class=" fas fa-percentage"></i><span> Property Loan Requires</span>
</a>
</li>
<li><a class="nav-link"href="{{route('management.Reimbursmentprog')}}">
    <i class=" fas fa-keyboard"></i><span> Reimbursment Request</span>
</a>
</li>
<li><a class="nav-link" href="{{route('management.Cooperation')}}">
    <i class=" fas fa-briefcase"></i><span> Cooperation</span>
</a>
</li>
<li><a class="nav-link" href="{{route('management.GAReports')}}">
    <i class=" fas fa-file"></i><span> GA Reports</span>
</a>
</li>
<li><a class="nav-link" href="{{route('clear-cache')}}">
    <i class=" fas fa-trash-alt"></i><span> Hapus Cache Web</span>
</a>
@endif
{{-- </li>
@elseif(Auth::user()->level == 0 || Auth::user()->level == 2)

@endif --}}