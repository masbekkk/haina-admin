<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <img class="navbar-brand-full app-header-logo" src="{{ asset('img/logo.png') }}" width="50"
             alt="Haina Logo"><br>
        <a href="{{ url('/') }}">Haina Media Dashboard</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <img class="navbar-brand-full" src="{{ asset('img/logo.png') }}" width="45px" alt=""/><br>
        {{-- <a href="{{ url('/') }}" class="small-sidebar-text">
            HM
        </a> --}}
    </div>
    <ul class="sidebar-menu">
        @include('layouts.menu')
    </ul>
</aside>
