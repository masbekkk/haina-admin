@extends('layouts.app')

@section('title')
Manage AE Live
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage AE Live</h3>
      </div>
      <div class="section-body">
              <div class="card">
                <div class="card-header">
                  <a href="{{route('add.ae-live')}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-user-plus"></i> Add Data</a>
                </div>
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                
                                <th>Platforms</th>
                               
                                <th>Live Start</th>
                                <th>Live End</th>
                                <th>Link Live</th>
                                <th>Result Audience</th>
                                <th>Result Selling</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    <tbody>
                    
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        
                        <td>
                            {{$a->platforms}}
                        </td>
                      
                        <td>
                          {{$a->live_start}}
                        </td>
                        <td>
                          {{$a->live_end}}
                        </td>
                        <td>
                          <a href="{{$a->link_live}}" target="_blank" class="btn btn-warning">Klik Untuk Menuju Link Live</a>
                        </td>
                        <td>
                          {{$a->result_audience}}
                        </td>
                        <td>
                            {{$a->result_selling}}
                        </td>
                        <td>
                          <a class="btn btn-icon icon-left btn-warning" href="{{route('edit.ae-live', ['id' => $a->id])}}"><i class="fas fa-pen-alt"></i> Edit Data</a>
                          <a class="btn btn-icon icon-left btn-danger" onclick="return confirm('Yakin ingin menghapus data?')" href="{{route('delete.ae-live', ['id' => $a->id])}}"><i class="fas fa-trash"></i> Hapus Data</a>
                        </td>
                      </tr>
                      @endforeach
                    
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection