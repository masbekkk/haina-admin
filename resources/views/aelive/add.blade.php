@extends('layouts.app') 
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

@endsection
@section('title')
AE Live
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Add AE Live</h3>
  </div>
  <div class="section-body">
            <form action="{{route('store.ae-live')}}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label>Platforms</label>
                        <select class="form-control js-example-basic-single" name="platforms" id="exampleFormControlSelect1">
                            <option value="Shopee">Shopee</option>
                            <option value="Tokopedia">Tokopedia</option>
                            <option value="Instagram">Instagram</option>
                            <option value="Tiktok">Tiktok</option>
                        </select>
                    </div>
                    <div class="form-group row mb-4">
                        <label>Live Start</label>
                        <input type="datetime-local" name="live_start" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Live End</label>
                        <input type="datetime-local" name="live_end" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Link Live</label>
                        <input type="url" name="link_live" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Result Audience</label>
                        <input type="number" name="result_audience" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Result Selling</label>
                        <input type="number" name="result_selling" class="form-control">
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Simpan</button>
                        <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section> 
@endsection 
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- // In your Javascript (external .js resource or <script> tag) --}}
    <script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    </script>
@endsection