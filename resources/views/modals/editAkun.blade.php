<div id="addAkun" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambahkan Akun yang Anda Manage</h5>
                <button type="button" aria-label="Close" class="close outline-none" data-dismiss="modal">×</button>
            </div>
            <form method="POST" id='addAkun' action="{{route('addAkun')}}" enctype="multipart/form-data">
            <div class="modal-body">
                {{-- @if ($errors->any())
                <div class="alert alert-danger d-none" id="">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif --}}
                    {{csrf_field()}}
                    <div class="form-group col-sm-12">
                        <label>Nama Akun :</label><span
                                class="required"></span><span class="required">*</span>
                        <div class="input-group">
                            <input class="form-control input-group__addon" id="pfCurrentPassword" value="" type="text"
                                   name="nama" required>
                            <div class="input-group-append input-group__icon">
                                <span class="input-group-text changeType">
                                    <i class="fas fa-user-plus"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Kategori</label>
                            <select class="form-control" name="kategori" id="exampleFormControlSelect1">
                              <option value="Original">Original</option>
                              <option value="Non Original">Non Original</option>
                              <option value="Account Executive">Account Executive</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Type Akun</label>
                            <select class="form-control" name="type" id="exampleFormControlSelect1">
                              <option value="Story Teller">Story Teller</option>
                              <option value="In Frame">In Frame</option>
                              <option value="Out Frame">Out Frame</option>
                              <option value="Film">Film</option>
                              <option value="Fanbase">Fanbase</option>
                              <option value="Quotes">Quotes</option>
                              <option value="General">General</option>
                              <option value="Product">Product</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Sub Type</label>
                            <select class="form-control" name="subtype" id="exampleFormControlSelect1">
                              <option value="Instagram">Instagram</option>
                              <option value="Tiktok">Tiktok</option>
                            </select>
                          </div>
                    </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" id="btnPrPasswordEditSave" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing...">Save</button>
                    <button type="button" class="btn btn-light ml-1" data-dismiss="modal">Cancel
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

