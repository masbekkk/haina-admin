<div id="addPoster" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambahkan Poster</h5>
                <button type="button" aria-label="Close" class="close outline-none" data-dismiss="modal">×</button>
            </div>
            <form method="POST" id='formPoster' action="{{route('admin.store.poster')}}" enctype="multipart/form-data">
            <div class="modal-body">
                    @csrf
                    <div class="form-group col-sm-12">
                        <label>Poster :</label><span
                                class="required"></span><span class="required"></span>
                        <div class="input-group">
                            <input class="form-control input-group__addon" type="file"
                                   name="link">
                            <div class="input-group-append input-group__icon">
                                <span class="input-group-text changeType">
                                    <i class="fas fa-link"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" id="btnPoster" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing...">Save</button>
                    <button type="button" class="btn btn-light ml-1" data-dismiss="modal">Cancel
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

