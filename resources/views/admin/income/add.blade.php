@extends('layouts.app') 
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

@endsection
@section('title')
Income
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Add Income</h3>
  </div>
  <div class="section-body">
            <form action="{{route('admin.store.income')}}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label>Nama</label>
                        <select class="form-control js-example-basic-single" name="user" id="exampleFormControlSelect1">
                            @foreach ($users as $a)
                            @if($a->level != 0)
                            <option value={{$a->id}}>{{$a->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row mb-4">
                        <label>Company</label>
                        <input type="text" name="company" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Duration Cooperation</label>
                        <input type="text" name="duration" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Pricing Deal</label>
                        <input type="text" name="pricingdeal" class="form-control number-separator">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Status</label>
                        <select class="form-control" name="status" id="exampleFormControlSelect1">
                            {{-- //1=habis pakai;2=tidak habis pakai;3=property shooting --}}
                            <option value=1>On Progress</option>
                            <option value=2>Done</option>
                          </select>
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Simpan</button>
                        <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section> 
@endsection 

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- // In your Javascript (external .js resource or <script> tag) --}}
    <script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>
@endsection