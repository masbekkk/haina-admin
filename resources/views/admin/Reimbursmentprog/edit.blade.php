@extends('layouts.app') 
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

@endsection
@section('title')
Reimbursment Progress
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Add Reimbursment Progress</h3>
  </div>
  <div class="section-body">
            <form action="{{route('admin.update.Reimbursmentprog', ['id' => $data->id])}}" method="post" enctype="multipart/form-data">
              @csrf
              {{method_field('PUT')}}
                <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label>Nama</label>
                        <select class="form-control js-example-basic-single" name="user" id="exampleFormControlSelect1">
                            @foreach ($users as $a)
                            @if($a->level != 0)
                            @if($a->id == $data->user_id)
                            <option selected value={{$a->id}}>{{$a->name}}</option>
                            @else
                                <option value={{$a->id}}>{{$a->name}}</option>
                            @endif
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row mb-4">
                        <label>Request</label>
                        <input type="text" name="rekues" class="form-control" value="{{$data->request}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Quantity</label>
                        <input type="number" name="quantity" class="form-control" value="{{$data->quantity}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Price</label>
                        <input type="text" name="price" class="form-control number-separator" value="{{$data->price}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Submitting Date</label>
                        <input type="date" name="submitdate" class="form-control" value="{{$data->submitdate}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Status</label>
                        <select class="form-control" name="status" id="exampleFormControlSelect1">
                            {{-- //1=habis pakai;2=tidak habis pakai;3=property shooting --}}
                            @if($data->status == 1)
                            <option selected value=1>On Progress</option>
                            <option value=2>Done</option>
                            @else
                            <option value=2>Done</option>
                            <option value=1>On Progress</option>
                            @endif
                          </select>
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Simpan</button>
                        <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section> 
@endsection 

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- // In your Javascript (external .js resource or <script> tag) --}}
    <script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    </script>
@endsection