@extends('layouts.app') 
{{-- @section('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection --}} 
@section('title')
Property Invenstment
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Add Property Invenstment</h3>
  </div>
  <div class="section-body">
            <form action="{{route('admin.store.property-inv')}}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label>Nama Barang</label>
                        <input type="text" name="nama_barang" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Status</label>
                        <select class="form-control" name="status" id="exampleFormControlSelect1">
                            {{-- //1=habis pakai;2=tidak habis pakai;3=property shooting --}}
                            <option value=1>Habis Pakai</option>
                            <option value=2>Tidak Habis Pakai</option>
                            <option value=3>Property Shooting</option>
                          </select>
                    </div>
                    <div class="form-group row mb-4">
                        <label>Harga</label>
                        <input type="text" name="harga" class="form-control number-separator">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Jumlah</label>
                        <input type="number" name="jumlah" class="form-control">
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Simpan</button>
                        <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section> 
@endsection 