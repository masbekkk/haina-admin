@extends('layouts.app') 
{{-- @section('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection --}} 
@section('title')
Property Invenstment
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Edit Property Invenstment</h3>
  </div>
  <div class="section-body">
            <form action="{{route('admin.update.property-inv', ['id' => $data->id])}}" method="post" enctype="multipart/form-data">
              @csrf
              {{method_field('PUT')}}
                <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label>Nama Barang</label>
                        <input type="text" name="nama_barang" class="form-control" value="{{$data->nama_barang}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Status</label>
                        <select class="form-control" name="status" id="exampleFormControlSelect1">
                            {{-- //1=habis pakai;2=tidak habis pakai;3=property shooting --}}
                            @if($data->status == 1)
                            <option selected value=1>Habis Pakai</option>
                            @elseif($data->status == 2)
                            <option selected value=2> Tidak Habis Pakai</option>
                            @elseif($data->status == 3)
                            <option selected value=3>Property Shooting</option>
                            @endif
                            <option value=1>Habis Pakai</option>
                            <option value=2>Tidak Habis Pakai</option>
                            <option value=3>Property Shooting</option>
                          </select>
                    </div>
                    <div class="form-group row mb-4">
                        <label>Harga</label>
                        <input type="text" name="harga" class="form-control number-separator" value="{{$data->harga}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Jumlah</label>
                        <input type="number" name="jumlah" class="form-control" value="{{$data->jumlah}}">
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Update</button>
                        <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section> 
@endsection 