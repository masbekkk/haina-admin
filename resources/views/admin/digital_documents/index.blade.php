@extends('layouts.app')

@section('title')
Manage Digital Documents
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Digital Documents 
              @if($kategori == "Rules and Regulation") (Rules and Regulation)
              @elseif($kategori == "Ratecard Estimation") (Ratecard Estimation)
              @endif
        </h3>
      </div>
      <div class="section-body">
              <div class="card">
                <div class="card-header">
                @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                  <a data-toggle="modal" data-target="#addFile" class="btn btn-icon icon-left btn-primary" data-keyboard="false" data-backdrop="false"><i class="fas fa-user-plus"></i> Add Data</a>
                @endif
                </div>
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                <th>Downloads</th>
                                <th>Nama File</th>
                                @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                                <th>Aksi</th>
                                @endif
                                <th>Tgl Dibuat</th>
                                <th>Tgl Diupdate</th>
                               
                            </tr>
                        </thead>
                    <tbody>
                    @if($ada == 1)
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        <td>
                            
                            <a class="btn btn-dark" href="{{route('download.digital_documents', ['id' => $a->id])}}">Download File</a>
                          
                        </td>
                        <td>
                            <span class="badge badge-danger">{{$a->nama_file}}</span>
                        </td>
                        
                        @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                        <td>
                          <a class="btn btn-icon icon-left btn-warning" data-toggle="modal" data-target="#edit{{$a->id}}"><i class="fas fa-pen-alt"></i> Edit Data</a>
                         
                          <a class="btn btn-icon icon-left btn-danger" onclick="return confirm('Yakin ingin menghapus data?')" href="{{route('admin.delete.digital_documents', ['id' => $a->id])}}"><i class="fas fa-trash"></i> Hapus Data</a>
                        </td>
                        @endif
                        <td>
                          {{date('d M Y H:m', strtotime($a->created_at))}}
                         </td>
                         <td>
                           {{date('d M Y H:m ', strtotime($a->updated_at))}}
                        </td>
                        
                      </tr>
                      <div id="edit{{$a->id}}" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Update File</h5>
                                    <button type="button" aria-label="Close" class="close" data-dismiss="modal">×</button>
                                </div>
                                <form method="POST" action="{{route('admin.update.digital_documents', ['id' => $a->id])}}" enctype="multipart/form-data">
                                <div class="modal-body">
                                    @csrf
                                    {{method_field('PUT')}}
                                    <div class="form-group">
                                        <label>Nama File :</label>
                                        <input type="text" class="form-control" name="nama_file" placeholder="Nama File" aria-label="Nama File" value="{{$a->nama_file}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Kategori :</label>
                                        <select class="form-control" name="kategori">
                                            @if($a->kategori == 1)
                                            <option selected value=1>Rules and Regulation</option>
                                            <option value=2>Ratecard Estimation</option>
                                            @elseif($a->kategori == 2)
                                            <option value=1>Rules and Regulation</option>
                                            <option selected value=2>Ratecard Estimation</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>File :</label>
                                        <input type="file" class="form-control" name="file" placeholder="File" aria-label="File">
                                      
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary btn-lg" id="btnFile" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing...">Save</button>
                                        <button type="button" class="btn btn-light ml-1" data-dismiss="modal">Cancel
                                        </button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- endmodal                 --}}


                      @endforeach
                      @else
                      @endif
                      
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
      <div id="addFile" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambahkan File</h5>
                    <button type="button" aria-label="Close" class="close" data-dismiss="modal">×</button>
                </div>
                <form method="POST" id='formFile' action="{{route('admin.store.digital_documents')}}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label>Nama File :</label>
                        <input type="text" class="form-control" name="nama_file" placeholder="Nama File" aria-label="Nama File">
                    </div>
                    <div class="form-group">
                        <label>Kategori :</label>
                        <select class="form-control" name="kategori">
                            <option value=1>Rules and Regulation</option>
                            <option value=2>Ratecard Estimation</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>File :</label>
                        <input type="file" class="form-control" name="file" placeholder="File" aria-label="File">
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary btn-lg" id="btnFile" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing...">Save</button>
                        <button type="button" class="btn btn-light ml-1" data-dismiss="modal">Cancel
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- endmodal                 --}}
    </section>
    
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection