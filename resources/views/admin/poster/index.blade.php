@extends('layouts.app')

@section('title')
Manage Poster dan Information Update
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Poster dan Information Update</h3>
      </div>
      <div class="section-body">
              <div class="card">
                <div class="card-header">
                  <a data-toggle="modal" data-target="#addPoster" class="btn btn-icon icon-left btn-primary" data-keyboard="false" data-backdrop="false"><i class="fas fa-user-plus"></i> Add Data</a>
         
                </div>
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                                <th>Poster</th>
                                @endif
                                <th>Aksi</th>
                                @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                                <th>Tgl Dibuat</th>
                                <th>Tgl Diupdate</th>
                                @endif
                            </tr>
                        </thead>
                    <tbody>
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                        <td>
                            
                            <a class="btn btn-dark" data-toggle="modal" data-target="#bukti{{$a->id}}" data-keyboard="false" data-backdrop="false">Lihat Poster</a>
                          
                        </td>
                        @endif

                        <td>
                          <a class="btn btn-icon icon-left btn-warning" data-toggle="modal" data-target="#edit{{$a->id}}"><i class="fas fa-pen-alt"></i> Edit Data</a>
                         
                          <a class="btn btn-icon icon-left btn-danger" onclick="return confirm('Yakin ingin menghapus data?')" href="{{route('admin.delete.poster', ['id' => $a->id])}}"><i class="fas fa-trash"></i> Hapus Data</a>
                        </td>
                        @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                        <td>
                          {{date('d M Y H:m', strtotime($a->created_at))}}
                         </td>
                         <td>
                           {{date('d M Y H:m ', strtotime($a->updated_at))}}
                        </td>
                        @endif
                      </tr>
                      <div id="edit{{$a->id}}" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Update Poster</h5>
                                    <button type="button" aria-label="Close" class="close" data-dismiss="modal">×</button>
                                </div>
                                <form method="POST" action="{{route('admin.update.poster', ['id' => $a->id])}}" enctype="multipart/form-data">
                                <div class="modal-body">
                                    @csrf
                                    {{method_field('PUT')}}
                                    <div class="section-title mt-0"><text>Poster Sebelumnya </text></div>
                                    <img src="{{asset($a->gambar)}}" width="100%">
                                    <div class="form-group">
                                        <label>Poster :</label>
                                        <input type="file" class="form-control" name="gambar" placeholder="Poster" aria-label="Poster">
                                        {{-- <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">@</span>
                                            </div>
    
                                        </div> --}}
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary btn-lg" id="btnPoster" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing...">Save</button>
                                        <button type="button" class="btn btn-light ml-1" data-dismiss="modal">Cancel
                                        </button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- endmodal                 --}}

                    <div class="modal fade" id="bukti{{$a->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                        <div class="modal-dialog bg-white">
                        <div class="modal-content bg-white">
                           <div class="modal-header">
                           <h5 class="modal-title" id="exampleModalLabel">Detail Poster</h5>
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     
                           </button>
                           </div>
                           <div class="modal-body">
                              <img src="{{ asset($a->gambar) }}" width = "100%">
                           </div>
                           <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           {{-- <a href="/user/profile/edit" class="btn btn-primary">Edit Profil</a> --}}
                           </div>
                        </div>
                        </div>
                     </div>
                      @endforeach
                      
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
      <div id="addPoster" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambahkan Poster</h5>
                    <button type="button" aria-label="Close" class="close" data-dismiss="modal">×</button>
                </div>
                <form method="POST" id='formPoster' action="{{route('admin.store.poster')}}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label>Poster :</label>
                        <input type="file" class="form-control" name="gambar" placeholder="Poster" aria-label="Poster">
                        {{-- <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">@</span>
                            </div>

                        </div> --}}
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary btn-lg" id="btnPoster" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing...">Save</button>
                        <button type="button" class="btn btn-light ml-1" data-dismiss="modal">Cancel
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- endmodal                 --}}
    </section>
    
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection