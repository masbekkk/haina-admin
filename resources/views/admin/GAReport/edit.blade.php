@extends('layouts.app') 
{{-- @section('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection --}} 
@section('title')
GA Reports
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Add GA Reports</h3>
  </div>
  <div class="section-body">
            <form action="{{route('admin.update.ga-reports', ['id' => $data->id])}}" method="post" enctype="multipart/form-data">
              @csrf
              {{method_field('PUT')}}
                <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label>Date Of Report</label>
                        <input type="date" name="date_of_reports" class="form-control" value="{{$data->date_of_reports}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Today Work</label>
                        <textarea name="today_works" class="form-control">{{$data->today_works}}</textarea>
                    </div>
                    <div class="form-group row mb-4">
                        <label>Number Of Works</label>
                        <input type="number" name="number_of_works" class="form-control" value="{{$data->number_of_works}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Working Of Trouble</label>
                        <textarea name="working_of_trouble" class="form-control">{{$data->working_of_trouble}}</textarea>
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Simpan</button>
                        <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section> 
@endsection 