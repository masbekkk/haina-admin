@extends('layouts.app')

@section('title')
Manage Property Loan Requires
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Property Loan Requires</h3>
      </div>
      <div class="section-body">
              <div class="card">
                <div class="card-header">
                  @if($ada != 2 || Auth::user()->level == 0)
                  <a href="{{route('admin.add.Propertyloan')}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-user-plus"></i> Add Data</a>
                  @else
                  <h1>Property Loan Requires</h1>
                  @endif
                </div>
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                @if($ada == 2)
                                <th>Nama</th>
                                @endif
                                <th>Kebutuhan</th>
                                <th>Tanggal Pinjam</th>
                                <th>Tanggal Pengembalian</th>
                                <th>Nama Property</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    <tbody>
                      @if($ada != 0) {{-- cek data --}}
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        @if($ada == 2)
                        <td>
                            {{$a->user->name}}
                        </td>
                        @endif
                        <td>
                          {{$a->kebutuhan}}
                        </td>
                        <td>
                          {{$a->tanggal_pinjam}}
                        </td>
                        <td>
                          {{$a->tanggal_kembali}}
                        </td>
                        <td>
                          {{$a->property->nama_barang}}
                        </td>
                        <td>
                            @if($a->status == 1)
                              <span class="badge badge-danger">On Progress</span>
                            @elseif($a->status == 2)
                              <span class="badge badge-success">Done</span>
                            @endif
                            {{-- @if($ada == 2) cek admin / ga  --}}
                        <td>
                          @if($a->status == 1)
                          @if($ada == 2) {{-- cek admin / ga --}}  
                          <a class="btn btn-icon icon-left btn-success" href="{{route('admin.done.Propertyloan', ['id' => $a->id])}}"><i class="fas fa-pen-alt"></i> Make as Done</a>
                          @endif {{-- end cek admin / ga --}}
                          @elseif($a->status == 2)
                          @if($ada == 2) {{-- cek admin / ga --}}  
                          <a class="btn btn-icon icon-left btn-secondary" href="{{route('admin.on-progress.Propertyloan', ['id' => $a->id])}}"><i class="fas fa-pen-alt"></i> Make as On Progress</a>
                          @endif {{-- end cek admin / ga --}}
                          @endif
                          @if($ada != 2 || Auth::user()->level == 0) {{-- cek non admin / ga --}}  
                          <a class="btn btn-icon icon-left btn-warning" href="{{route('admin.edit.Propertyloan', ['id' => $a->id])}}"><i class="fas fa-pen-alt"></i> Edit Data</a>
                          <a class="btn btn-icon icon-left btn-danger" onclick="return confirm('Yakin ingin menghapus data?')" href="{{route('admin.delete.Propertyloan', ['id' => $a->id])}}"><i class="fas fa-trash"></i> Hapus Data</a>
                          @endif
                        </td>
                        {{-- @endif --}}
                      </tr>
                      @endforeach
                      @else
                      @endif {{-- end cek data --}}
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection