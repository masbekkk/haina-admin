@extends('layouts.app')

@section('title')
Manage Purchasing Request
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Purchasing Request</h3>
      </div>
      <div class="section-body">
              <div class="card">
                <div class="card-header">
                  <a href="{{route('admin.add.purchasing-req')}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-user-plus"></i> Add Data</a>
                </div>
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                <th>Nama</th>
                                <th>Kebutuhan</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Photo</th>
                                <th>Acc/ Decline</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    <tbody>
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        <td>
                            {{$a->user->name}}
                        </td>
                        <td>
                           {{$a->kebutuhan}}
                        </td>
                        <td>
                          {{$a->harga}}
                        </td>
                        <td>
                            {{$a->jumlah}}
                        </td>
                        <td>
                          <img src="{{asset($a->foto)}}" alt="Foto" class="img-thumbnail" width="40" data-toggle="modal" data-keyboard="false" data-backdrop="false" data-target="#bukti{{$a->id}}">
                        </td>
                        <td>
                        @if($a->status == 1)
                          <a class="btn btn-icon icon-left btn-success" href="{{route('admin.acc.purchasing-req', ['id' => $a->id])}}"><i class="fas fa-pen-alt"></i> Acc</a>
                        @elseif($a->status == 2)
                          <a class="btn btn-icon icon-left btn-secondary" href="{{route('admin.decline.purchasing-req', ['id' => $a->id])}}"><i class="fas fa-pen-alt"></i> Decline</a>
                        @endif 
                        </td> 
                        <td>
                          <a class="btn btn-icon icon-left btn-warning" href="{{route('admin.edit.purchasing-req', ['id' => $a->id])}}"><i class="fas fa-pen-alt"></i> Edit Data</a>
                          <a class="btn btn-icon icon-left btn-danger" onclick="return confirm('Yakin ingin menghapus data?')" href="{{route('admin.delete.purchasing-req', ['id' => $a->id])}}"><i class="fas fa-trash"></i> Hapus Data</a>
                        </td>
                      </tr>
                      <div class="modal fade" id="bukti{{$a->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                        <div class="modal-dialog bg-white">
                        <div class="modal-content bg-white">
                           <div class="modal-header">
                           <h5 class="modal-title" id="exampleModalLabel">Detail Bukti</h5>
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     
                           </button>
                           </div>
                           <div class="modal-body">
                              <img src="{{ asset($a->foto) }}" width = "100%">
                           </div>
                           <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           {{-- <a href="/user/profile/edit" class="btn btn-primary">Edit Profil</a> --}}
                           </div>
                        </div>
                        </div>
                     </div>
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection