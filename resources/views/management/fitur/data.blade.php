@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Daily Works</h3>
      </div>
     
      <div class="section-body">
              <div class="card">
                <div class="card-header">
                  <a data-toggle="modal" data-target="#addAkun" href="#" data-id="{{ \Auth::id() }}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-user-plus"></i> Tambah Akun</a>
                </div>
                <div class="card-body">
                    <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://docs.google.com/spreadsheets/d/1NamqxQ3ZZa_cu0ofXJ76OHKmgSl4CCD8d8zX0m0IEnY/edit?usp=sharing" allowfullscreen></iframe>
                    </div>
                </div>
              </div>
      </div>
    </section>
@endsection

    