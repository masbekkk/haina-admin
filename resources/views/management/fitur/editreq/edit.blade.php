@extends('layouts.app')

@section('title')
Add Editing Request
@endsection

@section('css') 
<link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" /> 
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
@endsection 

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Edit Editing Request</h3>
      </div>
      <div class="section-body">
        <div class="card">
        <div class="card-header">
            <div class="alert alert-primary alert-has-icon alert-lg">
            <div class="alert-icon">
                <i class="far fa-user-circle"></i>
            </div>
            <div class="alert-body">
                <div class="alert-title">Nama Akun : {{$data->nama}} </div>
            </div>
            </div>
        </div>
        <div class="card-body">
            <form id="form" action="{{route('updateeditReq', ['id' => $data->id])}}" method="POST" enctype="multipart/form-data" >
                @csrf
                <div class="form-group">
                    <label>Concept</label>
                    <textarea class="form-control" name="concept">{{$req->concept}}</textarea>
                </div>
                <div class="form-group">
                    <label>Estimeted Posting</label>
                    <input type="date" class="form-control" name="estimeted_posting" value="{{$req->estimeted_posting}}">
                </div>
                <div class="text-left">
                    <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Simpan</button>
                    <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<script>
    $('#summernote').summernote({
      placeholder: 'Isi konsepnya disini',
      tabsize: 2,
      height: 100,
      codeviewFilter: false,
      codeviewIframeFilter: true,
      codeviewFilterRegex: 'custom-regex',
      codeviewIframeWhitelistSrc: ['my-own-domainname']

    });
    
  </script>
  @endsection