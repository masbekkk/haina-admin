@extends('layouts.app') 
{{-- @section('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection --}} 
@section('title')
Daily Works
@endsection

@section('content') 
<section class="section">
  <div class="section-header" class="form-control">
    <h3 class="page__heading">View Daily Works Akun</h3>
  </div>
  <div class="section-body">
    @if($ada == 1)
    <div class="card">
      <form action="{{route('adddaily', ['id' => $data->id])}}" method="post" enctype="multipart/form-data">
          <div class="card-header" class="form-control">
            <div class="alert alert-primary alert-has-icon alert-lg">
              <div class="alert-icon">
                <i class="far fa-user-circle"></i>
              </div>
              <div class="alert-body">
                <div class="alert-title">Nama Akun : {{$data->nama}} </div>
              </div>
            </div>
            <button type="submit" class="btn-icon icon-left btn-success btn-lg"><i class="fas fa-save"></i> Save</button>
          </div>
          <div class="card-body">
            <div class="table-responsive table">
              <table class="table table-striped" id="table-1">
                <thead>
                  <tr>
                    <th>Daily Works</th>
                    <th>Senin</th>
                    <th>Selasa</th>
                    <th>Rabu</th>
                    <th>Kamis</th>
                    <th>Jum'at</th>
                    <th>Sabtu</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item">Script</li>
                        <li class="list-group-item">Editing</li>
                        <li class="list-group-item">Shooting</li>
                        <li class="list-group-item">Posting</li>
                        <li class="list-group-item">Administration</li>
                      </ul>
                    </td> 
                    @csrf 
                    @for($i = 1; $i<=6; $i++) 
                    <td>
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                          <input disabled type="number" class="script{{$i}}" name="script{{$i}}">
                        </li>
                        <li class="list-group-item">
                          <input disabled type="number" class="edit{{$i}}" name="edit{{$i}}">
                        </li>
                        <li class="list-group-item">
                          <input disabled type="number" class="shooting{{$i}}" name="shooting{{$i}}">
                        </li>
                        <li class="list-group-item">
                          <input disabled type="number" class="posting{{$i}}" name="posting{{$i}}">
                        </li>
                        <li class="list-group-item">
                          <input disabled type="number" class="admin{{$i}}" name="admin{{$i}}">
                        </li>
                      </ul>
                    </td>
                    @endfor
                    <td>
                      <ul class="list-group list-group-flush">
                        @for($i = 1; $i<=5; $i++) 
                        <li class="list-group-item">
                          <input disabled disabled type="number" class="total{{$i}}" name="total{{$i}}">
                        </li>
                        @endfor
                      </ul>
                    </td>
                    
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </form>
    </div>
    @else
    <div class="card">
          <form action="{{route('updatedaily', ['id' => $data->id])}}" method="post" enctype="multipart/form-data"> 
            @csrf 
            {{ method_field('put disabled') }}
            <div class="card-header" class="form-control">
              <div class="alert alert-primary alert-has-icon alert-lg">
                <div class="alert-icon">
                  <i class="far fa-user-circle"></i>
                </div>
                <div class="alert-body">
                  <div class="alert-title">Nama Akun : {{$data->nama}} </div>
                </div>
              </div>
              <button type="submit" class="btn-icon icon-left btn-warning btn-lg"><i class="fas fa-pen-alt"></i> Update</button>
            </div>
            <div class="card-body">
              <div class="table-responsive table">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th>Daily Works</th>
                      <th>Senin</th>
                      <th>Selasa</th>
                      <th>Rabu</th>
                      <th>Kamis</th>
                      <th>Jum'at</th>
                      <th>Sabtu</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">Script</li>
                          <li class="list-group-item">Editing</li>
                          <li class="list-group-item">Shooting</li>
                          <li class="list-group-item">Posting</li>
                          <li class="list-group-item">Administration</li>
                        </ul>
                      </td> 
                      @csrf 
                      @for($i = 1; $i<=6; $i++) 
                      <td>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">
                            @if($i == 1)
                                <input disabled type="number" class="script{{$i}}" name="script{{$i}}" value="{{$script->senin}}">
                            @elseif($i == 2)
                                <input disabled type="number" class="script{{$i}}" name="script{{$i}}" value="{{$script->selasa}}">
                            @elseif($i == 3)
                                <input disabled type="number" class="script{{$i}}" name="script{{$i}}" value="{{$script->rabu}}">
                            @elseif($i == 4)
                                <input disabled type="number" class="script{{$i}}" name="script{{$i}}" value="{{$script->kamis}}">
                            @elseif($i == 5)
                                <input disabled type="number" class="script{{$i}}" name="script{{$i}}" value="{{$script->jumat}}">
                            @elseif($i == 6)
                                <input disabled type="number" class="script{{$i}}" name="script{{$i}}" value="{{$script->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                            @if($i == 1)
                                <input disabled type="number" class="edit{{$i}}" name="edit{{$i}}" value="{{$Edit->senin}}">
                            @elseif($i == 2)
                                <input disabled type="number" class="edit{{$i}}" name="edit{{$i}}" value="{{$Edit->selasa}}">
                            @elseif($i == 3)
                                <input disabled type="number" class="edit{{$i}}" name="edit{{$i}}" value="{{$Edit->rabu}}">
                            @elseif($i == 4)
                                <input disabled type="number" class="edit{{$i}}" name="edit{{$i}}" value="{{$Edit->kamis}}">
                            @elseif($i == 5)
                                <input disabled type="number" class="edit{{$i}}" name="edit{{$i}}" value="{{$Edit->jumat}}">
                            @elseif($i == 6)
                                <input disabled type="number" class="edit{{$i}}" name="edit{{$i}}" value="{{$Edit->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                            @if($i == 1)
                                <input disabled type="number" class="shooting{{$i}}" name="shooting{{$i}}" value="{{$Shooting->senin}}">
                            @elseif($i == 2)
                                <input disabled type="number" class="shooting{{$i}}" name="shooting{{$i}}" value="{{$Shooting->selasa}}">
                            @elseif($i == 3)
                                <input disabled type="number" class="shooting{{$i}}" name="shooting{{$i}}" value="{{$Shooting->rabu}}">
                            @elseif($i == 4)
                                <input disabled type="number" class="shooting{{$i}}" name="shooting{{$i}}" value="{{$Shooting->kamis}}">
                            @elseif($i == 5)
                                <input disabled type="number" class="shooting{{$i}}" name="shooting{{$i}}" value="{{$Shooting->jumat}}">
                            @elseif($i == 6)
                                <input disabled type="number" class="shooting{{$i}}" name="shooting{{$i}}" value="{{$Shooting->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                            @if($i == 1)
                                <input disabled type="number" class="posting{{$i}}" name="posting{{$i}}" value="{{$posting->senin}}">
                            @elseif($i == 2)
                                <input disabled type="number" class="posting{{$i}}" name="posting{{$i}}" value="{{$posting->selasa}}">
                            @elseif($i == 3)
                                <input disabled type="number" class="posting{{$i}}" name="posting{{$i}}" value="{{$posting->rabu}}">
                            @elseif($i == 4)
                                <input disabled type="number" class="posting{{$i}}" name="posting{{$i}}" value="{{$posting->kamis}}">
                            @elseif($i == 5)
                                <input disabled type="number" class="posting{{$i}}" name="posting{{$i}}" value="{{$posting->jumat}}">
                            @elseif($i == 6)
                                <input disabled type="number" class="posting{{$i}}" name="posting{{$i}}" value="{{$posting->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                            @if($i == 1)
                                <input disabled type="number" class="admin{{$i}}" name="admin{{$i}}" value="{{$admin->senin}}">
                            @elseif($i == 2)
                                <input disabled type="number" class="admin{{$i}}" name="admin{{$i}}" value="{{$admin->selasa}}">
                            @elseif($i == 3)
                                <input disabled type="number" class="admin{{$i}}" name="admin{{$i}}" value="{{$admin->rabu}}">
                            @elseif($i == 4)
                                <input disabled type="number" class="admin{{$i}}" name="admin{{$i}}" value="{{$admin->kamis}}">
                            @elseif($i == 5)
                                <input disabled type="number" class="admin{{$i}}" name="admin{{$i}}" value="{{$admin->jumat}}">
                            @elseif($i == 6)
                                <input disabled type="number" class="admin{{$i}}" name="admin{{$i}}" value="{{$admin->sabtu}}">
                            @endif
                          </li>
                        </ul>
                        </td> 
                        @endfor
                        <td>
                          <ul class="list-group list-group-flush">
                            @for($i = 1; $i<=5; $i++) 
                            <li class="list-group-item">
                              <input disabled disabled type="number" class="total{{$i}}" name="total{{$i}}">
                            </li>
                            @endfor
                          </ul>
                        </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </form>
      </div>

    @endif
  </div>
</section> 
@endsection 
{{-- @section('scripts')


							<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
							<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
							<script>
$(document).ready(function() {
    $('#table-1').DataTable();
})
</script>

@endsection --}}