@extends('layouts.app') 
{{-- @section('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection --}} 
@section('title')
Inventory Report
@endsection

@section('content') 
<section class="section">
  <div class="section-header" class="form-control">
    <h3 class="page__heading">Manage Inventory Report Akun</h3>
  </div>
  <div class="section-body">
    @if($ada == 1)
    <div class="card">
      <form action="{{route('add.inventory-control', ['id' => $data->id])}}" method="post" enctype="multipart/form-data">
          <div class="card-header" class="form-control">
            <div class="alert alert-primary alert-has-icon alert-lg">
              <div class="alert-icon">
                <i class="far fa-user-circle"></i>
              </div>
              <div class="alert-body">
                <div class="alert-title">Nama Akun : {{$data->nama}} </div>
              </div>
            </div>
            <button type="submit" class="btn-icon icon-left btn-success btn-lg"><i class="fas fa-save"></i> Save</button>
          </div>
          <div class="card-body">
            <div class="table-responsive table">
              <table class="table table-striped" id="table-1">
                <thead>
                  <tr>
                    <th>Inventory Control</th>
                    <th>Senin</th>
                    <th>Selasa</th>
                    <th>Rabu</th>
                    <th>Kamis</th>
                    <th>Jum'at</th>
                    <th>Sabtu</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item">Inventory Last Week</li>
                        <li class="list-group-item">Finish</li>
                        <li class="list-group-item">Posting</li>
                        <li class="list-group-item">Inventory</li>
                      </ul>
                    </td> 
                    @csrf 
                    @for($i = 1; $i<=6; $i++) 
                    <td>
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                          <input disabled type="number" class="Invlastweek{{$i}}" name="Invlastweek{{$i}}">
                        </li>
                        <li class="list-group-item">
                          <input disabled type="number" class="Finish{{$i}}" name="Finish{{$i}}">
                        </li>
                        <li class="list-group-item">
                        @if($bisa == 1)
                          @if($i == 1)
                          <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->senin}}">
                          @elseif($i == 2)
                            <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->selasa}}">
                          @elseif($i == 3)
                            <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->rabu}}">
                          @elseif($i == 4)
                            <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->kamis}}">
                          @elseif($i == 5)
                            <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->jumat}}">
                          @elseif($i == 6)
                            <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->sabtu}}">
                          @endif
                        @else
                        <input disabled type="number" class="post{{$i}}" name="posting{{$i}}">
                        @endif
                        </li>
                        <li class="list-group-item">
                        @if($bisa == 1)
                          @if($i == 1)
                              <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->senin}}">
                          @elseif($i == 2)
                              <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->selasa}}">
                          @elseif($i == 3)
                              <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->rabu}}">
                          @elseif($i == 4)
                              <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->kamis}}">
                          @elseif($i == 5)
                              <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->jumat}}">
                          @elseif($i == 6)
                              <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->sabtu}}">
                          @endif
                        @else
                          <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}">
                        @endif
                        </li>
                      </ul>
                    </td>
                    @endfor
                    <td>
                      <ul class="list-group list-group-flush">
                        @for($i = 1; $i<=4; $i++) 
                        <li class="list-group-item">
                          <input disabled disabled type="number" class="total{{$i}}" name="total{{$i}}">
                        </li>
                        @endfor
                      </ul>
                    </td>
                    
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </form>
    </div>
    @else
    <div class="card">
          <form action="{{route('update.inventory-control', ['id' => $data->id])}}" method="post" enctype="multipart/form-data"> 
            @csrf 
            {{ method_field('PUT') }}
            <div class="card-header" class="form-control">
              <div class="alert alert-primary alert-has-icon alert-lg">
                <div class="alert-icon">
                  <i class="far fa-user-circle"></i>
                </div>
                <div class="alert-body">
                  <div class="alert-title">Nama Akun : {{$data->nama}} </div>
                </div>
              </div>
              <button type="submit" class="btn-icon icon-left btn-warning btn-lg"><i class="fas fa-pen-alt"></i> Update</button>
            </div>
            <div class="card-body">
              <div class="table-responsive table">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th>Inventory Control</th>
                      <th>Senin</th>
                      <th>Selasa</th>
                      <th>Rabu</th>
                      <th>Kamis</th>
                      <th>Jum'at</th>
                      <th>Sabtu</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">Inventory Last Week</li>
                          <li class="list-group-item">Finish</li>
                          <li class="list-group-item">Posting</li>
                          <li class="list-group-item">Inventory</li>
                        </ul>
                      </td> 
                      @csrf 
                      @for($i = 1; $i<=6; $i++) 
                      <td>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">
                            @if($i == 1)
                                <input disabled type="number" class="Invlastweek{{$i}}" name="Invlastweek{{$i}}" value="{{$Invlastweek->senin}}">
                            @elseif($i == 2)
                                <input disabled type="number" class="Invlastweek{{$i}}" name="Invlastweek{{$i}}" value="{{$Invlastweek->selasa}}">
                            @elseif($i == 3)
                                <input disabled type="number" class="Invlastweek{{$i}}" name="Invlastweek{{$i}}" value="{{$Invlastweek->rabu}}">
                            @elseif($i == 4)
                                <input disabled type="number" class="Invlastweek{{$i}}" name="Invlastweek{{$i}}" value="{{$Invlastweek->kamis}}">
                            @elseif($i == 5)
                                <input disabled type="number" class="Invlastweek{{$i}}" name="Invlastweek{{$i}}" value="{{$Invlastweek->jumat}}">
                            @elseif($i == 6)
                                <input disabled type="number" class="Invlastweek{{$i}}" name="Invlastweek{{$i}}" value="{{$Invlastweek->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                            @if($i == 1)
                                <input disabled type="number" class="Finish{{$i}}" name="Finish{{$i}}" value="{{$Finish->senin}}">
                            @elseif($i == 2)
                                <input disabled type="number" class="Finish{{$i}}" name="Finish{{$i}}" value="{{$Finish->selasa}}">
                            @elseif($i == 3)
                                <input disabled type="number" class="Finish{{$i}}" name="Finish{{$i}}" value="{{$Finish->rabu}}">
                            @elseif($i == 4)
                                <input disabled type="number" class="Finish{{$i}}" name="Finish{{$i}}" value="{{$Finish->kamis}}">
                            @elseif($i == 5)
                                <input disabled type="number" class="Finish{{$i}}" name="Finish{{$i}}" value="{{$Finish->jumat}}">
                            @elseif($i == 6)
                                <input disabled type="number" class="Finish{{$i}}" name="Finish{{$i}}" value="{{$Finish->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                            @if($i == 1)
                                <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->senin}}">
                            @elseif($i == 2)
                                <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->selasa}}">
                            @elseif($i == 3)
                                <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->rabu}}">
                            @elseif($i == 4)
                                <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->kamis}}">
                            @elseif($i == 5)
                                <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->jumat}}">
                            @elseif($i == 6)
                                <input disabled type="number" class="post{{$i}}" name="posting{{$i}}" value="{{$posting->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                           
                            @if($i == 1)
                                <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->senin}}">
                            @elseif($i == 2)
                                <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->selasa}}">
                            @elseif($i == 3)
                                <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->rabu}}">
                            @elseif($i == 4)
                                <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->kamis}}">
                            @elseif($i == 5)
                                <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->jumat}}">
                            @elseif($i == 6)
                                <input disabled type="number" class="Inven{{$i}}" name="Inventory{{$i}}" value="{{$Inventory->sabtu}}">
                            @endif
                            
                          </li>
                        </ul>
                        </td> 
                        @endfor
                        <td>
                          <ul class="list-group list-group-flush">
                            @for($i = 1; $i<=4; $i++) 
                            <li class="list-group-item">
                              <input disabled disabled type="number" class="total{{$i}}" name="total{{$i}}">
                            </li>
                            @endfor
                          </ul>
                        </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </form>
      </div>

    @endif
  </div>
</section> 
@endsection 
@section('scripts')
<script>
//asyncronus inventory reporrts
for(let j = 1; j<=6; j++){
  $('.Invlastweek'+[j]).on('change', function(){
    var y = +$('.Invlastweek'+1).val() + +$('.Invlastweek'+2).val() + +$('.Invlastweek'+3).val() + +$('.Invlastweek'+4).val() + +$('.Invlastweek'+5).val() + +$('.Invlastweek'+6).val()
    $('.total'+1).val(y)
  });

  $('.post'+[j]).on('change', function(){
    var z = +$('.post'+1).val() + +$('.post'+2).val() + +$('.post'+3).val() + +$('.post'+4).val() + +$('.post'+5).val() + +$('.post'+6).val()
    $('.total'+3).val(z)
  });

  $('.Finish'+[j]).on('change', function(){
    var a = +$('.Finish'+1).val() + +$('.Finish'+2).val() + +$('.Finish'+3).val() + +$('.Finish'+4).val() + +$('.Finish'+5).val() + +$('.Finish'+6).val()
    $('.total'+2).val(a)
  });

  $('.Inven'+[j]).on('change', function(){
    var b = +$('.Inven'+1).val() + +$('.Inven'+2).val() + +$('.Inven'+3).val() + +$('.Inven'+4).val() + +$('.Inven'+5).val() + +$('.Inven'+6).val()
    $('.total'+4).val(b)
  });

}

$( document ).ready(function() {
  y = +$('.Invlastweek'+1).val() + +$('.Invlastweek'+2).val() + +$('.Invlastweek'+3).val() + +$('.Invlastweek'+4).val() + +$('.Invlastweek'+5).val() + +$('.Invlastweek'+6).val()
  $('.total'+1).val(y)

  var z = +$('.post'+1).val() + +$('.post'+2).val() + +$('.post'+3).val() + +$('.post'+4).val() + +$('.post'+5).val() + +$('.post'+6).val()
    $('.total'+3).val(z)
  
  var a = +$('.Finish'+1).val() + +$('.Finish'+2).val() + +$('.Finish'+3).val() + +$('.Finish'+4).val() + +$('.Finish'+5).val() + +$('.Finish'+6).val()
  $('.total'+2).val(a)
  var b = +$('.Inven'+1).val() + +$('.Inven'+2).val() + +$('.Inven'+3).val() + +$('.Inven'+4).val() + +$('.Inven'+5).val() + +$('.Inven'+6).val()
  $('.total'+4).val(b)
});
</script>

@endsection