@extends('layouts.app') 
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

@endsection
@section('title')
Property Loan Requires
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Add Property Loan Requires</h3>
  </div>
  <div class="section-body">
            <form action="{{route('admin.store.Propertyloan')}}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label>Kebutuhan</label>
                        <input type="text" name="kebutuhan" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Tanggal Pinjam</label>
                        <input type="date" name="tanggal_pinjam" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Tanggal Kembali</label>
                        <input type="date" name="tanggal_kembali" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Nama Barang</label>
                        <select class="form-control js-example-basic-single" name="property" id="exampleFormControlSelect1">
                            @foreach ($property as $a)
                            <option value={{$a->id}}>{{$a->nama_barang}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Simpan</button>
                        <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section> 
@endsection 

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- // In your Javascript (external .js resource or <script> tag) --}}
    <script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    </script>
@endsection