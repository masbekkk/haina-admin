@extends('layouts.app')

@section('title')
Manage Property Loan Requires
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Property Loan Requires</h3>
      </div>
      <div class="section-body">
              <div class="card">
               
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                               
                                <th>Nama</th>
                               
                                <th>Kebutuhan</th>
                                <th>Tanggal Pinjam</th>
                                <th>Tanggal Pengembalian</th>
                                <th>Nama Property</th>
                                <th>Status</th>
                              
                            </tr>
                        </thead>
                    <tbody>
                    
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        
                        <td>
                            {{$a->user->name}}
                        </td>
                      
                        <td>
                          {{$a->kebutuhan}}
                        </td>
                        <td>
                          {{$a->tanggal_pinjam}}
                        </td>
                        <td>
                          {{$a->tanggal_kembali}}
                        </td>
                        <td>
                          {{$a->property->nama_barang}}
                        </td>
                        <td>
                            @if($a->status == 1)
                              <span class="badge badge-danger">On Progress</span>
                            @elseif($a->status == 2)
                              <span class="badge badge-success">Done</span>
                            @endif
                            {{-- @if($ada == 2) cek admin / ga  --}}
                       
                        {{-- @endif --}}
                      </tr>
                      @endforeach
                     
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection