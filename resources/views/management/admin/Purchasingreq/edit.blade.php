@extends('layouts.app') 
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

@endsection
@section('title')
Purchasing Request
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Edit Purchasing Request</h3>
  </div>
  <div class="section-body">
            <form action="{{route('admin.update.purchasing-req', ['id' => $data->id])}}" method="post" enctype="multipart/form-data">
              @csrf
              {{method_field('PUT')}}
                <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label>Nama</label>
                        <select class="form-control js-example-basic-single" name="user_id" id="exampleFormControlSelect1">
                            @foreach ($users as $a)
                            @if($a->level != 0)
                            @if($a->id == $data->user_id)
                            <option selected value={{$a->id}}>{{$a->name}}</option>
                            @else
                                <option value={{$a->id}}>{{$a->name}}</option>
                            @endif
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row mb-4">
                        <label>Kebutuhan</label>
                        <input type="text" name="kebutuhan" class="form-control" value="{{$data->kebutuhan}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Harga</label>
                        <input type="text" name="harga" class="form-control number-separator" value="{{$data->harga}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Jumlah</label>
                        <input type="number" name="jumlah" class="form-control" value="{{$data->jumlah}}">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Foto</label>
                        <input accept="image/*" type='file' name="foto" id="imgInp" class="form-control"/>
                        <div class="section-title mt-0"><text>Preview Upload </text></div>
                        <div class="ml-2 col-lg-20">
                            <img id="blah" src="{{asset($data->foto)}}" alt="preview upload" style="max-height: 250px;"/>
                        </div>
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Update</button>
                        <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section> 
@endsection 
@section('scripts')
<script>
imgInp.onchange = evt => {
    const [file] = imgInp.files
    if (file) {
      blah.src = URL.createObjectURL(file)
    }
  }
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- // In your Javascript (external .js resource or <script> tag) --}}
    <script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    </script>
@endsection