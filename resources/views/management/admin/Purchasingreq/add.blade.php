@extends('layouts.app') 
{{-- @section('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection --}} 
@section('title')
Purchasing Request
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Add Purchasing Request</h3>
  </div>
  <div class="section-body">
            <form action="{{route('admin.store.purchasing-req')}}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-4">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Kebutuhan</label>
                        <input type="text" name="kebutuhan" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Harga</label>
                        <input type="text" name="harga" class="form-control number-separator">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Jumlah</label>
                        <input type="number" name="jumlah" class="form-control">
                    </div>
                    <div class="form-group row mb-4">
                        <label>Foto</label>
                        <input accept="image/*" type='file' name="foto" id="imgInp" class="form-control"/>
                        <div class="section-title mt-0"><text>Preview Upload </text></div><br>
                        <img id="blah" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif" alt="preview upload" style="max-height: 250px;"/>
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary btn-lg" id="submit" data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing..." tabindex="5">Simpan</button>
                        <button type="reset" class="btn btn-secondary " name="reset">Bersihkan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section> 
@endsection 
@section('scripts')
<script>
imgInp.onchange = evt => {
    const [file] = imgInp.files
    if (file) {
      blah.src = URL.createObjectURL(file)
    }
  }
</script>
@endsection