@extends('layouts.app')

@section('title')
Manage Income
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Income</h3>
      </div>
      <div class="section-body">
              <div class="card">
                
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                              
                                <th>Nama</th>
                               
                                <th>Company</th>
                                <th>Duration Cooperation</th>
                                <th>Pricing Deals</th>
                                <th>Status</th>
                              
                            </tr>
                        </thead>
                    <tbody>
                    
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                      
                        <td>
                            {{$a->user->name}}
                        </td>
                      
                        <td>
                          {{$a->company}}
                        </td>
                        <td>
                          {{$a->duration}}
                        </td>
                        <td>
                          {{$a->pricingdeal}}
                        </td>
                        <td>
                            @if($a->status == 1)
                              <span class="badge badge-danger">On Progress</span>
                            @elseif($a->status == 2)
                              <span class="badge badge-success">Done</span>
                            @endif
                           {{-- cek admin / ga --}} 
                       
                      </tr>
                      @endforeach
                     
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection