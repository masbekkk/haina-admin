@extends('layouts.app')

@section('title')
Manage GA Reports
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage GA Reports</h3>
      </div>
      <div class="section-body">
              <div class="card">
                
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                            
                                <th>Name of GA</th>
                                
                                <th>Date of Report</th>
                                <th>Today Works</th>
                                <th>Number of Working</th>
                                <th>Working Of Troubles</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    <tbody>
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        
                        <td>
                            {{$a->user->name}}
                        </td>
                        <td>
                          {{$a->date_of_reports}}
                        </td>
                        <td>
                          {{$a->today_works}}
                        </td>
                        <td>
                          {{$a->number_of_works}}
                        </td>
                        <td>
                            {{$a->working_of_trouble}}
                        </td>
                       
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection