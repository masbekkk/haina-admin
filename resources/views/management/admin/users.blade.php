@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">Manage Data Pengguna</h3>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                        <div class="table-responsive table">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Created_at</th>
                                    <th>Ubah Role</th>
                                </tr>
                            </thead>
                        <tbody>
                            @foreach ($data as $a)
                            <tr>
                                <td>
                                {{ $loop->iteration}}
                                </td>
                                <td>{{$a->name}}</td>
                                <td>{{$a->email}}</td>
                                <td>
                                    {{$a->role->nama}}
                                </td>
                                <td>{{date('d M Y H:m:s', strtotime($a->created_at))}}</td>   
                                <td>
                            
                                <div class="dropdown d-inline">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Role User
                                    </button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        {{-- <a class="dropdown-item has-icon" href="#"><i class="far fa-heart"></i> Original</a>
                                        <a class="dropdown-item has-icon" href="#"><i class="far fa-file"></i> Non Original</a> --}}
                                        <a href='/admin/change-role-user/{{$a->id}}/0' class="dropdown-item has-icon"
                                            onclick="return confirm('Yakin ingin mengubah role {{$a->name}} menjadi Admin?')"><i class="fas fa-user-cog"></i> As Admin</a>
                                    
                                        <a href='/admin/change-role-user/{{$a->id}}/2' class="dropdown-item has-icon"
                                            onclick="return confirm('Yakin ingin mengubah role {{$a->name}} menjadi GA?')"><i class="fas fa-user-shield"></i> As GA</a>
                                    
                                        <a href='/admin/change-role-user/{{$a->id}}/3' class="dropdown-item has-icon"
                                            onclick="return confirm('Yakin ingin mengubah role {{$a->name}} menjadi Creator?')"><i class="fas fa-paperclip"></i> As Creator</a>
                                        <a href='/admin/change-role-user/{{$a->id}}/4' class="dropdown-item has-icon"
                                            onclick="return confirm('Yakin ingin mengubah role {{$a->name}} menjadi Editor?')"><i class="fas fa-laptop"></i> As Editor</a>
                                    
                                        <a href='/admin/change-role-user/{{$a->id}}/5' class="dropdown-item has-icon" 
                                            onclick="return confirm('Yakin ingin mengubah role {{$a->name}} menjadi AE?')"><i class="fas fa-table"></i>  As AE</a>
                                    
                                        <a href='/admin/change-role-user/{{$a->id}}/6' class="dropdown-item has-icon"
                                            onclick="return confirm('Yakin ingin mengubah role {{$a->name}} menjadi Management?')"><i class="fas fa-tasks"></i> As Management</a>
                                    </div>
                                
                                </td>
                            
                            </tr>
                            @endforeach
                        {{-- <script type="text/javascript">
                            $(document).ready(function(){
                                $('#item').DataTable({
                                    processing: true,
                                    serverSide: true,
                                    ajax:{
                                        url: "{{route('admin.users')}}",
                                    }, 
                                    columns: [{
                                            data: 'name',
                                            name: 'name'
                                        },
                                        {
                                            data: 'email',
                                            name: 'email'
                                        },
                                        {
                                            data: 'created_at',
                                            name: 'created_at'
                                        },
                                        {
                                            data: 'Aksi',
                                            name: 'Aksi'
                                        },
                                    ]
                                });
                            });
                        </script> --}}
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(document).ready(function() {
		$('#table-1').DataTable();
	})
</script>

@endsection
