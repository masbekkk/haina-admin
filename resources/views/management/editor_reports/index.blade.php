@extends('layouts.app')

@section('title')
Manage Editor Reports
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Editor Reports</h3>
      </div>
      <div class="section-body">
              <div class="card">
             
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                                <th>Name of Editor</th>
                                @endif
                                <th>Date of Report</th>
                                <th>Editing Accounts</th>
                                <th>Number of Editing</th>
                                <th>Working Of Troubles</th>
                                <th>Tgl Dibuat</th>
                                <th>Tgl Diupdate</th>
                            </tr>
                        </thead>
                    <tbody>
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        @if(Auth::user()->level == 0 || Auth::user()->level = 2)
                        <td>
                            {{$a->editor->name}}
                        </td>
                        @endif
                        <td>
                          {{$a->date_of_report}}
                        </td>
                        <td>
                          {{$a->editing_accounts}}
                        </td>
                        <td>
                          {{$a->numbers_of_editing}}
                        </td>
                        <td>
                            {{$a->troubles}}
                        </td>
                        <td>
                         {{date('d M Y H:m', strtotime($a->created_at))}}
                        </td>
                        <td>
                          {{date('d M Y H:m', strtotime($a->updated_at))}}
                         </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection