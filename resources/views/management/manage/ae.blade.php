@extends('layouts.app')

@section('title')
Manage Monthly Progress
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Manage Monthly Progress</h3>
      </div>
     
      <div class="section-body">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                <th>Nama</th>
                                <th>Nama Akun</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    <tbody>
                      @foreach ($data as $a)
                      @if($a->kategori == "Account Executive")
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        <td>
                            {{$a->user->name}}
                        </td>
                        <td>
                          @if($a->kategori == "Account Executive")
                          <span class="badge badge-warning">{{$a->nama}}</span>
                          @else
                          <span class="badge badge-dark">{{$a->nama}}</span>
                          @endif
                        </td>
                        <td>
                          @if($a->kategori == "Account Executive")
                          <a class="btn btn-icon icon-left btn-success" href="/management/monthly/{{$a->id}}"><i class="fas fa-calendar-check"></i> Account Executive Progress</a>
                          @else
                          <a class="btn btn-icon icon-left btn-success" href="/management/monthly/{{$a->id}}"><i class="fas fa-calendar-check"></i> Monthly Progress</a>
                          @endif
                        </td>
                        
                      </tr>
                      @endif
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection
    