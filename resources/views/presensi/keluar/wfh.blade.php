@extends('layouts.app')

@section('title')
Presensi Keluar
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Presensi Keluar</h3>
      </div>
      <div class="section-body">
              <div class="card">
                <div class="card-body">
                    <div class="alert alert-warning alert-has-icon">
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                          <div class="alert-title">Silahkan mengupload bukti bekerja secara WFH</div>
                          Untuk validasi data, silahkan upload foto selfie dengan menggunakan aplikasi <text class="font-weight-bold">GPS Camera</text>.<br>
                          
                        </div>
                      </div>
                        <a class="btn btn-success btn-icon icon-left"
                        href="https://play.google.com/store/apps/details?id=com.gpsmapcamera.geotagginglocationonphoto&hl=en&utm_source=Website%20in&utm_campaign=Custom&showAllReviews=true" 
                        target="_blank"><i class="far fa-google-play"></i> Link Download ke Play Store</a>
                        <a class="btn btn-info btn-icon icon-left"
                        href="https://apps.apple.com/us/app/gps-map-camera-geotag-photos/id1503116917#?platform=iphone" 
                        target="_blank"><i class="far fa-app-store-ios"></i> Link Download ke App Store</a>
                        <br><br><br>
                    <form action="{{route('store.presensi_out', ['status' => 'wfh'])}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="section-title mt-0"><text>Contoh Bukti Presensi WFH</text></div>
                    <small>Klik gambar untuk detailnya</small>
                    {{-- </div> --}}

                    <div class="ml-2 col-lg-20">
                        <img data-toggle="modal" data-keyboard="false" data-backdrop="false" data-target="#contoh_bukti" src="{{asset('img/contoh.jpg')}}" alt="contoh upload" style="max-height: 250px;"/>
                    </div>
                    <br>
                    <div class="form-group row mb-4">
                        <label>Bukti Presensi Keluar WFH</label>
                        <input accept="image/*" type='file' name="bukti_out" id="imgInp" class="form-control"/>
                    </div>
                    <div class="section-title mt-0"><text>Preview Upload </text></div><br>
                    <img id="blah" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif" alt="preview upload" style="max-height: 250px;"/>
                    <br>
                    <div class="btn-group btn-group-lg" role="group">
                        <input type="submit" class="btn btn-info " name="btn_simpan" value="Simpan" data-modal="modal" data-toggle="modal" data-target="#myModal">
                        <button type="reset" class="btn btn-danger " name="reset">Bersihkan</button>
                    </div>
                    </form>
                    
                </div>
              </div>
      </div>
  </section>
  <div class="modal fade" id="contoh_bukti" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog bg-white">
    <div class="modal-content bg-white">
       <div class="modal-header">
       <h5 class="modal-title" id="exampleModalLabel">Detail Contoh</h5>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
 
       </button>
       </div>
       <div class="modal-body">
          <img src="{{ asset('img/contoh.jpg') }}" width = "100%">
       </div>
       <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       {{-- <a href="/user/profile/edit" class="btn btn-primary">Edit Profil</a> --}}
       </div>
    </div>
    </div>
 </div>
  @endsection
  @section('scripts')
    <script>
    imgInp.onchange = evt => {
        const [file] = imgInp.files
        if (file) {
        blah.src = URL.createObjectURL(file)
        }
    }
    </script>
@endsection