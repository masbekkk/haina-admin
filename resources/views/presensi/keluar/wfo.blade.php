@extends('layouts.app')

@section('title')
Presensi Keluar
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Presensi Keluar</h3>
      </div>
      <div class="section-body">
              <div class="card">
                <div class="card-body">
                    <div class="alert alert-warning alert-has-icon">
                        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                        <div class="alert-body">
                          <div class="alert-title">Silahkan mengupload bukti bekerja secara WFO</div>
                        </div>
                      </div>
                    <form action="{{route('store.presensi_out', ['status' => 'wfo'])}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="section-title mt-0"><text>Silahkan mengupload foto barcode</text></div>
                    <div class="form-group row mb-4">
                        <label>Bukti Presensi Keluar WFO</label>
                        <input accept="image/*" type='file' name="bukti_out" id="imgInp" class="form-control" capture/>
                    </div>
                    <div class="section-title mt-0"><text>Preview Upload </text></div><br>
                    <img id="blah" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif" alt="preview upload" style="max-height: 250px;"/>
                    <br>
                    <div class="btn-group btn-group-lg" role="group">
                        <input type="submit" class="btn btn-info " name="btn_simpan" value="Simpan" data-modal="modal" data-toggle="modal" data-target="#myModal">
                        <button type="reset" class="btn btn-danger " name="reset">Bersihkan</button>
                    </div>
                    </form>
                    
                </div>
              </div>
      </div>
  </section>
 </div>
  @endsection
  @section('scripts')
    <script>
    imgInp.onchange = evt => {
        const [file] = imgInp.files
        if (file) {
        blah.src = URL.createObjectURL(file)
        }
    }
    </script>
@endsection