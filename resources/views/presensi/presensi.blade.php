@extends('layouts.app')

@section('title')
Presensi
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Presensi</h3>
      </div>
      <div class="section-body">
              <div class="card">
                <div class="card-body">
                  @if(Auth::user()->level != 6)
                    @if(Auth::user()->status == 2)
                    <div class="alert alert-success alert-has-icon">
                      <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                      <div class="alert-body">
                        <div class="alert-title">Kamu Sudah Presensi secara WFH </div>
                        Terima kasih sudah melakukan Presensi, selamat Work From Home!
                      </div>
                    </div>
                    @elseif(Auth::user()->status == 1)
                    <div class="alert alert-success alert-has-icon">
                      <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                      <div class="alert-body">
                        <div class="alert-title">Kamu Sudah Presensi secara WFO </div>
                        Terima kasih sudah melakukan Presensi, selamat Work From Office!
                      </div>
                    </div>
                    @else
                    <div class="alert alert-danger alert-has-icon">
                      <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                      <div class="alert-body">
                        <div class="alert-title">Kamu Belum melakukan Presensi!</div>
                        Segera lakukan Presensi dengan sesuai dengan kondisi kerja saat ini
                      </div>
                    </div>
                    <div class="btn-group btn-group-lg" role="group" aria-label="...">
                      <a class="btn btn-warning btn-lg" href="/presensi/wfh">WFH</a>
                      <a class="btn btn-success btn-lg" href="/presensi/wfo">WFO</a>
                    </div>
                    @endif
                  @endif
                  @if(Auth::user()->level == 0 || Auth::user()->level == 2 || Auth::user()->level == 6)
                  <br><br>
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                <th>Nama</th>
                                <th>Bekerja Secara</th>
                                <th>Waktu Presensi In</th>
                                <th>Bukti Presensi In</th>
                                <th>Waktu Presensi Out</th>
                                <th>Bukti Presensi Out</th>
                               
                            </tr>
                        </thead>
                    <tbody>
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        <td>
                          {{$a->user->name}}
                        </td>
                        <td>
                          @if($a->status_id == 1)
                           <span class="badge badge-success">WFO</span>
                          @else <span class="badge badge-warning">WFH</span>
                          @endif
                        </td>
                        <td>
                           {{$a->created_at}}
                        </td>
                        <td>
                          <a href="#" class="btn btn-dark" data-toggle="modal" data-keyboard="false" data-backdrop="false" data-target="#bukti_in{{$a->id}}">Lihat Bukti</a>
                        </td>
                        <td>
                            {{$a->presensi_out}}
                        </td>
                      </td>
                      <td>
                        @if($a->presensi_out != NULL)
                        <a href="#" class="btn btn-secondary" data-toggle="modal" data-keyboard="false" data-backdrop="false" data-target="#bukti_out{{$a->id}}">Lihat Bukti Keluar</a>
                        @endif
                      </td>
                       
                      </tr>
                      <div class="modal fade" id="bukti_in{{$a->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                        <div class="modal-dialog bg-white">
                        <div class="modal-content bg-white">
                           <div class="modal-header">
                           <h5 class="modal-title" id="exampleModalLabel">Detail Bukti</h5>
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     
                           </button>
                           </div>
                           <div class="modal-body">
                              <img src="{{ asset($a->bukti_in) }}" width = "100%">
                           </div>
                           <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           {{-- <a href="/user/profile/edit" class="btn btn-primary">Edit Profil</a> --}}
                           </div>
                        </div>
                        </div>
                     </div>

                     <div class="modal fade" id="bukti_out{{$a->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                      <div class="modal-dialog bg-white">
                      <div class="modal-content bg-white">
                         <div class="modal-header">
                         <h5 class="modal-title" id="exampleModalLabel">Detail Bukti</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   
                         </button>
                         </div>
                         <div class="modal-body">
                            <img src="{{ asset($a->bukti_out) }}" width = "100%">
                         </div>
                         <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                         {{-- <a href="/user/profile/edit" class="btn btn-primary">Edit Profil</a> --}}
                         </div>
                      </div>
                      </div>
                   </div>
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                  @endif
                </div>
              </div>
              
            

      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection