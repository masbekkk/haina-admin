@extends('layouts.app') 
{{-- @section('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection --}} 

@section('title')
Daily Reports
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Manage Daily Reports Akun</h3>
  </div>
  <div class="section-body">
    @if($ada == 1)
    <div class="card">
      <form action="{{route('addreports', ['id' => $data->id])}}" method="post" enctype="multipart/form-data">
          <div class="card-header">
            <div class="alert alert-primary alert-has-icon alert-lg">
              <div class="alert-icon">
                <i class="far fa-user-circle"></i>
              </div>
              <div class="alert-body">
                <div class="alert-title">Nama Akun : {{$data->nama}} </div>
              </div>
            </div>
            <button type="submit" class="btn-icon icon-left btn-success btn-lg"><i class="fas fa-save"></i> Save</button>
          </div>
          <div class="card-body">
            <div class="table-responsive table">
              <table class="table table-striped" id="table-1">
                <thead>
                  <tr>
                    <th>Daily Reports</th>
                    <th>Senin (%)</th>
                    <th>Selasa (%)</th>
                    <th>Rabu (%)</th>
                    <th>Kamis (%)</th>
                    <th>Jum'at (%)</th>
                    <th>Sabtu (%)</th>
                    <th>Total (%)</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item">Followers</li>
                        <li class="list-group-item">Increasing</li>
                        <li class="list-group-item">Biggest View</li>
                        
                        <li class="list-group-item">Ratio</li>
                        <li class="list-group-item">Inventory</li>
                        <li class="list-group-item">Actual Posting</li>
                      </ul>
                    </td> 
                    @csrf 
                    @for($i = 1; $i<=6; $i++) 
                    <td>
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                          <input type="number" name="followers{{$i}}" class="followers{{$i}}">
                        </li>
                        <li class="list-group-item">
                          <input type="number" class="increasing{{$i}}" id="increasing{{$i}}" name="increasing{{$i}}">
                        </li>
                        <li class="list-group-item">
                          <input type="number" class="views{{$i}}" id="views{{$i}}" name="views{{$i}}">
                        </li>
                        
                        <li class="list-group-item">
                          <input disabled type="number" class="ratio{{$i}}" id="ratio{{$i}}" name="ratio{{$i}}">
                          <input type="hidden" class="ratio{{$i}}" name="ratio{{$i}}">
                        </li>
                        <li class="list-group-item">
                          <input type="number" name="inventory{{$i}}" class="inventory{{$i}}">
                        </li>
                        <li class="list-group-item">
                          <input type="number" name="actualp{{$i}}" class="actualp{{$i}}">
                        </li>
                      </ul>
                    </td> 
                    @endfor
                    <td>
                      <ul class="list-group list-group-flush">
                        @for($i = 1; $i<=6; $i++) 
                        <li class="list-group-item">
                          <input disabled type="number" class="totalreports{{$i}}" name="total{{$i}}">
                        </li>
                        @endfor
                      </ul>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </form>
    </div>
    @else
    <div class="card">
          <form action="{{route('updatereports', ['id' => $data->id])}}" method="post" enctype="multipart/form-data"> 
            @csrf 
            {{ method_field('PUT') }}
            <div class="card-header">
              <div class="alert alert-primary alert-has-icon alert-lg">
                <div class="alert-icon">
                  <i class="far fa-user-circle"></i>
                </div>
                <div class="alert-body">
                  <div class="alert-title">Nama Akun : {{$data->nama}} </div>
                </div>
              </div>
              <button type="submit" class="btn-icon icon-left btn-warning btn-lg"><i class="fas fa-pen-alt"></i> Update</button>
              <div class="ml-auto w-0">
                @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                <a href="{{ route('exportdailyreports', ['id' => $data->id]) }}" onclick="return confirm('Yakin ingin export to excel?')" class="btn btn-success"><i class="fas fa-file-excel"></i> Export</a>
                <a href="{{ route('resetdailyreports', ['id' => $data->id]) }}" class="btn btn-danger" onclick="return confirm('Yakin ingin reset data?')"><i class="fas fa-trash-restore-alt"></i> Reset</a>
                @endif
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive table">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th>Daily Reports</th>
                      <th>Senin (%)</th>
                      <th>Selasa (%)</th>
                      <th>Rabu (%)</th>
                      <th>Kamis (%)</th>
                      <th>Jum'at (%)</th>
                      <th>Sabtu (%)</th>
                      <th>Total (%)</th>
                      @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                      <th>Tanggal Isi</th>
                      <th>Tanggal Update</th>
                      @endif
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">Followers</li>
                          <li class="list-group-item">Increasing</li>
                          <li class="list-group-item">Biggest View</li>
                          
                          <li class="list-group-item">Ratio</li>
                          <li class="list-group-item">Inventory</li>
                          <li class="list-group-item">Actual Posting</li>
                        </ul>
                      </td> 
                      @csrf 
                      @for($i = 1; $i<=6; $i++) 
                      <td>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">
                            @if($i == 1)
                                <input type="number" class="followers{{$i}}" name="followers{{$i}}" value="{{$followers->senin}}">
                            @elseif($i == 2)
                                <input type="number" class="followers{{$i}}" name="followers{{$i}}" value="{{$followers->selasa}}">
                            @elseif($i == 3)
                                <input type="number" class="followers{{$i}}" name="followers{{$i}}" value="{{$followers->rabu}}">
                            @elseif($i == 4)
                                <input type="number" class="followers{{$i}}" name="followers{{$i}}" value="{{$followers->kamis}}">
                            @elseif($i == 5)
                                <input type="number" class="followers{{$i}}" name="followers{{$i}}" value="{{$followers->jumat}}">
                            @elseif($i == 6)
                                <input type="number" class="followers{{$i}}" name="followers{{$i}}" value="{{$followers->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                            @if($i == 1)
                                <input type="number" class="increasing{{$i}}" name="increasing{{$i}}" value="{{$increasing->senin}}">
                            @elseif($i == 2)
                                <input type="number" class="increasing{{$i}}" name="increasing{{$i}}" value="{{$increasing->selasa}}">
                            @elseif($i == 3)
                                <input type="number" class="increasing{{$i}}" name="increasing{{$i}}" value="{{$increasing->rabu}}">
                            @elseif($i == 4)
                                <input type="number" class="increasing{{$i}}" name="increasing{{$i}}" value="{{$increasing->kamis}}">
                            @elseif($i == 5)
                                <input type="number" class="increasing{{$i}}" name="increasing{{$i}}" value="{{$increasing->jumat}}">
                            @elseif($i == 6)
                                <input type="number" class="increasing{{$i}}" name="increasing{{$i}}" value="{{$increasing->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                            @if($i == 1)
                                <input type="number" class="views{{$i}}" name="views{{$i}}" value="{{$views->senin}}">
                            @elseif($i == 2)
                                <input type="number" class="views{{$i}}" name="views{{$i}}" value="{{$views->selasa}}">
                            @elseif($i == 3)
                                <input type="number" class="views{{$i}}" name="views{{$i}}" value="{{$views->rabu}}">
                            @elseif($i == 4)
                                <input type="number" class="views{{$i}}" name="views{{$i}}" value="{{$views->kamis}}">
                            @elseif($i == 5)
                                <input type="number" class="views{{$i}}" name="views{{$i}}" value="{{$views->jumat}}">
                            @elseif($i == 6)
                                <input type="number" class="views{{$i}}" name="views{{$i}}" value="{{$views->sabtu}}">
                            @endif
                          </li>
                         
                          <li class="list-group-item">
                            @if($i == 1)
                                <input disabled type="number" class="ratio{{$i}}" id="ratio{{$i}}" name="ratio{{$i}}" value="{{$ratio->senin}}">
                                <input type="hidden" class="ratio{{$i}}" name="ratio{{$i}}" value="{{$ratio->senin}}">
                            @elseif($i == 2)
                                <input disabled type="number" class="ratio{{$i}}" id="ratio{{$i}}" name="ratio{{$i}}" value="{{$ratio->selasa}}">
                                <input type="hidden" class="ratio{{$i}}" name="ratio{{$i}}" value="{{$ratio->selasa}}">
                            @elseif($i == 3)
                                <input disabled type="number" class="ratio{{$i}}" id="ratio{{$i}}" name="ratio{{$i}}"> 
                                <input type="hidden" class="ratio{{$i}}" name="ratio{{$i}}" value="{{$ratio->rabu}}">
                            @elseif($i == 4)
                                <input disabled type="number" class="ratio{{$i}}" id="ratio{{$i}}" name="ratio{{$i}}"  value="{{$ratio->kamis}}">
                                <input type="hidden" class="ratio{{$i}}" name="ratio{{$i}}" value="{{$ratio->kamis}}">
                            @elseif($i == 5)
                                <input disabled type="number" class="ratio{{$i}}" id="ratio{{$i}}" name="ratio{{$i}}" value="{{$ratio->jumat}}">
                                <input type="hidden" class="ratio{{$i}}" name="ratio{{$i}}" value="{{$ratio->jumat}}">
                            @elseif($i == 6)
                                <input disabled type="number" class="ratio{{$i}}" id="ratio{{$i}}" name="ratio{{$i}}"  value="{{$ratio->sabtu}}">
                                <input type="hidden" class="ratio{{$i}}" name="ratio{{$i}}" value="{{$ratio->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                            @if($i == 1)
                                <input type="number" class="inventory{{$i}}" name="inventory{{$i}}" value="{{$inventory->senin}}">
                            @elseif($i == 2)
                                <input type="number" class="inventory{{$i}}" name="inventory{{$i}}" value="{{$inventory->selasa}}">
                            @elseif($i == 3)
                                <input type="number" class="inventory{{$i}}" name="inventory{{$i}}" value="{{$inventory->rabu}}">
                            @elseif($i == 4)
                                <input type="number" class="inventory{{$i}}" name="inventory{{$i}}" value="{{$inventory->kamis}}">
                            @elseif($i == 5)
                                <input type="number" class="inventory{{$i}}" name="inventory{{$i}}" value="{{$inventory->jumat}}">
                            @elseif($i == 6)
                                <input type="number" class="inventory{{$i}}" name="inventory{{$i}}" value="{{$inventory->sabtu}}">
                            @endif
                          </li>
                          <li class="list-group-item">
                            @if($i == 1)
                                <input type="number" class="actualp{{$i}}" name="actualp{{$i}}" value="{{$actualp->senin}}">
                            @elseif($i == 2)
                                <input type="number" class="actualp{{$i}}" name="actualp{{$i}}" value="{{$actualp->selasa}}">
                            @elseif($i == 3)
                                <input type="number" class="actualp{{$i}}" name="actualp{{$i}}" value="{{$actualp->rabu}}">
                            @elseif($i == 4)
                                <input type="number" class="actualp{{$i}}" name="actualp{{$i}}" value="{{$actualp->kamis}}">
                            @elseif($i == 5)
                                <input type="number" class="actualp{{$i}}" name="actualp{{$i}}" value="{{$actualp->jumat}}">
                            @elseif($i == 6)
                                <input type="number" class="actualp{{$i}}" name="actualp{{$i}}" value="{{$actualp->sabtu}}">
                            @endif
                          </li>
                        </ul>
                        </td> 
                        @endfor
                        <td>
                          <ul class="list-group list-group-flush">
                            @for($i = 1; $i<=6; $i++) 
                            <li class="list-group-item">
                              <input disabled type="number" class="totalreports{{$i}}" name="total{{$i}}">
                            </li>
                            @endfor
                          </ul>
                        </td>
                        @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                        <td>
                          <ul class="list-group list-group-flush">
                            @for($i = 1; $i<=6; $i++)
                            <li class="list-group-item"> 
                              @if($i == 1)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($followers->created_at))}}">
                              @elseif($i == 2)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($increasing->created_at))}}">
                              @elseif($i == 3)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($views->created_at))}}">
                              @elseif($i == 4)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($ratio->created_at))}}">
                              @elseif($i == 5)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($inventory->created_at))}}">
                              @elseif($i == 6)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($actualp->created_at))}}">
                              @endif
                            </li>
                            @endfor
                          </ul>
                        </td>
                        
                        <td>
                          <ul class="list-group list-group-flush">
                            @for($i = 1; $i<=6; $i++)
                            <li class="list-group-item"> 
                              @if($i == 1)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($followers->updated_at))}}">
                              @elseif($i == 2)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($increasing->updated_at))}}">
                              @elseif($i == 3)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($views->updated_at))}}">
                              @elseif($i == 4)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($ratio->updated_at))}}">
                              @elseif($i == 5)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($inventory->updated_at))}}">
                              @elseif($i == 6)
                              <input disabled type="text" value="{{date('d M Y H:m', strtotime($actualp->updated_at))}}">
                              @endif
                            </li>
                            @endfor
                          </ul>
                        </td>
                        @endif
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </form>
      </div>

    @endif
  </div>
</section> 
@endsection 
