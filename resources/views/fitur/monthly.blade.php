@extends('layouts.app') 
{{-- @section('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection --}} 
@section('title')
Monthly Progress
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Manage Monthly Progress</h3>
  </div>
  <div class="section-body">
    <div class="card">
      <form id="form" action="{{route('monthlyProgress', ['id' => $akun->id])}}" method="post" enctype="multipart/form-akun">
        @csrf   
        <div class="card-header">
            <div class="alert alert-primary alert-has-icon alert-lg">
              <div class="alert-icon">
                <i class="far fa-user-circle"></i>
              </div>
              <div class="alert-body">
                <div class="alert-title">Nama Akun : 
                    {{$akun->nama}} 
                </div>
              </div>
            </div>
            <button type="submit" class="btn-icon icon-left btn-success btn-lg"><i class="fas fa-save"></i> Save</button>
            <div class="ml-auto w-0">
              @if(Auth::user()->level == 0 || Auth::user()->level == 2)
              <a href="{{ route('exportMonthlyprogress', ['id' => $akun->id]) }}" onclick="return confirm('Yakin ingin export to excel?')" class="btn btn-success"><i class="fas fa-file-excel"></i> Export</a>
              <a href="{{ route('resetMonthlyprogress', ['id' => $akun->id]) }}" class="btn btn-danger" onclick="return confirm('Yakin ingin reset data?')"><i class="fas fa-trash-restore-alt"></i> Reset</a>
              @endif
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive table">
              <table class="table table-striped" id="table-1">
                <thead>
                  <tr>
                    <th>Weeks</th>
                    <th>Target</th>
                    <th>Progress</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item">Week 1</li>
                        <li class="list-group-item">Week 2</li>
                        <li class="list-group-item">Week 3</li>
                        <li class="list-group-item">Week 4</li>
                      </ul>
                    </td> 
                    <td>
                      <ul class="list-group list-group-flush">
                        @if($ada == 0)
                        <li class="list-group-item">
                          <input type="text" class="form-control number-separator" name="target1" value="{{$data->target1}}">
                        </li>
                        <li class="list-group-item">
                          <input type="text" class="form-control number-separator" name="target2" value="{{$data->target2}}">
                        </li>
                        <li class="list-group-item">
                          <input type="text" class="form-control number-separator" name="target3" value="{{$data->target3}}">
                        </li>
                        <li class="list-group-item">
                          <input type="text" class="form-control number-separator" name="target4" value="{{$data->target4}}">
                        </li>
                        @else
                        @for($i = 1; $i <= 4; $i++)
                        <li class="list-group-item">
                          <input type="text" class="form-control number-separator" name="target{{$i}}">
                        </li>
                        @endfor
                        @endif
                      </ul>
                    </td> 
                    <td>
                        <ul class="list-group list-group-flush">
                            {{-- @for($i = 1; $i<=4; $i++)
                            <li class="list-group-item">
                                @if($data->progress.$i == NULL)
                                <input disabled type="text" name="progress{{$i}}" class="form-coanger" value="{{$data->progress .''. $i}}">
                                @elseif($data->target.$i >= $data->progress.$i)
                                <input type="text" name="target{{$i}}" class="form-control number-separator bg-success" value="{{$data->progress .''. $i}}">
                                @else
                                <input type="text" name="target{{$i}}" class="form-control number-separator" value="{{$data->progress .''. $i}}">
                                @endif
                            </li>
                            @endfor --}}
                            @if($ada == 0)
                            <li class="list-group-item">
                                @if($data->target1 == NULL)
                                <input disabled type="text" name="progress1" class="form-control number-separator">
                                @elseif((int)$data->progress1 >= (int)$data->target1)
                                <input type="text" style="color:white;" value="{{$data->progress1}}" name="progress1" class="form-control number-separator bg-success">
                                @else
                                <input type="text" style="color:white;" value="{{$data->progress1}}" name="progress1" class="form-control number-separator bg-danger">
                                @endif
                            </li>
                            <li class="list-group-item">
                                @if($data->target2 == NULL)
                                <input disabled type="text" name="progress2" class="form-control number-separator">
                                @elseif((int)$data->progress2 >= (int)$data->target2)
                                <input type="text" style="color:white;" value="{{$data->progress2}}" name="progress2" class="form-control number-separator bg-success">
                                @elseif((int)$data->progress1 < (int)$data->target1)
                                <input disabled type="text" value="{{$data->progress2}}" name="progress2" class="form-control number-separator">
                                @else
                                <input type="text" style="color:white;" value="{{$data->progress2}}" name="progress2" class="form-control number-separator bg-danger">
                                @endif
                            </li>
                            <li class="list-group-item">
                                @if($data->target3 == NULL)
                                <input disabled type="text" name="progress3" class="form-control number-separator">
                                @elseif((int)$data->progress3 >= (int)$data->target3)
                                <input type="text" style="color:white;" value="{{$data->progress3}}" name="progress3" class="form-control number-separator bg-success">
                                @elseif((int)$data->progress2 < (int)$data->target2)
                                <input disabled type="text" value="{{$data->progress3}}" name="progress3" class="form-control number-separator">
                                @else
                                <input type="text" style="color:white;" value="{{$data->progress3}}" name="progress3" class="form-control number-separator bg-danger">
                                @endif
                            </li>
                            <li class="list-group-item">
                                @if($data->target4 == NULL)
                                <input disabled type="text" name="progress4" class="form-control number-separator">
                                @elseif((int)$data->progress4 >= (int)$data->target4)
                                <input type="text" style="color:white;" value="{{$data->progress4}}" name="progress4" class="form-control number-separator bg-success">
                                @elseif((int)$data->progress3 < (int)$data->target3)
                                <input disabled type="text" value="{{$data->progress4}}" name="progress4" class="form-control number-separator">
                                @else
                                <input type="text" style="color:white;" value="{{$data->progress4}}" name="progress4" class="form-control number-separator bg-danger">
                                @endif
                            </li>
                            @else
                            @for($j=1; $j <= 4; $j++)
                            <input disabled type="text" name="progress{{$j}}" class="form-control number-separator">
                            @endfor
                            @endif
                        </ul>
                    </td> 
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
      </form>
    </div>
  </div>
</section> 
@endsection 
@section('scripts')
{{-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> --}}
<script src="{{asset('js/easy-number-separator.js')}}"></script>

@endsection