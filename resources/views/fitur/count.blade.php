@extends('layouts.app')

@section('title')
Akun Count Progress
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
  <section class="section">
      <div class="section-header">
          <h3 class="page__heading">Akun Count Progress</h3>
      </div>

      <div class="section-body">
              <div class="card">
                <div class="card-header">
                  {{-- <a data-toggle="modal" data-target="#addAkun" href="#" data-id="{{ \Auth::id() }}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-user-plus"></i> Tambah Akun</a> --}}
                </div>
                <div class="card-body">
                  <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                <th>Nama Akun</th>
                                {{-- <th>Tiktok</th>
                                <th>Instagram</th> --}}
                                <th>Link ke Akun</th>
                            </tr>
                        </thead>
                    <tbody>
                      @foreach ($data as $a)
                      <tr>
                        <td>
                          {{$loop->iteration}}
                        </td>
                        <td>
                          @if($a->kategori == "Account Executive")
                          <span class="badge badge-warning">{{$a->nama}}</span>
                          @else
                          <span class="badge badge-dark">{{$a->nama}}</span>
                          @endif
                        </td>
                        <td>
                          @if($a->subtype == "Tiktok")
                          <a class="btn btn-icon icon-left btn-light" href="{{$a->link}}" target="_blank"><i class="fab fa-tiktok"></i> Klik to melihat Tiktok Akun </a>
                          @endif
                        {{-- </td>
                        <td> --}}
                            @if($a->subtype == "Instagram")
                            <a class="btn btn-icon icon-left btn-danger" href="{{$a->link}}" target="_blank"><i class="fab fa-instagram"></i> Klik to melihat Instagram Akun</a>
                            @endif
                        </td>
                    </tr>
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
      </div>
    </section>
@endsection

@section('scripts')

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script>

@endsection

