@extends('layouts.app') 
@section('title') 
Create Editing Request 
@endsection 
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css"> 
@endsection
@section('content') 
<section class="section">
    <div class="section-header">
        <h3 class="page__heading">Create Editing Request</h3>
    </div>
    <div class="section-body">
        @if($editor != 2 || Auth::user()->level == 0)
        <div class="card">
            <div class="card-body">
                <div class="table-responsive table">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th>
                                <th>Nama Akun</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody> 
                            @if($editor == 0) 
                            @foreach ($data as $a) 
                            <tr>
                                <td>
                                    {{$loop->iteration}}
                                </td>
                                <td> 
                                    @if($a->kategori == "Account Executive") 
                                    <span class="badge badge-warning">{{$a->nama}}</span> 
                                    @else <span class="badge badge-dark">{{$a->nama}}</span> 
                                    @endif 
                                </td>
                                <td>
                                    <a class="btn btn-icon icon-left btn-warning" href="/create/editing-request/{{$a->id}}">
                                        <i class="fas fa-pen-alt"></i>Create Editing Request </a>
                                </td> 
                                @endforeach 
                                @elseif(Auth::user()->level == 0)
                                @foreach ($akuns as $a) 
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td> 
                                        @if($a->kategori == "Account Executive") 
                                        <span class="badge badge-warning">{{$a->nama}}</span> 
                                        @else <span class="badge badge-dark">{{$a->nama}}</span> 
                                        @endif 
                                    </td>
                                    <td>
                                        <a class="btn btn-icon icon-left btn-warning" href="/create/editing-request/{{$a->id}}">
                                            <i class="fas fa-pen-alt"></i>Create Editing Request </a>
                                    </td> 
                                    @endforeach
                                @endif 
                            </tbody>
                    </table>
                </div>
            </div>
        </div> 
        @else
        @endif
        <div class="card">
            <div class="card-header"> 
                @if($editor == 2) 
                <h1>List Request Editing</h1> 
                
                @else <h1>Hasil Request Editing Anda</h1> 
                @endif 
                <div class="ml-auto w-0">
                    @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                    <a href="{{ route('exportEditingrequest')}}" onclick="return confirm('Yakin ingin export to excel?')" class="btn btn-success"><i class="fas fa-file-excel"></i> Export</a>
                    <a href="{{ route('resetEditingrequest')}}" class="btn btn-danger" onclick="return confirm('Yakin ingin reset data?')"><i class="fas fa-trash-restore-alt"></i> Reset</a>
                    @endif
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive table">
                    <table class="table table-striped" id="table-2">
                        <thead>
                            <tr>
                                <th style="width: 10%">#</th> @if($editor == 2) <th>Nama</th> @endif <th>Nama Akun</th>
                                <th>Concept</th>
                                <th>Estimeted Posting</th>
                                <th>Remark</th> 
                                @if($editor == 2) 
                                <th>Aksi</th>
                                <th>Status</th> 
                                @else 
                                <th>Aksi</th> 
                                @endif
                            </tr>
                        </thead>
                        <tbody> 
                            @if($editor == 0) 
                            @if($belum_submit == 0) 
                            @foreach ($data_hasil_submit as $a) 
                            <tr>
                                <td>
                                    {{$loop->iteration}}
                                </td>
                                <td> 
                                    @if($a->akun->kategori == "Account Executive") 
                                    <span class="badge badge-warning">{{$a->akun->nama}}</span> 
                                    @else <span class="badge badge-dark">{{$a->akun->nama}}</span>
                                    @endif 
                                </td>
                                <td> {!! $a->concept !!} </td>
                                <td>
                                    {{$a->estimeted_posting}}
                                </td>
                                <td> 
                                    @if($a->acc_by != NULL) 
                                    <span class="badge badge-success">Dihandle oleh : {{$a->acc->name}}</span> 
                                    @elseif($a->acc_by == 1) 
                                    <span class="badge badge-info">Ditolak</span>
                                    @else <span class="badge badge-danger">Belum dihandle</span> 
                                    @endif 
                                </td>
                                <td>
                                    <a class="btn btn-icon icon-left btn-danger" href="/delete/request-editing/{{$a->id}}">
                                        <i class="fas fa-trash"></i> Hapus </a>
                                    <a class="btn btn-icon icon-left btn-warning" href="/edit/request-editing/{{$a->id}}">
                                        <i class="fas fa-pen"></i> Edit </a>
                                </td> 
                                @endforeach 
                                @else 
                                @endif 
                                @elseif($editor == 2) 
                                @foreach ($data as $a)
                            <tr>
                                <td>
                                    {{$loop->iteration}}
                                </td>
                                <td>
                                    {{$a->user->name}}
                                </td>
                                <td> 
                                    @if($a->akun->kategori == "Account Executive") 
                                    <span class="badge badge-warning">{{$a->akun->nama}}</span> 
                                    @else <span class="badge badge-dark">{{$a->akun->nama}}</span> 
                                    @endif 
                                </td>
                                <td> {!! $a->concept !!} </td>
                                <td>
                                    {{$a->estimeted_posting}}
                                </td>
                                <td> 
                                    @if($a->acc_by != NULL) 
                                    Dihandle oleh : {{$a->acc->name}} 
                                    @else Belum dihandle
                                    @endif 
                                </td>
                                <td>
                                    @if($a->status != 6)
                                    @if($a->acc_by != NULL)
                                        @if($a->acc_by == Auth::user()->id)
                                    <div class="dropdown d-inline">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Aksi
                                        </button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
                                       
                                            <a href="{{route('accept', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin mengaccept editing request ini?')">
                                                <i class="fas fa-vote-yea"></i> Accept </a>
                                            <a href="{{route('decline', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin mengdecline editing request ini?')">
                                                <i class="fas fa-eraser"></i> Decline </a> 
                                            <a href="{{route('hold', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin menghold editing request ini?')">
                                                <i class="fas fa-eraser"></i> Hold </a> 
                                            <a href="{{route('waiting', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin mengganti status ke waiting editing request ini?')">
                                                <i class="fas fa-eraser"></i> Waiting </a> 
                                            <a href="{{route('trouble', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin mengganti status ke trouble editing request ini?')">
                                                <i class="fas fa-eraser"></i> Trouble </a> 
                                            <a href="{{route('finish', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin mengganti status ke finish editing request ini?')">
                                                <i class="fas fa-eraser"></i> Finish </a> 

                                        </div>
                                    </div>
                                    @else
                                        <span class="badge badge-info">Tidak Ada Akses</span>
                                    @endif
                                    
                                    @else
                                    <div class="dropdown d-inline">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Aksi
                                        </button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
                                       
                                            <a href="{{route('accept', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin mengaccept editing request ini?')">
                                                <i class="fas fa-vote-yea"></i> Accept </a>
                                            <a href="{{route('decline', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin mengdecline editing request ini?')">
                                                <i class="fas fa-eraser"></i> Decline </a> 
                                            <a href="{{route('hold', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin menghold editing request ini?')">
                                                <i class="fas fa-eraser"></i> Hold </a> 
                                            <a href="{{route('waiting', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin mengganti status ke waiting editing request ini?')">
                                                <i class="fas fa-eraser"></i> Waiting </a> 
                                            <a href="{{route('trouble', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin mengganti status ke trouble editing request ini?')">
                                                <i class="fas fa-eraser"></i> Trouble </a> 
                                            <a href="{{route('finish', ['id' => $a->id])}}" class="dropdown-item has-icon" onclick="return confirm('Yakin ingin mengganti status ke finish editing request ini?')">
                                                <i class="fas fa-eraser"></i> Finish </a> 

                                        </div>
                                    </div>
                                    @endif
                                    @else
                                        <span class="badge badge-success">Sudah Finish</span>
                                    @endif
                                </td>
                                <td> 
                                    @if($a->status == 2) 
                                    <span class="badge badge-success">accept</span> 
                                    @elseif($a->status == 1) 
                                    <span class="badge badge-info">tolak</span> 
                                    @elseif($a->status == 3) 
                                    <span class="badge badge-warning">Hold</span> 
                                    @elseif($a->status == 4) 
                                    <span class="badge badge-dark">Waiting</span>
                                    @elseif($a->status == 5) 
                                    <span class="badge badge-danger">Trouble</span>
                                    @elseif($a->status == 6) 
                                    <span class="badge badge-success">Finish</span>  
                                    @else 
                                    <span class="badge badge-danger">Belum diaccept</span> 
                                    @endif 
                                </td> 
                                    @endforeach 
                                    @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section> 
@endsection
@section('scripts') <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-1').DataTable();
    })
</script> 
<script>
    $(document).ready(function() {
        $('#table-2').DataTable();
    })
</script> 
@endsection