@extends('layouts.app') 
{{-- @section('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection --}} 
@section('title')
Daily Trouble
@endsection

@section('content') 
<section class="section">
  <div class="section-header">
    <h3 class="page__heading">Manage Daily Trouble</h3>
  </div>
  <div class="section-body">
    @if($belum == 0)
            <form action="{{route('addtrouble', ['id' => $data->id])}}" method="post" enctype="multipart/form-data">
              @csrf
            <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                        <div class="alert alert-primary alert-has-icon alert-lg">
                        <div class="alert-icon">
                            <i class="far fa-user-circle"></i>
                        </div>
                        <div class="alert-body">
                            <div class="alert-title">Nama Akun : {{$data->nama}} </div>
                        </div>
                        </div>
                        &nbsp;
                        <button type="submit" class="btn-icon icon-left btn-success btn-lg"><i class="fas fa-save"></i> Save</button>
                    </div>
                    <div class="card-body">
                    @for($i = 1; $i <= 6; $i++)
                      <div class="form-group row mb-4">
                        @if($i == 1)
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Senin</label>
                        @elseif($i == 2)
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Selasa</label>
                        @elseif($i == 3)
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Rabu</label>
                        @elseif($i == 4)
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kamis</label>
                        @elseif($i == 5)
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jum'at</label>
                        @elseif($i == 6)
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Sabtu</label>
                        @endif
                        <div class="col-sm-12 col-md-7">
                          <textarea name="trouble{{$i}}" class="form-control"></textarea>
                        </div>
                      </div>
                    @endfor
                  </div>
                </div>
            </div>      
          </div>
        </form>
    </div>
    @else
    <form action="{{route('updatetrouble', ['id' => $data->akun->id])}}" method="post" enctype="multipart/form-data">
      @csrf
      {{method_field('PUT')}}
      <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <div class="alert alert-primary alert-has-icon alert-lg">
                  <div class="alert-icon">
                      <i class="far fa-user-circle"></i>
                  </div>
                  <div class="alert-body">
                      <div class="alert-title">Nama Akun : {{$data->akun->nama}} </div>
                  </div>
                  </div>
                  &nbsp;
                  <button type="submit" class="btn-icon icon-left btn-warning btn-lg"><i class="fas fa-pen-alt"></i> Update</button>
                  <div class="ml-auto w-0">
                    @if(Auth::user()->level == 0 || Auth::user()->level == 2)
                    <a href="{{ route('exportdailyTrouble', ['id' => $data->akun->id]) }}" onclick="return confirm('Yakin ingin export to excel?')" class="btn btn-success"><i class="fas fa-file-excel"></i> Export</a>
                    <a href="{{ route('resetdailyTrouble', ['id' => $data->akun->id]) }}" class="btn btn-danger" onclick="return confirm('Yakin ingin reset data?')"><i class="fas fa-trash-restore-alt"></i> Reset</a>
                    @endif
                  </div>
              </div>
              <div class="card-body">
              @for($i = 1; $i <= 6; $i++)
                <div class="form-group row mb-4">
                  @if($i == 1)
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Senin</label>
                  @elseif($i == 2)
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Selasa</label>
                  @elseif($i == 3)
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Rabu</label>
                  @elseif($i == 4)
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kamis</label>
                  @elseif($i == 5)
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jum'at</label>
                  @elseif($i == 6)
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Sabtu</label>
                  @endif
                  <div class="col-sm-12 col-md-7">
                    @if($i == 1)
                    <textarea name="trouble{{$i}}" value="{{$data->senin}}" class="form-control">{{$data->senin}}</textarea>
                    @elseif($i == 2)
                    <textarea name="trouble{{$i}}" value="{{$data->selasa}}" class="form-control">{{$data->selasa}}</textarea>
                    @elseif($i == 3)
                    <textarea name="trouble{{$i}}" value="{{$data->rabu}}" class="form-control">{{$data->rabu}}</textarea>
                    @elseif($i == 4)
                    <textarea name="trouble{{$i}}" value="{{$data->kamis}}" class="form-control">{{$data->kamis}}</textarea>
                    @elseif($i == 5)
                    <textarea name="trouble{{$i}}" value="{{$data->jumat}}" class="form-control">{{$data->jumat}}</textarea>
                    @elseif($i == 6)
                    <textarea name="trouble{{$i}}" value="{{$data->sabtu}}" class="form-control">{{$data->sabtu}}</textarea>
                    @endif
                  </div>
                </div>
              @endfor
            </div>
          </div>
      </div>      
    </div>
  </form>
</div>

    @endif
</div>
</section> 
@endsection 