/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!****************************************!*\
  !*** ./resources/assets/js/profile.js ***!
  \****************************************/
$(document).on('click', '.edit-profile', function (event) {
  $('#editProfileUserId').val(loggedInUser.id);
  $('#pfName').val(loggedInUser.name);
  $('#pfEmail').val(loggedInUser.email);
  $('#EditProfileModal').appendTo('body').modal('show');
});
$(document).on('change', '#pfImage', function () {
  var ext = $(this).val().split('.').pop().toLowerCase();

  if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
    $(this).val('');
    $('#editProfileValidationErrorsBox').html('The profile image must be a file of type: jpeg, jpg, png.').show();
  } else {
    displayPhoto(this, '#edit_preview_photo');
  }
});


window.displayPhoto = function (input, selector) {
  var displayPreview = true;

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      var image = new Image();
      image.src = e.target.result;

      image.onload = function () {
        $(selector).attr('src', e.target.result);
        displayPreview = true;
      };
    };

    if (displayPreview) {
      reader.readAsDataURL(input.files[0]);
      $(selector).show();
    }
  }
};
// $('#btnPrEditSave').on('click', function() {
//   var $this = $(this);
// $this.button('loading');
//   setTimeout(function() {
//      $this.button('reset');
//  }, 3000);
// });

//asyncronus for reports
for(let i = 1; i<=6; i++){
  $('.increasing, .views'+[i]).on('change', function(){
    let x = $('.increasing'+[i]).val() / $('.views'+[i]).val()
    $('.ratio'+[i]).val(x)
  
  });

  $('.followers'+[i]).on('change', function(){
    var f = +$('.followers'+1).val() + +$('.followers'+2).val() + +$('.followers'+3).val() + +$('.followers'+4).val() + +$('.followers'+5).val() + +$('.followers'+6).val()
    $('.totalreports'+1).val(f)
  });

  $('.views'+[i]).on('change', function(){
    var f = +$('.views'+1).val() + +$('.views'+2).val() + +$('.views'+3).val() + +$('.views'+4).val() + +$('.views'+5).val() + +$('.views'+6).val()
    $('.totalreports'+2).val(f)
  });

  $('.increasing'+[i]).on('change', function(){
    var f = +$('.increasing'+1).val() + +$('.increasing'+2).val() + +$('.increasing'+3).val() + +$('.increasing'+4).val() + +$('.increasing'+5).val() + +$('.increasing'+6).val()
    $('.totalreports'+3).val(f)
  });

  $('.ratio'+[i]).on('change', function(){
    var f = +$('.ratio'+1).val() + +$('.ratio'+2).val() + +$('.ratio'+3).val() + +$('.ratio'+4).val() + +$('.ratio'+5).val() + +$('.ratio'+6).val()
    $('.totalreports'+4).val(f)
  });

  $('.inventory'+[i]).on('change', function(){
    var f = +$('.inventory'+1).val() + +$('.inventory'+2).val() + +$('.inventory'+3).val() + +$('.inventory'+4).val() + +$('.inventory'+5).val() + +$('.inventory'+6).val()
    $('.totalreports'+5).val(f)
  });

  $('.actualp'+[i]).on('change', function(){
    var f = +$('.actualp'+1).val() + +$('.actualp'+2).val() + +$('.actualp'+3).val() + +$('.actualp'+4).val() + +$('.actualp'+5).val() + +$('.actualp'+6).val()
    $('.totalreports'+6).val(f)
  });
}
$( document ).ready(function() {
  var f = +$('.followers'+1).val() + +$('.followers'+2).val() + +$('.followers'+3).val() + +$('.followers'+4).val() + +$('.followers'+5).val() + +$('.followers'+6).val()
    $('.totalreports'+1).val(f)

    var f = +$('.views'+1).val() + +$('.views'+2).val() + +$('.views'+3).val() + +$('.views'+4).val() + +$('.views'+5).val() + +$('.views'+6).val()
    $('.totalreports'+2).val(f)
  
    var f = +$('.increasing'+1).val() + +$('.increasing'+2).val() + +$('.increasing'+3).val() + +$('.increasing'+4).val() + +$('.increasing'+5).val() + +$('.increasing'+6).val()
    $('.totalreports'+3).val(f)
    var f = +$('.ratio'+1).val() + +$('.ratio'+2).val() + +$('.ratio'+3).val() + +$('.ratio'+4).val() + +$('.ratio'+5).val() + +$('.ratio'+6).val()
    $('.totalreports'+4).val(f)
    var f = +$('.inventory'+1).val() + +$('.inventory'+2).val() + +$('.inventory'+3).val() + +$('.inventory'+4).val() + +$('.inventory'+5).val() + +$('.inventory'+6).val()
    $('.totalreports'+5).val(f)
    var f = +$('.actualp'+1).val() + +$('.actualp'+2).val() + +$('.actualp'+3).val() + +$('.actualp'+4).val() + +$('.actualp'+5).val() + +$('.actualp'+6).val()
    $('.totalreports'+6).val(f)
});

//asyncronus daily works
for(let j = 1; j<=6; j++){
  $('.script'+[j]).on('change', function(){
    var y = +$('.script'+1).val() + +$('.script'+2).val() + +$('.script'+3).val() + +$('.script'+4).val() + +$('.script'+5).val() + +$('.script'+6).val()
    $('.total'+1).val(y)
  });

  $('.edit'+[j]).on('change', function(){
    var z = +$('.edit'+1).val() + +$('.edit'+2).val() + +$('.edit'+3).val() + +$('.edit'+4).val() + +$('.edit'+5).val() + +$('.edit'+6).val()
    $('.total'+2).val(z)
  });

  $('.shooting'+[j]).on('change', function(){
    var a = +$('.shooting'+1).val() + +$('.shooting'+2).val() + +$('.shooting'+3).val() + +$('.shooting'+4).val() + +$('.shooting'+5).val() + +$('.shooting'+6).val()
    $('.total'+3).val(a)
  });

  $('.posting'+[j]).on('change', function(){
    var b = +$('.posting'+1).val() + +$('.posting'+2).val() + +$('.posting'+3).val() + +$('.posting'+4).val() + +$('.posting'+5).val() + +$('.posting'+6).val()
    $('.total'+4).val(b)
  });

  $('.admin'+[j]).on('change', function(){
    var c = +$('.admin'+1).val() + +$('.admin'+2).val() + +$('.admin'+3).val() + +$('.admin'+4).val() + +$('.admin'+5).val() + +$('.admin'+6).val()
    $('.total'+5).val(c)
  });

}

$( document ).ready(function() {
  y = +$('.script'+1).val() + +$('.script'+2).val() + +$('.script'+3).val() + +$('.script'+4).val() + +$('.script'+5).val() + +$('.script'+6).val()
  $('.total'+1).val(y);
  $('#total1').html(y);

  var z = +$('.edit'+1).val() + +$('.edit'+2).val() + +$('.edit'+3).val() + +$('.edit'+4).val() + +$('.edit'+5).val() + +$('.edit'+6).val()
    $('.total'+2).val(z)
  
  var a = +$('.shooting'+1).val() + +$('.shooting'+2).val() + +$('.shooting'+3).val() + +$('.shooting'+4).val() + +$('.shooting'+5).val() + +$('.shooting'+6).val()
  $('.total'+3).val(a)
  var b = +$('.posting'+1).val() + +$('.posting'+2).val() + +$('.posting'+3).val() + +$('.posting'+4).val() + +$('.posting'+5).val() + +$('.posting'+6).val()
  $('.total'+4).val(b)
  var c = +$('.admin'+1).val() + +$('.admin'+2).val() + +$('.admin'+3).val() + +$('.admin'+4).val() + +$('.admin'+5).val() + +$('.admin'+6).val()
  $('.total'+5).val(c)
});

//end asyncronus dailyworks

// $('.views1, .increasing1').on('change', function(){

//   $('.ratio1').val($('.views1').val() / $('.increasing1').val());

// });
//update live end start 
$('.live_start').on('change', function(){
  var c = $('.live_start').val()
  $('.hidden_live_start').val(c)
});

$('.live_end').on('change', function(){
  var d = $('.live_end').val()
  $('.hidden_live_end').val(d)
});

$(document).on('submit', '#editProfileForm', function (event) {
  event.preventDefault();
  // var userId = $('#editProfileUserId').val();
  var loadingButton = jQuery(this).find('#btnPrEditSave');
  loadingButton.button('loading');
  $.ajax({
    url: '/edit/foto-profil',
    type: 'post',
    data: new FormData($(this)[0]),
    processData: false,
    contentType: false,
    success: function success(result) {
      // if (result.success) {
      $('#EditProfileModal').modal('hide');
      //   setTimeout(function () {
      //     location.reload();
      //   }, 100);
      // }
    },
    error: function error(result) {
      console.log(result);
    },
    complete: function complete() {
      loadingButton.button('reset');
    }
  });
});
/******/ })()
;